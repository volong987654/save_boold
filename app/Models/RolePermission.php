<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolePermission extends Model
{
  use ListData;
  use SoftDeletes;

  protected $guarded = [];
  protected $hidden = ['deleted_at'];

  public function getCanAttribute($value)
  {
    return json_decode($value);
  }
  public function setCanAttribute($value)
  {
    $this->attributes['can'] =  json_encode($value);
  }


  public function scopeGetData($query, $request)
  {
    return $this->getListData($query, $request);
  }

  /**
   * Get the role for user.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function role()
  {
    return $this->belongsTo(Role::class);
  }

  /**
   * Get the role for user.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function permission()
  {
    return $this->belongsTo(Permission::class);
  }
}
