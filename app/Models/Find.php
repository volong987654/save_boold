<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Find extends Model
{
  use ListData;
  use SoftDeletes;

  protected $guarded = [];
  protected $hidden = ['deleted_at'];


  public function scopeGetData($query, $request)
  {
    return $this->getListData($query, $request);
  }

  public function blood()
  {
    return $this->belongsTo(\App\Models\Blood::class);
  }

  public function hospital()
  {
    return $this->belongsTo(\App\Models\Hospital::class);
  }

  public function user()
  {
    return $this->belongsTo(\App\User::class);
  }

  public function donor()
  {
    return $this->belongsTo(\App\User::class);
  }
}
