<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
  use ListData;
  use SoftDeletes;

  protected $guarded = [];
  protected $hidden = ['deleted_at'];

  public function scopeGetData($query, $request)
  {
    return $this->getListData($query, $request);
  }

  /**
   * Get the posts for the user.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function rolePermissions()
  {
    return $this->hasMany(RolePermission::class);
  }
}
