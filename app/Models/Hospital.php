<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
  use SoftDeletes;

  protected $guarded = [];
  protected $hidden = ['deleted_at'];

  public function users()
  {
    return $this->belongsToMany("App\User", "user_hospital")->withTimestamps();
  }
  public function children(){
    return $this->hasMany(Hospital::class,'parent_id');
  }
}
