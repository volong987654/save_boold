<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\RolePermission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RolePermissionController extends Controller
{
  /**
   * RolePermissionController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $result = RolePermission::create($this->validateRequest());
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\RolePermission $rolePermission
   * @return \Illuminate\Http\Response
   */
  public function show(RolePermission $rolePermission)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\RolePermission $rolePermission
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, RolePermission $rolePermission)
  {
    $rolePermission->update($this->validateRequest());
    return \Response::success(trans("messages.updateSuccess"), $rolePermission->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\RolePermission $rolePermission
   * @return \Illuminate\Http\Response
   */
  public function destroy(RolePermission $rolePermission)
  {
    $rolePermission->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  /**
   * @return array
   */
  protected function validateRequest(): array
  {
    return \Request::validate([
      "role_id" => "required",
      "permission_route" => "required",
      "can" => "required",
    ]);
  }
}
