<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
  /**
   * CategoryController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::get();
    return response(compact(["categories"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Category::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Category::find($value["id"])->update(["order" => $value["order"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Category $category
   * @return \Illuminate\Http\Response
   */
  public function show($status)
  {
    $categories = [];
    if ($status === "select") {
      $categories = Category::orderBy('order')->select("id","name")->get();
    }
    return response(compact(["categories"]), Response::HTTP_OK);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Category $category
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Category $category)
  {
    $category->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $category->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Category $category
   * @return \Illuminate\Http\Response
   */
  public function destroy(Category $category)
  {
    $category->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => "required",
      "slug" => "",
      "description" => "",
      "status" => "",
    ]);
    if ($query === "store") $data["order"] = Category::count();
    return $data;
  }
}
