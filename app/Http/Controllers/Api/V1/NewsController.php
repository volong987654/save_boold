<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\News;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NewsController extends Controller
{
  /**
   * NewsController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $result = News::with(["categories"])->getData($request);
    return response($result, Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $result = News::create($this->validateRequest("store"));
    if ($request->has("category_id")) {
      $result->categories()->attach($request->input("category_id"));
    }
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param News $news
   * @return Response
   */
  public function show(News $news)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param News $news
   * @return Response
   */
  public function update(Request $request, News $news)
  {
    if ($request->method() === "PUT") {
      $news->update($this->validateRequest("update"));
      if ($request->has("category_id")) $news->categories()->sync($request->input("category_id"));
    } else {
      $data = \Request::validate([
        "status" => "",
      ]);
      $news->update($data);
    }
    return \Response::success(trans("messages.updateSuccess"), $news->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param News $news
   * @return Response
   * @throws Exception
   */
  public function destroy(News $news)
  {
    $news->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  /**
   * @return array
   */
  protected function validateRequest($query): array
  {
    return \Request::validate([
      "name" => "required",
      "slug" => $query === "store" ? "unique:news,slug" : "required",
      "image"=>"required",
      "description" => "required",
      "body" => "required",
      "status" => "",
    ]);
  }
}
