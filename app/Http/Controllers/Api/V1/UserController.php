<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
  /**
   * UserController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $result = User::whereNotIn ('users.id', array_unique([1,auth()->user()->id]))->with(["role:id,name","blood:id,name","organization:id,name","hospitals","history"])->getData($request);
    return response($result, Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $result = User::create($this->validateRequest("store"));
    if ($request->has("hospital_id")) {
      $result->hospitals()->attach($request->input("hospital_id"));
    }
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param $status
   * @return \Illuminate\Http\Response
   */
  public function show($status)
  {
    switch ($status) {
      case "select":
        $users = User::select("id","name")->get();
        return response(compact(["users"]), Response::HTTP_OK);
        break;
      case "donor":
        $blood_id = \request()->get("blood_id");
        $result = User::where([
          ['blood_id', '=', $blood_id],
          ['last_time', '>=', Carbon::now()->subDays(90)->toDateTimeString()]
        ])->orderBy('last_time', 'DESC')->get();
        return response($result, Response::HTTP_OK);
      default :
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
    if ($request->method() === "PUT") {
      $user->update($this->validateRequest("update"));
      if ($request->has("hospital_id")) $user->hospitals()->sync($request->input("hospital_id"));
    } else {
      $data = \Request::validate([
        "status" => "",
        "note" => "",
      ]);
      $user->update($data);
    }
    return \Response::success(trans("messages.updateSuccess"), $user->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $user->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  /**
   * @return array
   */
  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => "required",
      "email" =>  $query === "store" ? "unique:users,email" : "required",
      "password" => $query === "store" ? "required" : "",
      "role_id" => "",
      "avatar" => "",
      "bio" => "",
      "status" => "",
    ]);
    if (empty($data["password"])) unset($data["password"]);
    return $data;
  }
}
