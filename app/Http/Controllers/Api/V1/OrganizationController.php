<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrganizationController extends Controller
{
  /**
   * OrganizationController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $organizations = Organization::get();
    return response(compact(["organizations"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Organization::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Organization::find($value["id"])->update(["order" => $value["order"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param $status
   * @return \Illuminate\Http\Response
   */
  public function show($status)
  {
    $organizations = [];
    if ($status === "select") {
      $organizations = Organization::orderBy('order')->select("id","name")->get();
    }
    return response(compact(["organizations"]), Response::HTTP_OK);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Organization $organization
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Organization $organization)
  {
    $organization->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $organization->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Organization $organization
   * @return \Illuminate\Http\Response
   */
  public function destroy(Organization $organization)
  {
    $organization->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => "required",
      "description" => "",
      "status" => "",
      "logo" => "",
      "url" => "",
    ]);
    if ($query === "store") $data["order"] = Organization::count();
    return $data;
  }
}
