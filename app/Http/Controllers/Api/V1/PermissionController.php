<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends Controller
{
  /**
   * PermissionController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $permissions = Permission::orderBy('order')->get();
    return response(compact(["permissions"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Permission::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Permission::find($value["id"])->update(["order" => $value["order"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Permission $permission
   * @return \Illuminate\Http\Response
   */
  public function show(Permission $permission)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Permission $permission
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Permission $permission)
  {
    $permission->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $permission->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Permission $permission
   * @return \Illuminate\Http\Response
   */
  public function destroy(Permission $permission)
  {
    $permission->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "route" => $query === "store" ? "unique:permissions,route" : "required",
      "name" => "required",
      "description" => "",
      "status" => "",
    ]);
    if ($query === "store") $data["order"] = Permission::count();
    return $data;
  }
}
