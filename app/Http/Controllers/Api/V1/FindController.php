<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Find;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FindController extends Controller
{
  /**
   * FindController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $result = Find::with(["hospital:id,name","blood:id,name", "user:id,name", "donor:id,name"])->getData($request);
    return response($result, Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $result = Find::create($this->validateRequest());
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param Find $find
   * @return Response
   */
  public function show(Find $find)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Find $find
   * @return Response
   */
  public function update(Request $request, Find $find)
  {
    if ($request->method() === "PUT") {
      $find->update($this->validateRequest());
    } else {
      $data = \Request::validate([
        "status" => "",
        "donor_id" => "",
      ]);
      $find->update($data);
    }
    return \Response::success(trans("messages.updateSuccess"), $find->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Find $find
   * @return Response
   * @throws Exception
   */
  public function destroy(Find $find)
  {
    $find->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  /**
   * @return array
   */
  protected function validateRequest(): array
  {
    return \Request::validate([
      "user_id" => "required",
      "hospital_id" => "required",
      "blood_id" => "required",
      "name" => "required",
      "instruction" => "required",
      "description" => "required",
      "relatives_name" => "required",
      "relatives_phone" => "required",
      "status" => "",
    ]);
  }
}
