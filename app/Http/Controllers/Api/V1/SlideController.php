<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SlideController extends Controller
{
  /**
   * SlideController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $slides = Slide::get();
    return response(compact(["slides"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Slide::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Slide::find($value["id"])->update(["order" => $value["order"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Category $category
   * @return \Illuminate\Http\Response
   */
  public function show($status)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param Slide $slide
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Slide $slide)
  {
    $slide->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $slide->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Slide $slide
   * @return \Illuminate\Http\Response
   * @throws \Exception
   */
  public function destroy(Slide $slide)
  {
    $slide->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "title" => "required",
      "text" => "required",
      "image" => "required",
      "page" => "required",
    ]);
    if ($query === "store") $data["order"] = Slide::count();
    return $data;
  }
}
