<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
  /**
   * RoleController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $roles = Role::with(["attributes"])->orderBy('order')->get();
    return response(compact(["roles"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Role::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Role::find($value["id"])->update(["order" => $value["order"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Role $role
   * @return \Illuminate\Http\Response
   */
  public function show(Role $role)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Role $role
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Role $role)
  {
    $role->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $role->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Role $role
   * @return \Illuminate\Http\Response
   */
  public function destroy(Role $role)
  {
    $role->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => $query === "store" ? "unique:roles,name" : "required",
      "title" => "",
      "description" => "",
      "is_admin" => "",
      "status" => "",
    ]);
    if ($query === "store") $data["order"] = Role::count();
    return $data;
  }
}
