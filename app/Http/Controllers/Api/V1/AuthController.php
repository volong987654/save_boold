<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use stdClass;

class AuthController extends Controller
{

  /**
   * AuthController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api', ['except' => ['index', "checkSlug", "store"]]);
    auth()->shouldUse('api');
  }


  /**
   * @return mixed
   */
  public function index()
  {
    $credentials = request(['email', 'password']); // !$token = auth()->setTTL(1)->attempt($credentials)
    $token = auth()->attempt($credentials);
    if (!$token || !auth()->user()->status || !auth()->user()->email_verified_at) {
      return \Response::error(trans("messages.unauthorized"), Response::HTTP_UNAUTHORIZED);
    }

    return $this->respondWithToken($token);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(Request $request)
  {
    $data = \Request::validate([
      "name" => "required",
      "email" =>  "unique:users,email",
      "password" => "required",
    ]);
    $result = User::create($data);
    $result->sendEmailVerificationNotification();
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  /**
   * Display the specified resource.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(Request $request)
  {
    if ($request->method() === "PUT") {
      $data = \Request::validate([
        "name" => "required",
        "password" => "",
        "bio" => "",
        "avatar" => "",
      ]);
      if (empty($data["password"])) unset($data["password"]);
      auth()->user()->update($data);
      return \Response::success(trans("messages.updateSuccess"), auth()->user()->refresh());
    }
    return $this->respondWithToken(auth()->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    //
  }

  /**
   * Check slug.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function checkSlug(Request $request){
    $data = $request->all();
    $model = $data["model"];
    unset($data["model"]);
    $check = true;
    $arrayKey = array_keys($data);
    $rows = DB::table($model)->where($arrayKey[0], $data[$arrayKey[0]])->get();
    if (isset($data["id"]) && count($rows) > 0) {
      foreach($rows as $row) {
        if ($check && $row->id !== $data["id"]) $check = false;
      }
    } else {
      $check = count($rows) === 0;
    }
    return \Response::success(null, $check);
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
    $user = auth()->user();
    $user->token = "Bearer " . $token;
    $user->expires_in = (strtotime("now") + (auth()->factory()->getTTL() * 60))  * 1000;
    $permissions = \App\Models\Role::find($user->role_id)->attributes()->get();
    $setting = Setting::all();
    $object = [];
    foreach ($setting as $item)
    {
      $object[$item->name] = $item->value;
    }
    $user->setting = $object;
    $user->permissions = $permissions;

    return \Response::success(trans("messages.getTokenSuccess"), $user);
  }
}
