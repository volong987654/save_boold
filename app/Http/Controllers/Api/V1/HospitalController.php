<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Hospital;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HospitalController extends Controller
{
  /**
   * HospitalController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $hospitals = Hospital::orderBy('order')->get();
    return response(compact(["hospitals"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Hospital::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Hospital::find($value["id"])->update(["order" => $value["order"], "parent_id" => $value["parent_id"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param $status
   * @return Response
   */
  public function show($status)
  {
    $hospitals = [];
    if ($status === "select") {
      $hospitals = Hospital::orderBy('order')->select("id","name","parent_id")->get();
    }
    return response(compact(["hospitals"]), Response::HTTP_OK);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Hospital $hospital
   * @return Response
   */
  public function update(Request $request, Hospital $hospital)
  {
    $hospital->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $hospital->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Hospital $hospital
   * @return Response
   * @throws Exception
   */
  public function destroy(Hospital $hospital)
  {
    $hospital->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => "required",
      "address" => "",
      "status" => "",
    ]);
    if ($query === "store") $data["order"] = Hospital::count();
    return $data;
  }
}
