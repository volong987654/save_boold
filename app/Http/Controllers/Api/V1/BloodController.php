<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Blood;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BloodController extends Controller
{
  /**
   * BloodController constructor.
   */
  public function __construct()
  {
    $this->middleware('auth:api');
    auth()->shouldUse('api');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $bloods = Blood::orderBy('order')->get();
    return response(compact(["bloods"]), Response::HTTP_OK);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    if ($request->has('order')) {
      $result = Blood::create($this->validateRequest("store"));
      return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
    } else {
      foreach ($request->all() as &$value) {
        if(!empty($value["id"])) Blood::find($value["id"])->update(["order" => $value["order"], "parent_id" => $value["parent_id"], "status" => $value["status"]]);
      }
      return \Response::success(trans("messages.updateSuccess"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param $status
   * @return Application|ResponseFactory|Response
   */
  public function show($status)
  {
    $bloods = [];
    if ($status === "select") {
      $bloods = Blood::orderBy('order')->select("id","name","parent_id")->get();
    }
    return response(compact(["bloods"]), Response::HTTP_OK);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Blood $blood
   * @return Response
   */
  public function update(Request $request, Blood $blood)
  {
    $blood->update($this->validateRequest("update"));
    return \Response::success(trans("messages.updateSuccess"), $blood->refresh());
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Blood $blood
   * @return Response
   * @throws Exception
   */
  public function destroy(Blood $blood)
  {
    $blood->delete();
    return \Response::success(trans("messages.deleteSuccess"));
  }

  protected function validateRequest($query): array
  {
    $data = \Request::validate([
      "name" => "required",
      "description" => "",
      "status" => "",
    ]);
    if ($query === "store") $data["order"] = Blood::count();
    return $data;
  }
}
