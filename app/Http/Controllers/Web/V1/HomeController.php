<?php

namespace App\Http\Controllers\Web\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPassword;
use App\Mail\ContactMail;

use App\Mail\MailNotify;
use App\Mail\UserMail;
use App\Models\Category;
use App\Models\Comment;
use App\Models\History;
use App\Models\Organization;

use App\Models\Setting;
use App\Models\Slide;
use App\Models\Find;
use App\Models\News;
use App\Models\Blood;
use App\Models\Hospital;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    //$this->middleware('auth');
  }

  public function admin()
  {
    return view("admin");
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $background = Slide::where('page', 'home')->where('status', 1)->get();
    $userAuth = auth()->user();
    $hospital = Hospital::with('children')->whereNull('parent_id')->get();
    $blood = Blood::with('children')->whereNull('parent_id')->get();
    return view('home', compact('blood', 'hospital', 'userAuth', 'background'));
  }

  public function findStore(Request $request)
  {

    $user = Auth::user();
    if (!isset($user)) {
      $data = $request->validate([
        "name" => 'required|min:6|max:255',
        "phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        "email" => "unique:users,email",
        "gender" => 'required|numeric',
        "address" => 'required|min:6|max:255',
        "identity_card" => 'required|numeric',
        "blood_id" => 'required|numeric',
        "hospital_id" => 'required|numeric',
        "instruction" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        "relatives_name" => 'required|min:6|max:255',
        "relatives_phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        "description" => "required",
      ]);
      $dataU = $request->validate([
        "name" => 'required|min:6|max:255',
        "phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        "email" => "unique:users,email",
        "gender" => 'required|numeric',
        "address" => 'required|min:6|max:255',
        "identity_card" => 'required|numeric',
      ]);
      $dataU["password"] = $dataU['email'];
      $dataU["role_id"] = 2;
      $dataU["status"] = 1;
      $dataU["email_verified_at"] = Carbon::now();
      $result = User::create($dataU);
      Mail::to($data['email'])->send(new MailNotify($data));
      $dataF = $request->validate([
        "blood_id" => 'required|numeric',
        "hospital_id" => 'required|numeric',
        "relatives_name" => 'required|min:6|max:255',
        "relatives_phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        "description" => "required",
      ]);
      $imageName = '/storage/'.time() . '.' . $request->instruction->extension();
      $request->instruction->storeAs('public/', time() . '.' . $request->instruction->extension());
      $dataF["instruction"] = $imageName;
      $dataF["user_id"] = $result->id;
      $dataF["status"]=0;
      $result2 = Find::create($dataF);
      Auth::attempt($dataU);
      return \Response::success(trans("messages.createSuccess"), [$result, $result2], Response::HTTP_CREATED);

    } else {
      $dataF = $request->validate([
        "blood_id" => 'required|numeric',
        "hospital_id" => 'required|numeric',
        "relatives_name" => 'required|min:6|max:255',
        "relatives_phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        "description" => "required",
        "instruction" => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      $imageName = '/storage/'.time() . '.' . $request->instruction->extension();
      $request->instruction->storeAs('public/', time() . '.' . $request->instruction->extension());
      $dataF["instruction"] = $imageName;
      $dataF["user_id"] = $user->id;
      $dataF["status"]=0;
      $result2 = Find::create($dataF);
      return \Response::success(trans("messages.createSuccess"), $result2, Response::HTTP_CREATED);
    }
  }

  public function BloodDonation($id)
  {
    auth()->user()->update(['blood_id' => $id]);
    return back();
  }


  public function news()
  {
    $footer = Setting::where('status', 1)->first();
    $new1 = News::where('status', 1)->orderBy('id', 'desc')->paginate(7);
    $new2 = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);
    $organizations = Organization::where('status', 1)->where('featured', 1)->get();
    $userAuth = auth()->user();
    $background = Slide::where('page', 'news')->where('status', 1)->get();
    $new = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);

    return view('news', compact('new', 'background', 'userAuth', 'organizations', 'footer','new1','new2'));
  }


  public function newDetail(Request $request, $slug)
  {
    $userAuth = auth()->user();
    $new1 = News::where('status', 1)->orderBy('id', 'desc')->paginate(7);
    $new2 = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);
    $footer = Setting::where('status', 1)->first();
    $background = Slide::where('page', 'news')->where('status', 1)->get();
    $new = News::where('slug', $slug)->firstOrFail();
    $newT =News::where('created_at','>',$new->created_at)->orderBy('id', 'desc')->first();
    $newS =News::where('created_at','<',$new->created_at)->orderBy('id', 'desc')->first();
    $comment = Comment::where('news_id', $new->id)->with('children')->whereNull('parent_id')->orderby('id', 'desc')->paginate(5);
    if ($request->ajax()) {
      return view('inc.pagination_child', compact('comment'));
    }
    return view('news-blog', compact('new', 'background', 'userAuth', 'comment', 'footer','newT','newS','new1','new2'));
  }

  public function viewContact()
  {
    $new1 = News::where('status', 1)->orderBy('id', 'desc')->paginate(7);
    $new2 = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);
    $footer = Setting::where('status', 1)->first();
    $userAuth = auth()->user();
    $background = Slide::where('page', 'news')->where('status', 1)->get();
    return view('contact', compact('background', 'userAuth', 'footer','new2','new1'));
  }


  public function contactStore(Request $request)
  {

    $data = $request->validate([
      'email' => 'required',
      'subject' => 'required',
      'messenger' => 'required',
    ]);
    $result=Mail::to('dangcong1810@gmail.com')->send(new ContactMail($data));
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  public function login(Request $request)
  {
    $data = \Request::validate([
      "email" => "required",
      "password" => "required",
    ]);
    $data["status"] = 1;
    if (Auth::attempt($data)) return \Response::success(trans("messages.createSuccess"), true, Response::HTTP_CREATED);
    else return \Response::error(trans("auth.failed"), Response::HTTP_UNAUTHORIZED);
  }

  public function logout(Request $request)
  {
    Auth::logout();
    return redirect()->route('home');
  }

  public function register(Request $request)
  {

    $data = \Request::validate([
      "name" => "required",
      "email" => "unique:users,email",
      "password" => "required",
      "phone" => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
      "gender" => 'required|numeric',
      "address" => 'required|min:6|max:255',
      "identity_card" => 'required|numeric',
      "birthday" => "required",
    ]);
    $data["role_id"] = 2;
    $data["status"] = 1;
    $data["email_verified_at"] = Carbon::now();

    $result = User::create($data);
    Mail::to($data['email'])->send(new MailNotify($data));
    Auth::attempt($data);
    return \Response::success(trans("messages.createSuccess"), $result, Response::HTTP_CREATED);
  }

  public function viewProfile()
  {
    $now = Carbon::now();
    $user = auth()->user();
    $history = History::where('user_id', $user->id)->orderBy('id', 'desc')->get();
    $history1 = History::where('user_id', $user->id)->orderBy('id', 'desc')->get();
    $blood = Blood::with('children')->whereNull('parent_id')->get();
    return view('profile', compact('user', 'blood', 'history', 'history1', 'now'));
  }

  public function updateProfile(Request $request)
  {
    $data = $request->validate([
      "name" => "required",
      "email" => "required",
      "phone" => "required",
      "identity_card" => "required",
      "gender" => "required",
      "birthday" => "",
      "address" => "required",
      "job" => "",
      "blood_id" => "",
      "note" => "",
    ]);
    if ($request->hasFile('avatar')) {
      $imageName = '/storage/'.time() . '.' . $request->avatar->extension();
      $request->avatar->storeAs('public/', time() . '.' . $request->avatar->extension());
      $data['avatar'] = $imageName;
    }

    $user = auth()->user();
    $user->update($data);
    return back();
  }


  public function findBlood()
  {
    $footer = Setting::where('status', 1)->first();
    $new1 = News::where('status', 1)->orderBy('id', 'desc')->paginate(7);
    $new2 = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);
    $organizations = Organization::where('status', 1)->where('featured', 1)->get();
    $background = Slide::where('page', 'news')->where('status', 1)->get();
    $find = Find::where('status', 1)
      ->orderBy('id', 'desc')->paginate(6);

    return view('timmau', compact('find', 'background', 'organizations', 'footer','new1','new2'));
  }

  public function vinhdanh()
  {
    $footer = Setting::where('status', 1)->first();
    $new1 = News::where('status', 1)->orderBy('id', 'desc')->paginate(7);
    $new2 = News::where('status', 1)->orderBy('id', 'desc')->paginate(6);
    $organizations = Organization::where('status', 1)->where('featured', 1)->get();
    $background = Slide::where('page', 'news')->where('status', 1)->get();
    $history = History::select(DB::raw('count(user_id) as id_no'), 'user_id')->groupBy('user_id')->orderBy('id_no', 'desc')->paginate(8);;
    return view('vinhdanh', compact('background', 'history', 'organizations', 'footer','new1','new2'));
  }

  public function resetPassword(ResetPassword $request)
  {

    auth()->user()->update(['password' => $request->get('password')]);

    return back()->withPasswordStatus(__('Password successfully updated.'));
  }

  public function postCommentC(Request $request, $id, $new_id)
  {
    $data = $request->validate([
      "content" => 'required|max:255',
    ]);
    $data['parent_id'] = $id;
    $data['author_id'] = auth()->user()->id;
    $data['news_id'] = $new_id;
    $result = Comment::create($data);
    return back();
  }

  public function postComment(Request $request, $new_id)
  {
    $data = $request->validate([
      "content" => 'required|max:255',
    ]);
    $data['author_id'] = auth()->user()->id;
    $data['news_id'] = $new_id;
    $result = Comment::create($data);
    return back();

  }

  public function blood()
  {
    $blood = Blood::with('children')->get();
    return \response()->json($blood);
  }

  public function hospital()
  {
    $hospital = Hospital::with('children')->get();
    return \response()->json($hospital);
  }

  public function comment(Request $request, $slug)
  {
    if ($request->ajax()){
      $user= auth()->user();
    $new = News::where('slug', $slug)->firstOrFail();
    $comment = Comment::where('news_id', $new->id)->with('children')->whereNull('parent_id')->orderby('id', 'desc')->paginate(5);
      return view('inc.pagination_child', compact('comment','user','new'))->render();
    }
  }

  /**
   * Check slug.
   *
   * @param \App\User $user
   * @return \Illuminate\Http\Response
   */
  public function checkUnique(Request $request)
  {
    $data = $request->all();
    $model = $data["model"];
    unset($data["model"]);
    $check = true;
    $arrayKey = array_keys($data);
    $rows = DB::table($model)->where($arrayKey[0], $data[$arrayKey[0]])->get();
    if (isset($data["id"]) && count($rows) > 0) {
      foreach ($rows as $row) {
        if ($check && $row->id !== $data["id"]) $check = false;
      }
    } else {
      $check = count($rows) === 0;
    }
    if ($check) {
      return \Response::success(null, $check);
    } else {
      return \Response::error();
    }
  }
}
