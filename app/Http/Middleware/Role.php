<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Http\Response;


class Role
{
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   * @param $url
   * @return mixed
   */
  public function handle($request, Closure $next, $url)
  {
    try {
      //Access token from the request
      $token = JWTAuth::parseToken();
      //Try authenticating user
      $user = $token->authenticate();
    } catch (TokenExpiredException $e) {
      //Thrown if token has expired
      return $this->unauthorized('Your token has expired. Please, login again.');
    } catch (TokenInvalidException $e) {
      //Thrown if token invalid
      return $this->unauthorized('Your token is invalid. Please, login again.');
    }catch (JWTException $e) {
      //Thrown if token was not found in the request.
      return $this->unauthorized('Please, attach a Bearer Token to your request');
    }
    //If user was authenticated successfully and user is in one of the acceptable roles, send to next request.0


    if ($user->role_id === 1) {
      return $next($request);
    } else {
      $permissions = \App\Models\Role::find($user->role_id)->attributes()->get();
      $array = $permissions->pluck('permission_route')->toArray();
      $path = $request->path();
      $index = -1;
      foreach ($array as $key => $value) {
        if (strpos($path, $url.$value) !== false) $index = $key;
      }
      if ($index > -1) {
        $method = $request->method() === "PATCH" ? "PUT" : $request->method();
        if (in_array($method, $permissions[$index]->can)) return $next($request);
      }
      return \Response::error(trans("messages.unauthorized"), $index, Response::HTTP_UNAUTHORIZED);
    }
  }
}
