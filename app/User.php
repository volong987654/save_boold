<?php

namespace App;

use App\Models\ListData;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
  use ListData;
  use Notifiable;
  use SoftDeletes;

  protected $guarded = [];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'deleted_at',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  /**
   * Get the identifier that will be stored in the subject claim of the JWT.
   *
   * @return mixed
   */
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims()
  {
    return [];
  }

  public function setPasswordAttribute($value)
  {
    $this->attributes['password'] =  Hash::make($value);
  }

  public function scopeGetData($query, $request)
  {
    return $this->getListData($query, $request);
  }

  public function role()
  {
    return $this->belongsTo(\App\Models\Role::class);
  }

  public function blood()
  {
    return $this->belongsTo(\App\Models\Blood::class);
  }

  public function organization()
  {
    return $this->belongsTo(\App\Models\Organization::class);
  }

  public function history()
  {
    return $this->hasMany("App\Models\History")->orderBy('time', 'desc');
  }

  public function hospitals()
  {
    return $this->belongsToMany("App\Models\Hospital", "user_hospital")->withTimestamps();
  }
  public function defaultImage(){
    $imagePath =($this->avatar) ? $this->avatar :'/storage/1601368436.jpeg';
    return $imagePath;
  }
}
