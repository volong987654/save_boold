@extends('layouts.app')

@section('title', __('client.contact'))
@section('css')

@endsection
@section('content')
  @include('inc.background')
  <div class="uk-section">
    <div class="uk-container">
      <div class="" uk-grid>
        <div class="uk-width-1-2@m ">
          <div class="">
            <h3 class="Leave">
              Liên hệ chúng tôi</h3>
            <p style="font-weight: 400">Hãy để lại thông tin của bạn chúng tôi sẽ liên hệ lại</p>
            <form class="uk-form-stacked" id="formContact">

              <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Họ và tên :</label>
                <div class="uk-form-controls input">
                  <input class="uk-input" id="email1" name="email" type="text" placeholder="Tên của bạn">
                </div>
              </div>
              <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Nghề nghiêp :</label>
                <div class="uk-form-controls input">
                  <input class="uk-input" id="subject" name="subject" type="text" placeholder="Nghề nghiệp của bạn">
                </div>
              </div>
              <div class="uk-inline uk-margin ">
                <label class="uk-form-label" for="form-stacked-text">Tin nhắn:</label>
                <textarea class="uk-input uk-height-small uk-from-xlarge  uk-width-1-1 input"
                          style="min-height: 200px; width: 1000px; background-color:white" name="messenger"
                          id="messenger" type="text" placeholder="Để lại lời nhắn của bạn" required></textarea>
              </div>
              <button type="submit" id="btnContact"
                      class="uk-button uk-button-primary uk-border-rounded buttons snip1582">Gửi
              </button>
            </form>

          </div>
        </div>
        <div class="uk-width-1-2@m">
          <div class="">
            <div class="">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.656848507269!2d107.66658441481287!3d16.44224518865146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a1e4851768b1%3A0x34678cf6bb582722!2zQ2jDoW5oIMSQw7RuZywgVHguIEjGsMahbmcgVGjhu6d5LCBUaOG7q2EgVGhpw6puIEh14bq_LCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1595001218043!5m2!1svi!2s"
                width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                tabindex="0"></iframe>
            </div>
            <div>
              <h3 class="uk-margin-top our">Địa chỉ</h3>
              <p style="font-weight: 400">{!! $footer->address !!}</p>
              <p style="font-weight: 400">Sdt: {!! $footer->phone !!}</p>
              <p style="font-weight: 400">Email: {!! $footer->email !!}</p>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>

@endsection
@section('script')
  <script>
    $(document).ready(function () {
      $('#btnContact').click(function () {
        var formData = new FormData();
        formData.append("email", $("#email1").val());
        formData.append("subject", $("#subject").val());
        formData.append("messenger", $("#messenger").val());
        let request = $.ajax({
          url: '{{route('contact.store')}}',
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            if (response) {
              UIkit.notification(response.message, {status: 'success'})
              location.reload();
              window.location.reload();
            }
          }
        });
        return false;
      });
    });


  </script>
@endsection
