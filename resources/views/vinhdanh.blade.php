@extends('layouts.app')
@section('title', __('client.vinhdanh'))
@section('css')

@endsection
@section('content')
  <!-- vinhdanh -->
  <div class="uk-section">
    <div class="uk-text-center">
      <h1 class="title1 uk-margin-large">Tôn vinh những cá nhân xuất sắc</h1>
      <h2 class="title uk-text-bold uk-margin-large">Có số lần hiến máu cao nhất</h2>
      <h3> Được save blood ghi nhận</h3>

      </h6>
    </div>
    <div class="uk-container">
      <div class="uk-grid-match uk-grid-small uk-text-center uk-margin" uk-grid>
        @foreach($history as  $history)
          <div class="uk-width-1-4@m">
            <div class="hovereffect">
              <img class="img-responsive" src="{{$history->user->defaultImage() }} " alt="">
              <div class="overlay">
                <h2>{{$history->user->name}} </h2>
                <a class="info" href="#"> Có số lần hiến máu là {{$history->id_no}} </a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>

  </div>
  <!-- end off vinhdanh -->

@endsection
