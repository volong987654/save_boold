<div class="form-home">
  <div class="uk-position-relative uk-visible-toggle " data-uk-height-viewport="expand: true" tabindex="-1" data-uk-slideshow="ratio: 10:3; animation: fade; autoplay: true; autoplay-interval: 5000; pause-on-hover: true;">
    <ul class="uk-slideshow-items uk-light">
      @foreach($background as $background)
      <li>
        <div class="uk-position-cover uk-animation-reverse uk-transform-origin-center-left">
        <img src="{!!  $background->image !!}" alt="event" uk-cover>
      <div class="uk-position-bottom-left uk-position-small uk-light uk-margin-large-left uk-margin-large-bottom">
        <h1 data-uk-scrollspy="cls: uk-animation-scale-up; delay: 1000; repeat: true;" class="uk-visible@s  uk-margin-auto uk-w400 uk-text-bold">{!!  $background->title !!}</h1>
        <p data-uk-scrollspy="cls: uk-animation-slide-bottom; delay: 2000; repeat: true;" class="uk-hidden-small  uk-visible@s" >{!!  $background->text !!}</p>
    </div>
  </div>
      </li>
      @endforeach
    </ul>
    <a class="uk-position-center-left uk-position-small uk-hidden-hover uk-visible@m" href="#" data-uk-slidenav-previous data-uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover uk-visible@m" href="#" data-uk-slidenav-next data-uk-slideshow-item="next"></a>
  </div>
</div>
