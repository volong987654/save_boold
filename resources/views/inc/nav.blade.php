
<div id="navbar-scroll" class="background-home">
  <!-- Menu -->
  <nav class="uk-navbar-container" data-uk-navbar="boundary-align: true; align: center;">
    <div class="uk-navbar-left">
      <ul class="uk-navbar-nav">
        <li>
          <a id="imgLogo" class="uk-navbar-item uk-logo uk-margin-medium-left" href="{{route('home')}}" style="display: none">
            <img src="{{asset('images/trang250x100.png')}}" style="width: 100px;max-width: 100px">
          </a>
        </li>
        <li>
          <a id="textLogo" href="{{route('home')}}">
            <p class="uk-text-bold uk-text-danger text-logo">Save Blood</p>
          </a>
        </li>
      </ul>
    </div>
    <div class="uk-navbar-right uk-dark">
      <ul class="uk-navbar-nav uk-margin-medium-right">

        <li>
          <a class="uk-visible@l link" @if(url()->current()!=route('home')) href="{{route('home')}}" @endif  >Trang Chủ</a>
        </li>
        <li>
        <a class="uk-visible@l link"  @if(url()->current()!=route('find.blood')) href="{{route('find.blood')}}" @endif >Tìm Máu</a>
        </li>
        <li>
          <a class="uk-visible@l link"  @if(url()->current()!=route('news')) href="{{route('news')}}" @endif>Tin Tức</a>
        </li>
        <li>
          <a class="uk-visible@l link" @if(url()->current()!=route('vinhdanh')) href="{{route('vinhdanh')}} @endif" >Vinh Danh</a>
        </li>
        <li>
          <a class="uk-visible@l link" @if(url()->current()!=route('contact')) href="{{route('contact')}} @endif">Liên Hệ</a>
        </li>
{{-- cái ni là cái phần icon mặt người--}}
        @if (Auth::check())
          <li>
            <a class="uk-navbar-toggle" href="#" data-uk-toggle>
              <i class="las la-user la-lg"></i>
            </a>
            <div data-uk-dropdown="pos: bottom-right">
              <ul class="uk-nav uk-navbar-dropdown-nav">
                <li>
                  <div class="uk-grid-small uk-flex uk-flex-middle" data-uk-grid>
                    <div class="uk-width-auto@m">
                      <img class="uk-border-circle lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{!! Auth::user()->defaultImage()  !!}" width="50" height="50" style="height: 50px" alt="avatar">
                    </div>
                    <div class="uk-width-auto@m">
                      {{Auth::user()->name}}<br />
                      {{Auth::user()->email}}
                    </div>
                  </div>
                </li>
                <hr>
                <li>
                  <a href="/profile">Profile</a>
                </li>
                <li>
                  <a href="/logout">Logout</a>
                </li>
              </ul>
            </div>
          </li>
          {{-- để yên--}}
        @else
        <li >
          <a class="link">Đăng nhập</a>
          <div id="login-form" data-uk-dropdown="pos: bottom-justify; animation: uk-animation-slide-top-small; duration: 1000;delay-hide: 100">
            <ul data-uk-tab="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium" class="from1 uk-flex uk-flex-center">
              <li class="link2" ><a href="#">Đăng Nhập</a></li>
              <li class="link2"><a href="#">
                  <div class="uk-text-center uk-width-1-1">
                      <div type="button" uk-toggle="target: #modal-close-default1 ">Đăng Kí</div>
                  </div>
                  <div id="modal-close-default1" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                      <button class="uk-modal-close-default" type="button" uk-close></button>
                      <h2 class="uk-modal-title uk-text-center" style="font-size: 50px;font-weight: 400;  font-family: var(--heading-font); ">Tạo Tài Khoản</h2>
                      <hr class="uk-text-secondary">
                        <form  name="formRegister" class="uk-grid-small uk-padding-small"  >
                          <div class="uk-grid-small" >
                              <div class="uk-margin" uk-grid>
                                  <div class="uk-width-1-2@s error-nameNav">
                                    <input class="uk-input uk-input2" type="text" placeholder="Họ Tên" name="name"
                                    pattern="[^*+&%$#@!~()0-9/><\][\\\x22,;|]+"
                                    data-parsley-minlength="6"
                                    data-parsley-errors-container=".error-nameNav"
                                    title="Họ Tên Bạn" uk-tooltip  required>
                                </div>
                                <div class="uk-width-1-3@s">
                                  <select name="gender" class="uk-select uk-input2" placeholder="Giới tính">
                                    <option value="1">Nam</option>
                                    <option value="2">Nữ</option>
                                  </select>
                                </div>
                              </div>
                              <div class="uk-margin" uk-grid>
                                <div class="uk-width-1-2@s error-email-nav">
                                  <input class="uk-input uk-input2 " name="email" type="email" placeholder="Email"
                                  data-parsley-errors-container=".error-email-nav"
                                  title="Địa Chỉ Email" uk-tooltip  required   data-parsley-remote="users">
                                </div>
                                <div class="uk-width-1-2@s">
                                  <div class="uk-form-controls error-passNav">
                                    <input class="uk-input uk-input2 "  type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" minlength="6" required
                                    name="password"
                                    data-parsley-errors-container=".error-passNav"
                                    title="Mật khẩu gồm có ít nhất 1 kí tự in hoa và in thường,độ dài lớn hơn 6" uk-tooltip
                                    placeholder="Mật Khẩu" />
                                  </div>
                                </div>

                              </div>
                            <div class="uk-margin" uk-grid>
                              <div class="uk-width-1-2@m">
                                <div class="uk-form-controls error-cmndNav" >
                                    <input class="uk-input uk-input2"type="text"
                                    data-parsley-minlength="9"
                                    data-parsley-maxlength="12"
                                    data-parsley-type="number"
                                    name="identity_card"
                                    data-parsley-errors-container=".error-cmndNav"
                                    placeholder="CMND / CCCD" title="CMND / CCCD" uk-tooltip required />
                                </div>
                              </div>

                              <div class="uk-width-1-2@m">
                                <div class="uk-form-controls uk-inline error-phoneNav">
                                  <span class="uk-form-icon" style="max-height: 40px;">
                                      <i class="las la-phone la-lg"></i>
                                  </span>
                                  <input class="uk-input uk-input2" name="phone" type="phone"
                                       placeholder="Nhập Số Điện Thoại"
                                       data-parsley-minlength="9"
                                      data-parsley-errors-container=".error-phoneNav"
                                      title="Số Điện Thoại" uk-tooltip
                                      data-inputmask="'mask': '+(99) 9999-9999{1,3}'" required />
                                </div>
                              </div>
                            </div>
                            <div class="uk-margin" uk-grid>
                              <div class="uk-width-1-2@m error-addressNav">
                                <input class="uk-input uk-input2" name="address" placeholder="Địa chỉ"
                                data-parsley-errors-container=".error-addressNav"
                                title="Địa Chỉ" uk-tooltip required>
                              </div>
                              <div class="uk-width-1-2@m">
                                <div class="uk-inline  ">
                                  {{-- Để Tree-Select Vào luôn Nhé <3 :) --}}
                                  <span class="uk-form-icon" style="max-height: 40px;">
                                      <i class="las la-calendar la-lg"></i>
                                  </span>
                                  <input class="uk-input uk-input2"
                                    placeholder="Ngày Sinh" id="birthday"
                                    name="birthday"
                                    style="cursor: pointer;"
                                         type="datetime-js"
                                    required />
                              </div>
                              </div>

                            </div>

                            <div class="uk-width-1-1 uk-text-right uk-margin-large-top">
                              <hr class="uk-text-secondary">
                              <button class="uk-button uk-button-small uk-border-rounded uk-button-default uk-modal-close" type="button">Hủy</button>
                              <button class="uk-button uk-button-small uk-margin-left uk-border-rounded uk-button-primary" type="submit">Đăng kí</button>
                            </div>

                          </div>
                        </form>
                    </div>
                  </div>
                  </a>
              </li>

            </ul>
            <ul class="uk-switcher">
              <li>
                <form name="formLogin">
                  <div uk-grid>
                    <div class="uk-width-1-1 error-email-login">
                      <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon">
                          <i class="las la-envelope la-lg"></i>
                        </span>
                        <input class="uk-input uk-input2" name="email" type="email" placeholder="Email Address"
                        data-parsley-errors-container=".error-email-login" required>
                      </div>
                    </div>
                    <div class="uk-width-1-1 error-password-login">
                      <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon">
                          <i class="las la-lock la-lg"></i>
                        </span>
                        <input class="uk-input uk-input2" name="password" type="password" placeholder="Password"
                        data-parsley-errors-container=".error-password-login" required>
                      </div>
                    </div>
                    <div class="uk-width-1-1">
                        <i>
                          <a href="#" class="uk-button uk-button-text uk-text-decoration uk-text-primary" uk-toggle="target: #modal-forgetPass">Quên Mật Khẩu ?</a>
                        </i>
                    </div>

                    <div class="uk-width-1-1 uk-text-center">
                      <button type="submit" class="uk-button uk-button-small uk-button-primary uk-border-rounded">Login</button>
                    </div>
                  </div>
                </form>
                          {{-- modal Forget Pass --}}
                          <div id="modal-forgetPass" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                              <button class="uk-modal-close-default" type="button" uk-close></button>
                              <h2 class="uk-modal-title uk-text-center" style="font-size: 50px;font-weight: 400;  font-family: var(--heading-font); ">Quên Mật Khẩu</h2>
                              <hr class="uk-text-secondary">
                                <form action="#" class="uk-grid-small uk-padding">
                                      <div class="uk-margin-large" uk-grid>
                                        <div class="uk-width-1-2@s">
                                          <label class="uk-form-label" for="form-emailForget">Email</label>
                                          <input class="uk-input uk-input2 " name="email" type="email" placeholder="Email" title="Địa Chỉ Email" uk-tooltip data-parsley-errors-container=".error-email-register"  required>
                                        </div>
                                        <div class="uk-width-1-2@m">
                                          <label class="uk-form-label" for="form-phoneForget">Phone</label>
                                          <div class="uk-form-controls uk-inline .error-phoneForget">
                                            <span class="uk-form-icon" style="max-height: 40px;">
                                                <i class="las la-phone la-lg"></i>
                                            </span>
                                            <input class="uk-input uk-input2" name="phone" type="phone"
                                                data-parsley-errors-container=".error-phoneForget" placeholder="Nhập Số Điện Thoại" title="Số Điện Thoại" uk-tooltip
                                                data-inputmask="'mask': '+(99) 9999-9999{1,3}'" required />
                                          </div>
                                        </div>
                                      </div>
                                    <div class="uk-width-1-1 uk-text-right uk-margin-large-top">
                                      <hr class="uk-text-secondary">
                                      <button class="uk-button uk-button-small uk-border-rounded uk-button-default uk-modal-close" type="button">Hủy</button>
                                      <button class="uk-button uk-button-small uk-margin-left uk-border-rounded uk-button-primary" type="submit">Đăng kí</button>
                                    </div>
                                </form>
                          </div>
                        </div>
              </li>
            </ul>
          </div>
        </li>
        @endif
        {{-- để yêu--}}
       {{-- donate--}}
        <li>
          <a href="#"><button class="uk-button uk-button-medium uk-border-rounded uk-button-danger uk-visible@s">Donate</button></a>
        </li>
        {{--end off donate--}}

          <div class="uk-navbar-right uk-hidden@l">
            <a class="uk-navbar-toggle" data-uk-navbar-toggle-icon data-uk-toggle="target: #offcanvas-nav"></a>
          </div>
        </li>
      </ul>
    </div>

  </nav>
</div>
<div id="offcanvas-nav" data-uk-offcanvas="mode: push; overlay: true; esc-close: true; flip: true;">
  <div class="uk-offcanvas-bar uk-text-while uk-flex uk-flex-middle">
      <ul class="uk-nav-default uk-nav-parent-icon " data-uk-nav>
        <li>
          <a href="#"><button class="uk-button uk-button-medium uk-border-rounded uk-button-danger">Donate</button></a>
        </li>
        <li><a href="#"><img class="uk-border-rounded" src="/images/savebold.PNG" alt="avatar"></a></li>
        <li class="uk-margin-top"><a href="{{route('home')}}">Trang Chủ</a></li>
        <li><a href="{{route('find.blood')}}">Tìm Máu</a></li>
        <li><a href="{{route('news')}}">Tin Tức</a></li>
        <li><a href="{{route('vinhdanh')}}" >Vinh Danh</a></li>
        <li>
          <a href="{{route('contact')}}">Liên Hệ</a>
        </li>
        <div>
            <p class="uk-text-center uk-padding-large uk-margin-top">copyright © 2020 off all rights reserved</p>
        </div>
      </ul>
  </div>
</div>
@section("script-header")
  <script>
    $(document.formLogin).parsley({
      trigger: "change",
      errorClass: "uk-form-danger",
      errorsWrapper: "<div class=\"invalid-message\"></div>",
      errorTemplate: "<span class='uk-text-danger'></span>",
    })
      .on("form:submit",(e) => {
        const data = {};
        for (let i = 0; i < e.fields.length; i++) {
          data[e.fields[i].element.name] = e.fields[i].value;
        }
        jQuery.ajax({
          method: "POST",
          url: "/login",
          data: JSON.stringify(data),
          success: function (response) {
            if (response.data) {
              UIkit.notification(response.message, {status:'success', pos: "top-right"})
              location.reload();
            }
          }
        });
        return false;
      });
    $(document.formRegister).parsley({
      trigger: "change",
      errorClass: "uk-form-danger",
      errorsWrapper: "<div class=\"invalid-message\"></div>",
      errorTemplate: "<span class='uk-text-danger'></span>",
    })
      .on("form:submit",(e) => {
        const data = {};
        for (let i = 0; i < e.fields.length; i++) {
          data[e.fields[i].element.name] = e.fields[i].value;
        }
        jQuery.ajax({
          method: "POST",
          url: "/register",
          data: JSON.stringify(data),
          success: function (response) {
            if (response.data) {
              UIkit.notification(response.message, {status:'success'})
              location.reload();
            }
          }
        });
        return false;
      });

  </script>
  <script>
    $(document).ready(function () {
      $(window).scroll(function(){
        let position = $(this).scrollTop();

        if(position >= 10){
            $('#navbar-scroll').addClass('custom-navbar');
            $('#imgLogo').show();
            $('#textLogo').hide();
        } else{
            $('#navbar-scroll').removeClass('custom-navbar');
            $('#textLogo').show();
            $('#imgLogo').hide();
        }
    });
     })
  </script>
@endsection
<!-- End Menu -->
