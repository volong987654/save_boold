<?php $i = 0 ?>
@foreach( $comment as  $comments)
  <li>
    <article class="uk-comment uk-visible-toggle" tabindex="-1">
      <header class="uk-comment-header uk-position-relative">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
          <div>
            <img class="uk-comment-avatar uk-border-rounded" width="80"
                 src="{!! $comments->user->avatar !!}">
          </div>
          <div class="uk-width-expand">
            <h4 class="uk-comment-title uk-margin-remove uk-w600"><a
                class="uk-link-reset" href="#">{!! $comments->user->name !!}</a></h4>
            <p class="uk-comment-meta uk-margin-remove-top"><a
                class="uk-link-reset"
                href="#">{!! $comments->created_at->format('h A ,F Y') !!}</a></p>
          </div>
        </div>
        @if(Auth::check())
          <div class="uk-position-top-right uk-position-small uk-hidden-hover rep"><a
              class="uk-link-muted" href="#"
              uk-toggle="target: #toggle-usage{!!  $comments->id+$i+1 !!};animation: uk-animation-fade;cls: false"><i class="las la-share"></i></a>
          </div>
        @endif
      </header>
      <div class="uk-comment-body uk-padding-small">
        <p class="uk-text-secondary">{!!  $comments->content !!}</p>
      </div>
    </article>
    <ul>
      <li>

        <form method="post" action="{{route('comment',[$comments,$new->id])}}"
              id="toggle-usage{!!  $comments->id+$i+1 !!}" hidden="true">
          @csrf <textarea name="content" type="message" class="uk-input uk-input2 uk-border-rounded"
                          placeholder="Viết Bình Luận"
                          style="min-height: 100px;" required></textarea>
          <button class="uk-button uk-button-small uk-btn uk-margin uk-border-rounded"
                  @if (!Auth::check()) uk-toggle="target: #modal-close-default1"
                  @else type="submit" @endif>Bình Luận
          </button>
        </form>
        @if($comments->children)
          @foreach($comments->children as $child)
            <article class="uk-comment uk-visible-toggle" tabindex="-1">
              <header class="uk-comment-header uk-position-relative">
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                  <div>
                    <img class="uk-comment-avatar uk-border-rounded" width="80"
                         src="{!! $child->user->avatar !!}">
                  </div>
                  <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove uk-w600"><a
                        class="uk-link-reset" href="#">{!! $child->user->name !!}</a></h4>
                    <p class="uk-comment-meta uk-margin-remove-top"><a
                        class="uk-link-reset"
                        href="#">{!! $child->created_at->format('h A ,F Y') !!}</a></p>
                  </div>
                </div>

              </header>
              <div class="uk-comment-body uk-padding-small">
                <p>{!!  $child->content !!}</p>
              </div>

            </article>
          @endforeach
        @endif
        <form id="toggle-usage1" hidden="true">
                                <textarea name="content" type="message" class="uk-input uk-input2 uk-border-rounded"
                                          placeholder="Viết Bình Luận"
                                          style="min-height: 150px;" required></textarea>
          <button class="uk-button  uk-btn uk-button-small uk-margin uk-border-rounded"
                  @if (!Auth::check()) uk-toggle="target: #modal-close-default1"
                  @else type="submit" @endif>Bình Luận
          </button>
        </form>
      </li>
    </ul>
  </li>
@endforeach
<div class="uk-align-center uk-flex uk-flex-center uk-width-1-1" id="navigator-new">
{{--  {!! $comment->appends(['sort' => 'department'])->links() !!}--}}
  {{ $comment->links('vendor.pagination.bootstrap-4') }}
</div>
