<fotter>
  <div class="uk-section section ">
    <div class="uk-container ">
      <div class="uk-grid-match uk-grid-small uk-text-center uk-text-left@s uk-text-left" uk-grid>

        <div class="uk-width-1-4@m">

          <h3 class="h_header uk-text-left"> Save Blood<span class="textd uk-text-left">D</span></h3>
          <p class="uk-text-left heading">
            {!! $footer->details !!}
          </p>
          <ul class="heading1">

            <a href="{!! $footer->facebook !!}"><li><i class="lab la-facebook-f uk-text-left" style="font-size:24px;"></i></li></a>
            <a href="{!! $footer->ins !!}"><li><i class="lab la-instagram" style="font-size:24px;"></i></li></a>



          </ul>

        </div>
        <div class="uk-width-1-4@m">
          <h3 class="header uk-text-left">Bài viết gần đây</h3>
          <ul class="uk-text-left  header1 ">
            @foreach($new1 as $new1)
              <li><a href="{{route('news.tintuc',$new1->slug)}}">{!! $new1->name !!}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="uk-width-1-4@m">
          <h3 class="header uk-text-left">Hoạt động</h3>

          <div class="uk-child-width-1-3 uk-grid-small > " uk-grid>
            @foreach($new2 as $new2)
            <div class="uk-text-center header2">
              <div class="single-speaker-inner ">
                <div class="thumb">
                  <img src="{!! $new2->image  !!}" alt="img">
                </div>
                <div class="details">
                  <li><i class="las la-tint" style="font-size:24px; color:white"></i></li>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="uk-width-1-4@m">
          <h3 class="titlelienhe uk-text-left">Liên hệ với chúng tôi </h3>
          <iframe
            src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fclbit.edu%2F&tabs=272&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
            width="340" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
            allowTransparency="true" allow="encrypted-media"></iframe>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom uk-text-center section">
    <div class="uk-container">
      <p>Sản phẩm do CLB Tin học Đại học sư phạm Huế thi công</p>
    </div>
  </div>
</fotter>


<!-- off footer -->
