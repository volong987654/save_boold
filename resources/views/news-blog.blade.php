@extends('layouts.app')
@section('title', __('client.news'))
@section('css')
@endsection
@section('content')
  <div>
    <!-- icon scroll top -->
    <div id="scrollTop">
    </div>
    @include('inc.background')
    {{-- off scroll --}}
    <div class="uk-section-default uk-section">
      <div class="uk-grid-margin uk-container">
        <div class="tm-grid-expand uk-child-width-1-1 uk-grid uk-grid-stack" uk-grid="">
          <div class="uk-width-1-1@m uk-first-column">
            <div class="uk-height-large uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light uk-background-center-center" data-src="{!! $new->image !!}" uk-img>
            </div>
          </div>
        </div>
      </div>
      <div class="uk-margin-large uk-container uk-container-xsmall">
        <div class="tm-grid-expand uk-child-width-1-1 uk-grid uk-grid-stack" uk-grid="">
          <div class="uk-width-1-1@m uk-first-column">
            <h1 class="uk-margin-remove-vertical uk-text-center heading-color" > {!! $new->name !!}    </h1>
            <div class="uk-panel uk-margin uk-text-center uk-text-danger" ><time datetime="{!! $new->created_at !!}">{!! $new->created_at->toFormattedDateString(); !!}</time></div>
            <div class="uk-panel uk-margin-medium">
              <p class="detail">{!! $new->body !!}</p>
            </div>
            {{--            pagination--}}
            <div class="uk-margin-large uk-text-center">
              <ul class="uk-pagination uk-margin-remove-bottom uk-flex-center" uk-margin="">
                @if(isset($newS))
                <li class="uk-margin-auto-right uk-first-column blog-checker">

                  <a href="{{route('news.tintuc',$newS->slug)}}">
                          <span uk-pagination-previous="" class="uk-icon uk-pagination-previous">
                             <svg width="7" height="12" viewBox="0 0 7 12" xmlns="http://www.w3.org/2000/svg" data-svg="pagination-previous">
                                <polyline fill="none" stroke="#000" stroke-width="1.2" points="6 1 1 6 6 11"></polyline>
                             </svg>
                          </span>
                    {!! $newS->name !!}
                  </a>

                </li>
                @endif
                  @if(isset($newT))
                <li class="uk-margin-auto-left blog-checker">
                  <a href="{{route('news.tintuc',$newT->slug)}}">
                    {!! $newT->name !!}

                    <span uk-pagination-next="" class="uk-icon uk-pagination-next">
                             <svg width="7" height="12" viewBox="0 0 7 12" xmlns="http://www.w3.org/2000/svg" data-svg="pagination-next">
                                <polyline fill="none" stroke="#000" stroke-width="1.2" points="1 1 6 6 1 11"></polyline>
                             </svg>
                          </span>
                  </a>
                </li>
                  @endif
              </ul>
            </div>
            {{--            pagination--}}
            <div class="uk-panel uk-margin-remove-first-child uk-margin">
              <div class="uk-child-width-expand uk-grid-column-medium uk-flex-middle uk-grid" uk-grid="">
                <div class="">
                  <div class="uk-card uk-card-body uk-card-small uk-margin-large-top">
                    <p class="uk-text-bold uk-text-secondary">{!! $comment->count() !!} bình luận</p>
                    <div class="uk-divider-small"></div>
                    @csrf
                    <ul class="uk-comment-list" id="table_data">
                      @include('inc.pagination_child')
                    </ul>
                    <form method="Post" action="{{route('commentnull',$new->id)}}" class="uk-margin-large-top">
                      @csrf
                      <legend class="uk-text-uppercase">Bình Luận Bài Viết</legend>
                      <div class="uk-margin" uk-grid>
                        <div class="uk-margin uk-width-1-1">
                                       <textarea type="message" name="content"
                                                 class="uk-input uk-input2 uk-border-rounded"
                                                 placeholder="Viết Bình Luận"
                                                 style="min-height: 150px;" required></textarea>
                        </div>
                      </div>
                      <button class="uk-button uk-padding-small uk-border-rounded uk-btn "
                              @if (!Auth::check()) uk-toggle="target: #modal-close-default1" @else type="submit" @endif
                      >Đăng Bình
                        Luận
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  </div>
@endsection
@section('script')
  <script type="text/javascript">
    $(window).on('hashchange', function () {
      if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
          return false;
        } else {
          getData(page);
        }
      }
    });

    $(document).ready(function () {
      $(document).on('click', '.pagination a', function (event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getData(page);
      });

    });

    function getData(page) {
      $.ajax(
        {
          url: '?page=' + page,
          type: "get",
          datatype: "html"
        }).done(function (data) {
        $("#tag_container").empty().html(data);
        location.hash = page;
      }).fail(function (jqXHR, ajaxOptions, thrownError) {
        alert('No response from server');
      });
    }
  </script>
  <script>
    var a = document.body.setAttribute("onscroll", "myFunction()");
    let sTop = document.getElementById("scrollTop");
    const icont = document.getElementById("icon-top");

    function myFunction() {
      500 <= window.pageYOffset ?
        (sTop.setAttribute('class', 'button-top'), sTop.innerHTML =
          '<a class="uk-text-while uk-link uk-link-toggle uk-position-absolute icon-toggle " id="icon-top" href="#" uk-totop uk-scroll></a>',
          sTop.setAttribute("style", "transition: all .2s")) :
        (sTop.removeAttribute("class"), sTop.innerHTML = '');
    };
  </script>
@endsection

