
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
  <link rel="shortcut icon" href="{{asset('images/savebold.png')}}">
  <link rel="stylesheet" href="{{ asset('styles/vendor-form.css') }}">
  <link rel="stylesheet" href="{{ asset('styles/vendor-extra.css') }}">
  <link rel="stylesheet" href="{{ asset('styles/main.css') }}">


  @yield('css')
</head>

<body>
@include('inc.loadding')
@yield('content')


<script src="{{ asset('scripts/vendor.js') }}"></script>
<script src="{{ asset('scripts/vendor-form.js') }}"></script>
<script src="{{ asset('scripts/vendor-extra.js') }}"></script>
<script src="{{ asset('scripts/main.js') }}"></script>
<script>
  document.addEventListener("DOMContentLoaded", (event) => {
    $.ajaxSetup({
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
      },
      dataType: "json",
      crossDomain: true,
      contentType: "application/json",
      error: function (request) {
        if (request.responseJSON.message) UIkit.notification(request.responseJSON.message, {status: "danger", pos: "top-right"})
      }
    });
    const loading = $('#loading-ajax');
    $(document).ajaxSend(function(event, jqXHR, settings) {
      loading.show();
    }).ajaxComplete(function(event, jqXHR, settings) {
      loading.hide();
    });
    Parsley.setLocale("{{app()->getLocale()}}");
    Parsley.addValidator('remote', {
      validateString: async (value, model, data) => {
         return  await $.ajax({
          method: "POST",
          url: "/checkUnique",
          data: JSON.stringify({model, [data.element.name]: value}),
        });
      },
      messages: {
        en: 'This value has been used',
        vi: 'Giá trị này đã được sử dụng',
        ja: 'この値は使用されています'
      }
    });

    jQuery.datetimepicker.setLocale("{{app()->getLocale()}}");

  })
</script>
@yield('script-header')
@yield('script')
</body>
</html>
