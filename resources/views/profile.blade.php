@extends('layouts.layoutNotFooter')
@section('title', __('client.profile'))
@section('css')

@endsection
@section('content')

  <div class="background-home form-profile" style="min-height: 100vh">
    @include('inc.nav')
    <hr class="uk-margin-remove">
    <div class="uk-section uk-section-small uk-background-muted">
      <div class="uk-container">
        <div class="page uk-background-default uk-border-rounded card-1" uk-grid>
          {{-- switcher --}}
          <div class="uk-width-1-4@m uk-border-color uk-padding-remove-left">
            <ul class="uk-nav uk-nav-default"
                uk-switcher="connect: #component-nav; animation: uk-animation-fade">
              <li class="item-nav">
                <a class="uk-margin-left" href="#">Chỉnh sửa trang cá nhân</a>
              </li>
              <li class="item-nav">
                <a class="uk-margin-left" href="#">Đổi mật khẩu</a>
              </li>
              <li class="item-nav">
                <a class="uk-margin-left" href="#">Lịch sử hiến máu</a>
              </li>
            </ul>
          </div>
          <div class="uk-width-3-4@m">
            <ul id="component-nav" class="uk-switcher">
              <li>
                <form name="formProfile" id="formProfile" class="uk-form-stacked uk-padding uk-padding-remove-left"
                      method="post" action="{{route('profile.update')}}" enctype="multipart/form-data"
                      autocomplete="off">
                  @csrf
                  @method('put')
                  <div class="uk-border-bottom uk-padding-small uk-padding-remove-horizontal">
                    <div class="uk-grid-small uk-flex-middle"  uk-grid>
                      <div uk-form-custom uk-lightbox="animation: fade"
                           class="uk-upload uk-width-auto@m uk-width-1-1@s uk-flex uk-flex-center uk-position-relative uk-text-left uk-text-center@s" >
                          <a class="uk-inline" href="{!! $user->defaultImage()  !!}">
                            <img id="img-avatar" alt="avatar"
                            class=" uk-border-circle card-1"
                            width="70" height="70" src="{!! $user->defaultImage()  !!}" style="height: 70px;"/>
                          </a>
                        <i class="las la-camera uk-position-absolute uk-border-circle icon-img"
                           tabindex="-1" title="Đổi ảnh đại diện"
                           uk-tooltip="pos: bottom-left"><input type="file" name="avatar"/></i>
                      </div>
                      <div
                        class="uk-width-expand@m uk-width-1-1@s uk-flex uk-flex-left@m uk-flex-center uk-text-center">
                        <h3 class="uk-card-title uk-margin-remove-bottom ">
                          {{ old('name', $user->name)}}
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="uk-margin " uk-grid>
                    <div class="uk-width-3-5@m">
                      <label class="uk-form-label" for="form-name">Họ Và Tên</label>
                      <div class="uk-form-controls error-nameProfile">
                        <input class="uk-input uk-input2" id="name" type="text"
                               name="name" value="{{ old('name', $user->name)}}"
                               data-parsley-errors-container=".error-nameProfile"
                               pattern="[^*+&%$#@!~()0-9/><\][\\\x22,;|]+"
                               data-parsley-minlength="6"
                               placeholder="Nhập Họ Tên" required/>
                      </div>
                    </div>
                    <div class="uk-width-2-5@m uk-width-2-3@s">
                      <label class="uk-form-label" for="form-sex">Giới Tính</label>
                      <div class="uk-form-controls">
                        <select class="uk-select uk-input2" name="gender" id="gender">
                          <option value="1" @if(Auth::check()) @if(Auth::user()->gender==1) selected @endif @endif>Nam
                          </option>
                          <option value="2" @if(Auth::check()) @if(Auth::user()->gender==2) selected @endif @endif>Nữ
                          </option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="uk-margin " uk-grid>
                    <div class="uk-width-1-3@m">
                      <label class="uk-form-label" for="form-Phone">Số Điện Thoại</label>
                      <div class="uk-form-controls uk-inline error-phoneProfile">
                                            <span class="uk-form-icon" style="max-height: 40px;">
                                                <i class="las la-phone la-lg"></i>
                                            </span>
                        <input class="uk-input uk-input2" name="phone" type="phone" id="phone"
                               data-parsley-minlength="9"
                               type="phone"
                               data-parsley-errors-container=".error-phoneProfile"
                               value="{{ old('phone', $user->phone)}}"
                               data-inputmask="'mask': '+(99) 9999-9999{1,3}'" required/>
                      </div>
                    </div>

                    <div class="uk-width-1-3@m  error-timeProfile">
                      <label class="uk-form-label" for="form-born">Ngày Sinh</label>
                      <div class="uk-inline  ">
                                            <span class="uk-form-icon" style="max-height: 40px;">
                                                <i class="las la-calendar la-lg"></i>
                                            </span>
                        <input class="uk-input uk-input2"
                               placeholder="Time" data-parsley-errors-container=".error-timeProfile" id="birthday"
                               value="{!!old('birthday',Carbon\Carbon::parse($user->birthday)->format('Y-m-d'))!!}"
                               name="birthday"
                               style="cursor: pointer;"/>
                      </div>
                    </div>
                    <div class="uk-width-1-3@m uk-width-1-3@s error-bornProfile">
                      <label class="uk-form-label" for="form-cmnd">CMND / CCCD</label>
                      <div class="uk-form-controls">
                        <input class="uk-input uk-input2" id="identity_card" type="text"
                               data-parsley-minlength="9"
                               data-parsley-maxlength="12"
                               data-parsley-type="number"
                               name="identity_card" data-parsley-errors-container=".error-bornProfile"
                               value="{{ old('identity_card', $user->identity_card)}}" required/>
                      </div>
                    </div>
                  </div>
                  <div class="uk-margin " uk-grid>
                    <div class="uk-width-1-3@m error-adressProfile">
                      <label class="uk-form-label" for="form-address">Địa Chỉ</label>
                      <div class="uk-form-controls">
                        <input class="uk-input uk-input2" id="address" type="text"
                               name="address" data-parsley-errors-container=".error-adressProfile"
                               value="{{ old('address', $user->address)}}" required/>
                      </div>
                    </div>
                    <div class="uk-width-1-3@m error-jobProfile">
                      <label class="uk-form-label" for="form-job">Nghề Nghiệp</label>
                      <div class="uk-form-controls">
                        <input class="uk-input uk-input2" id="job" type="text"
                               data-parsley-errors-container=".error-jobProfile"
                               name="job" value="{{ old('job', $user->job)}}"/>
                      </div>
                    </div>
                    <div class="uk-width-1-3@m error-hp">
                      <label class="uk-form-label" for="form-hp">Nhóm máu</label>
                      <div class="uk-form-controls">
                        <input class="uk-input "
                               name="blood_id" value="{{ old('blood_id', $user->blood_id)}}" id="blood_id"
                               type="tree-select" placeholder="Chọn"
                               data-parsley-errors-container=".error-hp">
                      </div>
                    </div>
                  </div>
                  <div class="uk-margin " uk-grid>
                    <div class="uk-width-1-2@m error-emailProfile">
                      <label class="uk-form-label" for="form-Email">Email</label>
                      <div class="uk-form-controls">
                        <input class="uk-input uk-input2" id="email" name="email" type="email"
                               value="{{ old('email', $user->email)}}" title="Email Không Được Thay Đổi" uk-tooltip
                               data-parsley-errors-container=".error-emailProfile"
                               readonly/>
                      </div>
                    </div>

                  </div>
                  <div class="uk-margin ">
                    <label class="uk-form-label" for="form-note">Ghi Chú Của Bác Sĩ:
                    </label>
                    <div class="uk-form-controls">
                                        <textarea name="note" id="note" rows="5" class="uk-textarea uk-input2"
                                                  style="cursor: none" readonly>{{ old('note', $user->note)}}</textarea>
                    </div>
                  </div>
                  <div class="uk-text-right@m uk-text-center@s">
                    <button
                      class="uk-flex-end uk-button uk-button-small uk-btn uk-border-rounded"
                      type="submit"
                      onclick="checkProfile()"
                      id="updateProfile"
                    >
                      {{ __('Cập nhật thông tin') }}
                    </button>
                  </div>
                </form>
              </li>
              <li>
                <form id="resetPassword" name="resetPassword" method="POST" action="{{route('resetPassword')}}"
                      class="uk-form-stacked uk-padding uk-padding-remove-left">
                  @csrf
                  <div>
                    <h2 class="uk-border-bottom">
                      Đổi Mật Khẩu
                    </h4>
                  </div>
                  <div class="uk-width-1-2@s uk-margin-auto-bottom uk-visible-toggle error-oldProfile" tabindex="-1">
                    <label class="uk-form-label" for="form-Email">Mật Khẩu Cũ</label>
                    <div class="uk-form-controls">
                      <div class="uk-position-relative" style="max-height: 40px;">
                        <input class="uk-input uk-input2" id="old_password" name="old_password" type="password"
                               data-parsley-errors-container=".error-oldProfile"
                               name="password" required/>
                        <i class="las la-eye icon-password uk-position-absolute uk-hidden-hover " id="iconPassword"
                           title="Xem Mật khẩu" uk-tooltip="pos: top-right"></i>

                      </div>
                    </div>

                  </div>
                  <div class="uk-width-1-2@s uk-margin uk-margin-auto-bottom error-newProfile">
                    <label class="uk-form-label" for="form-newPass">Mật Khẩu Mới</label>
                    <div class="uk-form-controls">
                      <div class="uk-position-relative"  style="max-height: 40px;">
                        <input class="uk-input uk-input2 " id="password" name="password" type="password"
                               pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" minlength="6" required
                               data-parsley-errors-container=".error-newProfile"
                               name="newPass" onkeyup="check();"/>
                             <div id="warning-pass">

                             </div>

                      </div>
                    </div>

                  </div>

                  <div class="uk-width-1-2@s uk-margin error-confirmProfile">
                    <label class="uk-form-label" for="form-oldPassword">Xác Nhận Mật Khẩu</label>
                    <div class="uk-form-controls">
                      <div id="test1" class="uk-position-relative">
                        <input class="uk-input uk-input2" id="password_confirmation" name="password_confirmation"
                               type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" minlength="6"
                               required
                               data-parsley-errors-container=".error-confirmProfile"
                               name="oldPass" onkeyup="check();"/>
                        <span id="message"></span>
                      </div>
                    </div>
                  </div>
                  <div class="uk-text-right">
                    <button class="uk-button uk-button-small uk-btn uk-border-rounded"
                            type="submit" onclick="checkPassword()">
                      Cập nhật mật khẩu
                    </button>
                  </div>
                </form>
              </li>
              <li>
                <form class="uk-form-stacked uk-padding uk-padding-remove-left">
                  <div>
                    <h2 class="uk-border-bottom">
                      Thông Tin Lịch Sử Hiến Máu
                    </h2>
                  </div>
                  <div class="uk-overflow-auto uk-height-medium">
                    <table class="uk-table uk-table-hover uk-table-divider uk-table-condensed">
                      <thead>
                      <tr>
                        <th>Stt</th>
                        <th>Ngày Hiến</th>
                        <th>Nơi Hiến</th>
                        <th title="Đơn Vị Hiến" uk-tooltip>ĐV (ml)</th>
                        <th>Trạng Thái</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php $i=0 ?>
                      @foreach($history as $history)
                      <tr href="#modal-table{{$history->id}}" uk-toggle>
                        <td>{!! $i=$i+1 !!}</td>
                        <td>{!! $history->time !!}</td>
                        <td>{!! $history->address !!}</td>
                        <td>{!! $history->unit !!}</td>
                        @if($now->diffInDays( $history->time)<2)
                        <td>máu đã xét nghiệp</td>
                        @elseif($now->diffInDays( $history->time)<4)
                          <td>máu đang nằm trong kho</td>
                        @else
                          <td>máu đã được sử dụng</td>
                        @endif
                      </tr>
                      @endforeach






                      </tbody>
                    </table>
                  </div>
                  @foreach($history1 as $history1)
                  <div id="modal-table{{$history1->id}}" class="modal-tab" uk-modal>
                    <div class="uk-modal-dialog">
                      <button class="uk-modal-close-default" type="button" uk-close></button>
                      <div class="uk-modal-header">
                        <h2 class="uk-modal-title uk-text-center">
                          Bảng Thông Tin Chi Tiết
                        </h2>
                      </div>
                      <div class="uk-modal-body uk-padding-remove-right">
                        <div uk-grid>
                          <div class="uk-width-3-5@m uk-width-1-1@s">
                            <h3>
                              Các chỉ số chi tiết về lần hiến máu ngày :
                                {!! $history1->time !!}
                            </h3>
                            <div class="uk-overflow-auto uk-height-small">
                              <ul uk-accordion class="uk-margin-small-right">
                                <li class="uk-open">
                                  <a class="uk-accordion-title size-form-md" href="#">Số
                                    lượng hồng cầu(WBC)</a>
                                  <div
                                    class="uk-accordion-content uk-padding uk-padding-remove-top uk-background-muted">
                                    <div class="uk-padding-small">
                                      <p>Chỉ Số An Toàn: 4-10 g/L</p>
                                      <p>Chỉ số của bạn:</p>
                                      <div class="uk-position-relative">
                                        <p
                                          class="uk-position-absolute value-number">
                                          {!! $history1->erythrocyte !!}
                                        </p>
                                        <div class="uk-position-absolute setting-icon uk-text-danger"
                                             id="icon-change"></div>
                                        <progress id="js-progressbar"
                                                  class="uk-progress setting-progress"
                                                  value=" {!! $history1->erythrocyte !!}" max="100"></progress>

                                      </div>
                                    </div>
                                  </div>
                                </li>



                                <li>
                                  <a class="uk-accordion-title size-form-md" href="#">Phần
                                    trăm tiểu cầu </a>
                                  <div
                                    class="uk-accordion-content uk-padding uk-padding-remove-top uk-background-muted">
                                    <div class="uk-padding-small">
                                      <p>Chỉ Số An Toàn: 20-50 %</p>
                                      <p>Chỉ số của bạn:</p>
                                      <div class="uk-position-relative">
                                        <p
                                          class="uk-position-absolute value-number">
                                          {!! $history1->platelet!!}
                                        </p>
                                        <div class="uk-position-absolute setting-icon uk-text-danger"
                                             id="icon-change"></div>
                                        <progress id="js-progressbar"
                                                  class="uk-progress setting-progress"
                                                  value="  {!! $history1->platelet!!}" max="100"></progress>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <a class="uk-accordion-title size-form-md" href="#">Phần
                                    trăm huyết sắc tố</a>
                                  <div
                                    class="uk-accordion-content uk-padding uk-padding-remove-top uk-background-muted">
                                    <div class="uk-padding-small">
                                      <p>Chỉ Số An Toàn: 20-50 %</p>
                                      <p>Chỉ số của bạn:</p>
                                      <div class="uk-position-relative">
                                        <p
                                          class="uk-position-absolute value-number">
                                          {!! $history1->hemoglobin !!}
                                        </p>
                                        <div class="uk-position-absolute setting-icon uk-text-danger"
                                             id="icon-change"></div>
                                        <progress id="js-progressbar"
                                                  class="uk-progress setting-progress"
                                                  value=" {!! $history1->hemoglobin !!}" max="100"></progress>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="uk-width-2-5@m uk-width-1-1@s">
                            <div uk-overflow-auto>
                              <h3 class="uk-text-center">
                                Lời Khuyên Dinh Dưỡng
                              </h3>
                              <div class="uk-margin-small-right">
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipiscing elit, sed do eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua.
                                  Ut enim ad minim veniam, quis nostrud
                                  exercitation ullamco laboris nisi ut aliquip
                                  ex ea commodo consequat. Duis aute irure dolor
                                  in reprehenderit in voluptate velit esse
                                  cillum dolore eu fugiat nulla pariatur.
                                  Excepteur sint occaecat cupidatat non
                                  proident, sunt in culpa qui officia deserunt
                                  mollit anim id est laborum.
                                </p>
                                <div class="uk-panel">
                                  <img class="uk-align-left uk-margin-remove-adjacent"
                                       src="images/light.jpg" width="225" height="150"
                                       alt="Example image"/>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip
                                    ex ea commodo consequat.
                                  </p>
                                  <p>
                                    Duis aute irure dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat
                                    nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui
                                    officia deserunt mollit anim id est laborum.
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                  </p>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipiscing elit, sed do eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua.
                                  Ut enim ad minim veniam, quis nostrud
                                  exercitation ullamco laboris nisi ut aliquip
                                  ex ea commodo consequat. Duis aute irure dolor
                                  in reprehenderit in voluptate velit esse
                                  cillum dolore eu fugiat nulla pariatur.
                                  Excepteur sint occaecat cupidatat non
                                  proident, sunt in culpa qui officia deserunt
                                  mollit anim id est laborum.
                                </p>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipiscing elit, sed do eiusmod tempor
                                  incididunt ut labore et dolore magna aliqua.
                                  Ut enim ad minim veniam, quis nostrud
                                  exercitation ullamco laboris nisi ut aliquip
                                  ex ea commodo consequat. Duis aute irure dolor
                                  in reprehenderit in voluptate velit esse
                                  cillum dolore eu fugiat nulla pariatur.
                                  Excepteur sint occaecat cupidatat non
                                  proident, sunt in culpa qui officia deserunt
                                  mollit anim id est laborum.
                                </p>
                                <div class="uk-panel">
                                  <img class="uk-align-right uk-margin-remove-adjacent"
                                       src="images/light.jpg" width="225" height="150"
                                       alt="Example image"/>
                                  <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip
                                    ex ea commodo consequat.
                                  </p>
                                  <p>
                                    Duis aute irure dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat
                                    nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui
                                    officia deserunt mollit anim id est laborum.
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="uk-modal-footer uk-flex uk-flex-between">
                        <a href="#" target="_blank"
                           class="uk-button uk-button-small uk-button-text uk-text-primary"><i>Liên
                            Hệ Tư vấn Trực Tiếp</i></a>
                        <button
                          class="uk-button uk-button-small uk-button-default uk-border-rounded uk-modal-close"
                          type="button">
                          Hủy
                        </button>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
@section('script')
  <script>
    $(function blood() {
      $.ajax({
        url: 'http://127.0.0.1:8000/blood',
        method: 'GET',
        success: function (result) {
          const arrayToTree = (items=[], id = null, link = 'parent_id') => items.filter(item => id==null ? !items.some(ele=>ele.id===item[link]) : item[link] === id ).map(item => ({ ...item, children: arrayToTree(items, item.id) }))
          const temp1=arrayToTree(result)
          console.log(temp1)
          $("[type=text][id=blood_id]").treeSelect({
            json: {
              title: 'name',
              value: 'id',
              child: 'children'
            },
            datatree: temp1
          });
        }, error: function (e) {
          console.log(e)
        }
      });
    })



  </script>



  <script>
    $(document.formProfile).parsley({
      trigger: "change",
      errorClass: "uk-form-danger",
      errorsWrapper: "<div class=\"invalid-message\"></div>",
      errorTemplate: "<span class='uk-text-danger'></span>",
    });

    $(document.resetPassword).parsley({
      trigger: "change",
      errorClass: "uk-form-danger",
      errorsWrapper: "<div class=\"invalid-message\"></div>",
      errorTemplate: "<span class='uk-text-danger'></span>",
    });

  </script>
  <script>
    var iconPassword = document.getElementById("iconPassword"), inputPassword = document.getElementById("old_password"),
      onToggleTypePassword = function () {
        null !== inputPassword && (iconPassword.classList.toggle("la-low-vision"), "password" === inputPassword.type ? inputPassword.type = "text" : inputPassword.type = "password")
      };
    iconPassword.addEventListener("click", onToggleTypePassword), UIkit.util.ready(function () {
      let t = document.getElementById("js-progressbar");
      t.min = 37, t.maxchiso = 72, t.value < t.min ? document.getElementById("icon-change").innerHTML = '<i class="las la-exclamation uk-background-warning" id="icon-progress" title="Nguy Hiểm: Chỉ số của Bạn thấp hơn mức bình thường" uk-tooltip="pos: bottom-left"></i>' : t.value <= t.maxchiso && t.value >= t.min ? document.getElementById("icon-change").innerHTML = '<i class="lar la-check-circle uk-text-success" title="An toàn" uk-tooltip="pos: bottom-left"></i>' : document.getElementById("icon-change").innerHTML = '<i class="las la-exclamation uk-background-warning" id="icon-progress" title="Nguy Hiểm: Chỉ số của Bạn cao hơn mức bình thường" uk-tooltip="pos: bottom-left"></i>'
    });
    const check = function () {
      if ("" === document.getElementById("password").value && "" === document.getElementById("password_confirmation").value) {
        document.getElementById("message").innerHTML = ``;
      }
      "" == document.getElementById("password").value && "" == document.getElementById("password_confirmation").value || (document.getElementById("password").value == document.getElementById("password_confirmation").value ? document.getElementById("message").innerHTML = '<i class="lar la-check-circle uk-position-absolute icon-truePassword" id="icon-oldPassword" uk-tooltip="title: Mật khẩu trùng khớp; pos: bottom-right"></i>' : document.getElementById("message").innerHTML = '<i class="las la-exclamation-circle uk-position-absolute icon-wrongPassword" id="icon-oldPassword" uk-tooltip="title: Mật khẩu không khớp; pos: bottom-right"></i>')
    };
    $(document).ready(function () {
      new Image_Upload_Preview($("input[type=file]"), $("#img-avatar"))
    });

  </script>
  <script>
    const Image_Upload_Preview = function (t, n) {
      if (this.is_supported = function () {
        if (!FileReader instanceof Function) return !1
      }, this.validate_inputs = function () {
        return !(!$(t).get(0) instanceof HTMLInputElement || "file" != $(t).first().attr("type")) && (!(!$(n).get(0) instanceof HTMLImageElement) && void 0)
      }, this.is_supported() || this.validate_inputs()) return !1;
      $(t).attr("accept", "image/*"), $(t).change(function () {
        if (!this.files || this.files.length < 1) return !1;
        const t = new FileReader;
        t.onload = function (t) {
          let e = null;
          e = !(e = t.target && t.target.result ? t.target.result : e) && t.srcElement && t.srcElement.result ? t.srcElement.result : e, $(n).attr("src", e)
        }, t.readAsDataURL(this.files[0])
      })
    };
  </script>
  <script>
      const warningpass = document.querySelector('#password');
      const addWarning = document.querySelector('#warning-pass')
      warningpass.addEventListener('keyup', () => {
        warningpass.value !== '' ?
        addWarning.innerHTML = ` <i class="las la-exclamation uk-position-absolute icon-checkPassword uk-text-warning"
                           title="Mật khẩu gồm có ít nhất 1 kí tự in hoa và in thường,độ dài lớn hơn 6"
                           uk-tooltip="pos: top-right" style="font-size: 20px"></i>`
                           :
        addWarning.innerHTML = ``
      })
  </script>
@endsection
