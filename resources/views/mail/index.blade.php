<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 14px;font-family:Arial, Helvetica, sans-serif;background-color: #222;">
  <tbody>
  <tr>
    <td>
      <table style="width: 600px; margin: 0 auto; border-collapse: collapse;">
        <tbody>
        <tr bgcolor="red">
          <td style="color: white;text-align: center;padding: 1.5rem;">
            <h1>Save Blood</h1>
          </td>
        </tr>
        <tr bgcolor="lightgray">
          <td style="text-align: center;padding: 1rem;">
            <h2>Thông tin phản hồi của người dùng</h2>
          </td>
        </tr>
        <tr bgcolor="white">
          <td style="padding: 1rem;">
            <table style="width: 100%; border-spacing: 0;">
              <tbody>
              <tr style="vertical-align: top;">
                <td>
                  <h3 style="text-decoration: underline; color: crimson;">Địa chỉ email người dùng</h3>
                  <p style="line-height: 1.5;">
                    <strong>{{$details['email']}}</strong><br>
                  </p>
                </td>
              </tr>
              <tr style="vertical-align: top;">
                <td>
                  <h3 style="text-decoration: underline; color: crimson;">chức danh người dung</h3>
                  <p style="line-height: 1.5;">
                    <strong>{{$details['subject']}}</strong><br>
                  </p>
                </td>
              </tr>
              <tr style="vertical-align: top;">
                <td>
                  <h3 style="text-decoration: underline; color: crimson;">lời nhắn người dung</h3>
                  <p style="line-height: 1.5;">
                    <strong>{{$details['messenger']}}</strong><br>
                  </p>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr bgcolor="black" style="border-top: solid 4px crimson;">
          <td>
            <table style="width: 100%; color: lightgray; text-align: center">
              <tbody>
              <tr valign="top">
                <td style="padding: 2rem; width: 50%">
                  Liền Kề 226 No-04 khu A Khu đất dịch vụ yên Nghĩa, Quận Hà Đông, Hà Nội.
                </td>
                <td style="padding: 2rem; width: 50%">
                  +94 423-23-221 <br>
                  cskh@hotro.wesports.vn
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr bgcolor="white">
          <td align="center" style="padding: 1rem;">
            &copy; {{date("Y")}} Công ty TNHH Wesports.,
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>
