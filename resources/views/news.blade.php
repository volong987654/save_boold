@extends('layouts.app')
@section('title', __('client.news'))
@section('css')

@endsection
@section('content')

  <!--=================================
 rev-slider -->

  <div>
    <!-- icon scroll top -->
    <div id="scrollTop">
    </div>
  {{-- off scroll --}}
  @include('inc.background')
  <!-- body -->
    <div class="uk-section">
      <div class="uk-container">

        <div class="uk-grid-medium uk-padding" uk-grid>
          @foreach ($new as $news)
            <div class="uk-width-1-3@l uk-width-1-3@m uk-width-1-2@s">
              <div class="" uk-grid>
                <div>
                  <div class="uk-card uk-card-default  uk-box-shadow-large ">
                    <div class="uk-card-media-top uk-cover-container anh">
                      <img src="{!! $news->image !!}" class="uk-animation-reverse uk-transform-origin-top-right" uk-scrollspy="cls: uk-animation-kenburns; repeat: true"  >
                    </div>
                    <div class="uk-card-body">
                      <p class="uk-text-danger ">{!! $news->created_at->toFormattedDateString(); !!}</p>
                      <h3 class="uk-text-bold  uk-w700 uk-text-secondary uk-margin-remove-top title-blog"><a
                          href="{{route('news.tintuc',$news->slug)}}"> {!! $news->name !!} </a></h3>
                      <p class="uk-text-lighter">{!! $news->description !!}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="uk-width-1-1 uk-flex uk-flex-center uk-margin-large uk-text-center uk-padding-remove-left"
             id="navigator-new">
          {{--            {!! $new->links() !!}--}}
          {!! $new->links('vendor.pagination.bootstrap-4') !!}
        </div>
      </div>
    </div>
  </div>
  <div class="uk-section uk-background-muted">
    <div class="uk-container">
      <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1"
           uk-slider="autoplay: true;autoplay-interval: 3000;">
        <ul class="uk-slider-items uk-child-width-1-5@l uk-child-width-1-3@m uk-width-1-1@s">
          @foreach($organizations as $organizations)
            <li class="uk-margin-left"><a href="{!! $organizations->url !!}" target="_blank"><img
                  src="{!! $organizations->logo !!}" alt="Thumb" class="uk-align-center "
                  style="margin-bottom: 0;"/></a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
  <!-- off -->
  </div>
  <!--Newsletter -->
@endsection
@section('script')
  <script>
    var a = document.body.setAttribute("onscroll", "myFunction()");
    let sTop = document.getElementById("scrollTop");
    const icont = document.getElementById("icon-top");

    function myFunction() {
      500 <= window.pageYOffset ?
        (sTop.setAttribute('class', 'button-top'), sTop.innerHTML =
          '<a class="uk-text-while uk-link uk-link-toggle uk-position-absolute icon-toggle " id="icon-top" href="#" uk-totop uk-scroll></a>',
          sTop.setAttribute("style", "transition: all .2s")) :
        (sTop.removeAttribute("class"), sTop.innerHTML = '');
    };
  </script>
@endsection
