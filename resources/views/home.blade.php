@extends('layouts.layoutNotFooter')
@section('title', __('client.homePage'))
@section('css')

@endsection
@section('content')
  <div class="background-home" style="min-height: 100vh">
    @include('inc.nav')
    {{-- <hr class="uk-margin-remove">   --}}
    <div >
      <div class="form-home">
        <div class="uk-grid-match">
          <div class="uk-width-1-1 uk-light uk-position-absolute"
          uk-scrollspy="cls: uk-animation-slide-left; repeat: true;target: .slider; delay: 100"
          >
            <div class="slider"
                 uk-slideshow="ratio: 8:3;animation: slide; autoplay: true; autoplay-interval: 8000; pause-on-hover: false"

                 >
              <div class="uk-slideshow-items uk-border-rounded">
                @foreach($background as  $background)
                  <div>
                    <div
                      class="uk-position-cover uk-animation-reverse uk-transform-origin-center-left uk-border-rounded">
                      <img class="uk-border-rounded" src="{!!  $background->image  !!}" alt="" uk-cover>
                      <div class="uk-width-1-2 uk-position-center-left uk-panel uk-padding">
                        <h2 class=" uk-text-bold uk-visible@m"
                            data-uk-scrollspy="cls: uk-animation-slide-left; delay: 500; repeat: true;"
                            style="font-size: 50px; "
                            >{!!  $background->title !!}</h2>
                        <p data-uk-scrollspy="cls: uk-animation-slide-right; delay: 500; repeat: true;"
                           class="uk-text-while uk-visible@m">
                          {!!  $background->text  !!}.</p>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>

          <div class="uk-position-relative uk-width-1-3@m uk-width-1-1@s uk-border-rounded uk-padding-remove-left form-tt"
          uk-scrollspy="cls: uk-animation-slide-right; repeat: true;"
          >
            <form name="form1" class="uk-padding-small form uk-grid-match card-1 form-home"
                  onsubmit="return checkForm()"
                  method="POST"
            >
              <div>
                <h2 class="uk-text-center uk-padding-small uk-padding-remove-bottom uk-text-while" style="font-size: 40px;font-weight: 500; ">Thông Tin
                  Tìm Máu</h2>
              </div>
              <div class="uk-margin error-nameHome">
                <label class="uk-form-label uk-text-while" for="name">Họ Và Tên</label>
                <div class="uk-inline">
                  <span class="uk-form-icon">
                    <i class="las la-user la-lg"></i>
                  </span>
                  <input class="uk-input uk-input2 uk-border-rounded uk-light"
                         id="name"
                         name="name"
                         type="text"
                         placeholder="Nhập Họ Tên"
                         data-parsley-errors-container=".error-nameHome"
                         @if(Auth::check())
                         value=" {{Auth::user()->name}}"
                         disabled
                    @endif
                  >

                </div>
              </div>


              <div class="uk-margin error-blood">
                <label class="uk-form-label uk-text-while" for="blood_id">Nhóm máu</label>
                <div class="uk-form-controls ">
                  <input class="uk-input uk-input1"
                         name="blood_id" id="blood_id"
                         data-parsley-errors-container=".error-blood"
                         type="text"
                         placeholder="Chọn">
                </div>
              </div>
              <div class="uk-margin uk-margin-medium-bottom uk-width-1-1">
                <label class="uk-form-label uk-text-while" for="form-stacked-where">Nơi Điều Trị</label>
                <div class="uk-form-controls error-hospital">
                  <input class="uk-input uk-input2 uk-height-medium uk-overflow-auto"
                         id="hospital_id"
                         data-parsley-errors-container=".error-hospital"
                         name="hospital_id"
                         type="text"
                         placeholder="Nhập nơi điều trị">
                </div>
              </div>
              <div class="uk-text-center uk-margin uk-width-1-1">
                <div>
                  <button class="uk-button uk-margin uk-button-small uk-btn uk-border-rounded" type="submit"
                          id="bform1"
                  >Xác Nhận
                  </button>
                  <div id="modal-close-default" uk-modal>
                    {{-- id="form-mdal" --}}
                    <div class="uk-modal-dialog uk-modal-body form-home" id="form-mdal">
                      <button class="uk-modal-close-default" type="button" uk-close></button>
                      <h2 class="uk-modal-title uk-text-center"  style="font-size: 50px;font-weight: 400;  font-family: var(--heading-font); ">Thông Tin Chi Tiết</h2>
                      <hr class="uk-text-secondary">
                      <div>
                        <form id="form2" name="form2" class="uk-grid-small uk-padding-small" method="POST"
                              data-parsley-validate>
                          <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-3@m uk-width-1-1@s uk-text-center">
                              <div class="uk-thumbnail preview">
                                <img class="uk-margin" width="100" alt="Chọn Y Lệnh"
                                     uk-tooltip="title: Đây Là Ảnh Y Lệnh; pos: right"
                                     src="">
                                <label
                                  class="uk-button uk-btn uk-button-small uk-border-rounded uk-text-center">
                                            <span>
                                                Chọn ảnh
                                            </span>
                                  <input type="file" name="instruction" id="instruction" accepts="image/*"
                                         id="file_input_demo" class="uk-invisible uk-position-absolute"
                                         data-parsley-required
                                  />
                                </label>
                              </div>
                            </div>

                            <div class="uk-width-2-3@m uk-width-1-1@s">
                              <div class="error-emailHome">
                                <input class="uk-input uk-input2 uk-margin-bottom"
                                       type="email"
                                       name="email"
                                       id="email"
                                       data-parsley-remote="users"
                                       data-parsley-errors-container=".error-emailHome"
                                       title="Địa Chỉ Email" uk-tooltip
                                       placeholder="Email" @if(Auth::check()) value=" {{Auth::user()->email}}"
                                       disabled @endif required>
                              </div>
                              <div class="error-addressHome">
                                <input class="uk-input uk-input2" name="address" id="address" type="text"
                                       data-parsley-errors-container=".error-addressHome"
                                       title="Địa Chỉ Thường Trú Của Bạn Cụ Thể Số Nhà,Phường xã..." uk-tooltip
                                       placeholder="Địa chỉ" @if(Auth::check()) value=" {{Auth::user()->address}}"
                                       disabled @endif required>
                              </div>
                            </div>
                            <div uk-grid>
                              <div class="uk-width-1-3@m error-phoneHome">
                                <input class="uk-input uk-input2" type="phone" name="phone" id="phone" type="text"
                                       data-parsley-errors-container=".error-phoneHome"
                                       data-parsley-minlength="9"
                                       placeholder="Số điện thoại" title="Số Điện Thoại" uk-tooltip
                                       data-inputmask="'mask': '+(99) 9999-9999{1,4}'"

                                       @if(Auth::check()) value=" {{Auth::user()->phone}}"
                                       disabled @endif  data-parsley-required
                                >
                              </div>
                              <div class="uk-width-1-3@m error-cmndHome">
                                <input class="uk-input uk-input2" name="identity_card" id="identity_card" type="text"
                                       data-parsley-minlength="9"
                                       data-parsley-maxlength="12"
                                       data-parsley-type="number"
                                       data-parsley-errors-container=".error-cmndHome"
                                       placeholder="CMND / CCCD" title="Chứng Minh Nhân Dân Hoặc Căn Cước Công Dân"
                                       uk-tooltip
                                       @if(Auth::check()) value=" {{Auth::user()->identity_card}}"
                                       disabled @endif required>
                              </div>
                              <div class="uk-width-1-3@m">
                                <select name="gender" id="gender" class="uk-select uk-input2" placeholder="Giới tính">
                                  <option value="1"
                                          @if(Auth::check()) @if(Auth::user()->gender==1) selected @endif @endif>Nam
                                  </option>
                                  <option value="2"
                                          @if(Auth::check()) @if(Auth::user()->gender==2) selected @endif @endif>Nữ
                                  </option>
                                </select>
                              </div>
                            </div>


                            <div class="uk-width-1-2@m error-fatherOrMomHome">
                              <input class="uk-input uk-input2" name="relatives_name" id="relatives_name" type="text"
                                     data-parsley-errors-container=".error-fatherOrMomHome"
                                     pattern="[^*+&%$#@!~()0-9/><\][\\\x22,;|]+"
                                     data-parsley-minlength="6"
                                     title="Họ Tên Bố Hoặc Mẹ" uk-tooltip
                                     placeholder="Họ tên người thân" required>
                            </div>
                            <div class="uk-width-1-2@m error-phoneFatherOrMomHome">
                              <input
                                class="uk-input uk-input2"
                                name="relatives_phone"
                                id="relatives_phone"
                                type="phone"
                                placeholder="SDT Người Thân"
                                data-parsley-minlength="9"
                                title="Số Điện Thoại Người Thân" uk-tooltip
                                data-parsley-errors-container=".error-phoneFatherOrMomHome"
                                data-inputmask="'mask': '+(99) 9999-9999{1,3}'"
                                data-parsley-required
                              />
                            </div>


                            <div class="uk-width-1-1 uk-margin error-descriptionHome">
                              <textarea class="uk-textarea uk-input2"
                                        data-parsley-errors-container=".error-descriptionHome"
                                        id="description" name="description" rows="5"
                                        placeholder="Lý do cần máu" data-parsley-required></textarea>
                            </div>

                            <div class="uk-width-1-1 uk-text-right">
                              <button
                                class="uk-button uk-button-small uk-border-rounded uk-button-default uk-modal-close"
                                type="button">Hủy
                              </button>
                              <button
                                class="uk-button uk-button-small uk-margin-left uk-border-rounded uk-btn"
                                id="bform2" type="submit">Tìm kiếm
                              </button>
                            </div>

                          </div>
                        </form>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- footer -->

  <!--=================================
  Newsletter -->
@endsection
@section('script')
  <script>
    $(function blood() {
      $.ajax({
        url: 'http://127.0.0.1:8000/blood',
        method: 'GET',
        success: function (result) {
          var arrayToTree = (items=[], id = null, link = 'parent_id') => items.filter(item => id==null ? !items.some(ele=>ele.id===item[link]) : item[link] === id ).map(item => ({ ...item, children: arrayToTree(items, item.id) }))
          var temp1=arrayToTree(result)
          console.log(temp1)
          $("[type=text][id=blood_id]").treeSelect({
            json: {
              title: 'name',
              value: 'id',
              child: 'children'
            },
            datatree: temp1
          });
        }, error: function (e) {
          console.log(e)
        }
      });
    })
    $(function hospital() {
      $.ajax({
        url: 'http://127.0.0.1:8000/hospital',
        method: 'GET',
        success: function (result) {
          var arrayToTree = (items = [], id = null, link = 'parent_id') => items.filter(item => id == null ? !items.some(ele => ele.id === item[link]) : item[link] === id).map(item => ({
            ...item,
            children: arrayToTree(items, item.id)
          }))
          var temp1 = arrayToTree(result)
          console.log(temp1)
          $("[type=text][id=hospital_id]").treeSelect({
            json: {
              title: 'name',
              value: 'id',
              child: 'children'
            },
            datatree: temp1
          });
        }, error: function (e) {
          console.log(e)
        }
      });
    })
  </script>
  <script>

    $(document).ready(function () {
      $('#bform2').click(function () {
        var formData = new FormData();
        formData.append("instruction", $('#instruction')[0].files[0]);
        formData.append("name", $("#name").val());
        formData.append("blood_id", $("#blood_id").val());
        formData.append("hospital_id", $("#hospital_id").val());
        formData.append("email", $("#email").val());
        formData.append("phone", $("#phone").val());
        formData.append("relatives_name", $("#relatives_name").val());
        formData.append("gender", $("#gender").val());
        formData.append("relatives_phone", $("#relatives_phone").val());
        formData.append("identity_card", $("#identity_card").val());
        formData.append("description", $("#description").val());
        formData.append("address", $("#address").val());
        let request = $.ajax({
          url: '{{route('find.store')}}',
          method: 'post',
          data: formData,
          processData: false,
          contentType: false,
          success: function (response) {
            if (response.data) {
              UIkit.notification(response.message, {status: 'success'})
              location.reload();

            }
          }
        });
        return false;
      });

      // server chỗ mô
      $(document.form1, document.form2).parsley({
        trigger: "change",
        errorClass: "uk-form-danger",
        errorsWrapper: "<div class=\"invalid-message\"></div>",
        errorTemplate: "<span class='uk-text-danger'></span>",
      })
        .on("form:submit", function (t) {
          for (let e = {}, n = 0; n < t.fields.length; n++) e[t.fields[n].element.name] = t.fields[n].value;
          return !1
        });


    })
  </script>
  <script>
    const Image_Upload_Preview = function (t, n) {
      if (this.is_supported = function () {
        if (!FileReader instanceof Function) return !1
      }, this.validate_inputs = function () {
        return !(!$(t).get(0) instanceof HTMLInputElement || "file" != $(t).first().attr("type")) && (!(!$(n).get(0) instanceof HTMLImageElement) && void 0)
      }, this.is_supported() || this.validate_inputs()) return !1;
      $(t).attr("accept", "image/*"), $(t).change(function () {
        if (!this.files || this.files.length < 1) return !1;
        const t = new FileReader;
        t.onload = function (t) {
          let e = null;
          e = !(e = t.target && t.target.result ? t.target.result : e) && t.srcElement && t.srcElement.result ? t.srcElement.result : e, $(n).attr("src", e)
        }, t.readAsDataURL(this.files[0])
      })
    };
    $(document).ready(function () {
      new Image_Upload_Preview($("input[type=file]"), $(".preview img"))
    });
  </script>
  <script type="text/javascript">
    // javascript check
    function checkForm() {
      let t = document.getElementById("name").value, e = document.getElementById("blood_id").value,
        w = document.getElementById("hospital_id").value;
      if (t == "") {
        alert("Vui Lòng Điền Họ Tên Bạn");
        return false;
      } else if (t.length < 6) {
        alert("Vui Lòng Điền Đầy Đủ Họ Tên Bạn");
        return false;
      } else if (e == "") {
        alert("Vui Lòng Chọn Nhóm Máu");
        return false;
      } else if (w == "") {
        alert("Vui Lòng Chọn Nơi Điều Trị");
        return false;
      }
      UIkit.modal("#modal-close-default").show();
      // return false;
    }
  </script>
@endsection
