@extends('layouts.app')
@section('title', __('client.find-blood'))
@section('css')

@endsection
@section('content')
  <!-- background -->
  <div class="abc uk-section uk-section-large uk-padding-remove-bottom">
    <div class="uk-container uk-box-shadow">
      <div class="uk-text-center">
        <h1 class="title1 uk-margin-large">Chúng Ta Làm Gì</h1>
        <h2 class="title uk-text-bold uk-margin-large">Đăng Kí Hiến Máu Tại Đây</h2>
        <h3> Các cộng tác viên sẽ chủ động liên hệ với bạn.</h3>
        <h3> Các bạn yên tâm nhé</h3>
        </h6>
      </div>
      <div>

        <div class="uk-grid-match uk-grid-small uk-text-center" uk-grid>
          @foreach($find as $finds)
            <div class="uk-width-1-3@m link1 ">
              <div class="uk-card uk-card-body ">
                <p><span class="font">{!! $finds->Blood->name!!}</span></p>
                <p class="top">{!! $finds->hospital->name !!}</p>
                <a @if (Auth::check()) href="{{route('dangki',$finds->Blood->id)}}"@endif ">
                <button class="uk-button uk-border-rounded uk-button-danger"
                        @if (!Auth::check()) uk-toggle="target: #modal-close-default1" @endif>Đăng Kí<i
                    class="lab la-sistrix icon"></i></button>
                </a>
              </div>
            </div>
          @endforeach

        </div>
        <div class="uk-align-center uk-flex uk-flex-center uk-width-1-1" id="navigator-new">
          {{--          {!!$find->links()!!}--}}
          {!! $find->links('vendor.pagination.bootstrap-4') !!}
        </div>
      </div>
    </div>
  </div>
  <div class="uk-section uk-background-muted uk-margin-top">
    <div class="uk-container">
      <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1"
           uk-slider="autoplay: true;autoplay-interval: 500;">
        <ul class="uk-slider-items uk-child-width-1-5@l uk-child-width-1-3@m uk-width-1-1@s">
          @foreach($organizations as $organizations)
            <li><a href="{!! $organizations->url !!}}" target="_blank"><img src="{!! $organizations->logo !!}}"
                                                                            alt="Thumb" class="uk-align-center"
                                                                            style="margin-bottom: 0;"/></a></li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@endsection
