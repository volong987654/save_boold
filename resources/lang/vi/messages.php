<?php

return [
  "createSuccess" => "Tạo thành công",
  "updateSuccess" => "Cập nhật thành công",
  "deleteSuccess" => "Xóa thành công",
  "unauthorized" => "Bạn không được phép truy cập tài nguyên này",
  "getTokenSuccess" => "Nhận Mã thành công",
  "alreadyExists" => "Đã tồn tại",
  "unsupportedFileType" => "Loại tập tin không được hỗ trợ",
  "theFolderNeedsEmpty" => "Thư mục cần trống để được xóa",
  "fileDoesNotExist" => "Tập tin không tồn tại",
];
