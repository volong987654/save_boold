<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'middleware' => ['language']], function ($router) {
  Route::post("check-slug", "AuthController@checkSlug");
  Route::apiResources([
    'auth' => 'AuthController',
    'media' => 'MediaController',
  ]);
  Route::group(["middleware" => ["role:api/v1"]], function($router) {
    Route::apiResources([
      'user' => 'UserController',
      'role' => 'RoleController',
      'permission' => 'PermissionController',
      'role-permission' => 'RolePermissionController',
      "setting" => 'SettingController',
      "blood" => 'BloodController',
      "find" => 'FindController',
      "hospital" => 'HospitalController',
      "news" => 'NewsController',
      "organization" => 'OrganizationController',
      "category" => 'CategoryController',
      "slide" => 'SlideController',
    ]);
  });
});
