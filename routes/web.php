<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Auth::routes();


Route::group(['namespace' => 'Web\V1'], function ($router) {
  app()->setLocale('vi');


  Route::get('/', 'HomeController@index')->name('home');
  Route::post('/', 'HomeController@findStore')->name('find.store');
  Route::get('/administrator', 'HomeController@admin');

  Route::get('/news', 'HomeController@news')->name('news');
  Route::get('news/{slug}', 'HomeController@newDetail')->name('news.tintuc');
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/contact', 'HomeController@viewContact')->name('contact');
  Route::get('/profile', 'HomeController@viewProfile')->name('profile');
  Route::put('/profile', 'HomeController@updateProfile')->name('profile.update');
  Route::post('/contact', 'HomeController@contactStore')->name('contact.store');
  Route::post("/login", 'HomeController@login');
  Route::post("/register", 'HomeController@register');
  Route::get("/logout",'HomeController@logout');
  Route::get("/find-blood",'HomeController@findBlood')->name('find.blood');
  Route::get("/vinhdanh",'HomeController@vinhdanh')->name('vinhdanh');
  Route::post("/profile/1",'HomeController@resetPassword')->name('resetPassword');
  Route::get("/BloodDonation/{id}",'HomeController@BloodDonation')->name('dangki');
  Route::get("/blood",'HomeController@blood')->name('blood');
  Route::get("/hospital",'HomeController@hospital')->name('hospital');
  Route::post("/news/fetch/{slug}",'HomeController@comment')->name('pagination.fetch');;
  Route::post('/checkUnique', 'HomeController@checkUnique');
  Route::post('/postCommentcon/{id}/{new_id}', 'HomeController@postCommentC')->name('comment');
  Route::post('/postCommentcon/{new_id}', 'HomeController@postComment')->name('commentnull');


  Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
  });
  Route::get('/link-storage', function () {
    Artisan::call('storage:link');
  //    $targetFolder = $_SERVER['DOCUMENT_ROOT'].'/storage/app/public';
  //    $linkFolder = $_SERVER['DOCUMENT_ROOT'].'/public/storage';
  //    symlink($targetFolder,$linkFolder);
  //    echo 'Symlink process successfully completed';
  });
});
