import React from 'react';
import { connect, router, routerRedux } from 'dva';
import { Layout } from 'antd';
import { SwitchTransition, CSSTransition } from 'react-transition-group';
import $$ from 'cmn-utils';
import cx from 'classnames';
import isEqual from 'react-fast-compare';
import PropTypes from 'prop-types';

import NavBar from 'components/NavBar';
import TopBar from 'components/TopBar';
import SkinToolbox from 'components/SkinToolbox';
import { LeftSideBar } from 'components/SideBar';

import config from '@/config';
import routerLinks from "@/utils/routerLinks";
import checkAuth from "@/utils/checkAuth";
import { enquireIsMobile } from '@/utils/enquireScreen';
import TabsLayout from './TabsLayout';

import 'assets/styles/transition.less';
import './styles/basic.less';

const { Switch } = router;
const { Content, Header } = Layout;

/**
 * Basic department
 * Can set a variety of skin theme: [light, grey, primary, info, warning, danger, alert, system, success, dark]
 * A variety of layouts can be set [header, sidebar, breadcrumb, tabLayout]
 */
@connect(({ global }) => ({ global }))
class BasicLayout extends React.PureComponent {
  constructor(props) {
    super(props);
    const user = $$.getStore(config.store.user, []);
    const theme = $$.getStore(config.store.theme, {
      navbar: 'light',
      leftSide: 'dark',
      layout: [
        'fixedHeader',
        'fixedSidebar',
        'fixedBreadcrumbs'
      ]
    });

    this.state = {
      collapsedLeftSide: false, // Left sidebar switch control
      leftCollapsedWidth: 60, // Left column width
      theme, // Skin settings
      user,
      currentMenu: {},
      isMobile: false
    };

    props.dispatch({
      type: 'global/getMenu'
    });
  }

  componentDidMount() {
    this.checkLoginState();
    this.unregisterEnquire = enquireIsMobile(ismobile => {
      const { isMobile, theme } = this.state;
      if (isMobile !== ismobile) {
        // If the check is mobile, the sidebar is not fixed.
        if (ismobile && $$.isArray(theme.layout)) {
          theme.layout = theme.layout.filter(item => item !== 'fixedSidebar');
        }
        this.setState({
          isMobile: ismobile
        });
      }
    });
  }

  // Check if a user is logged in
  checkLoginState() {
    const user = $$.getStore(config.store.user);
    if (!user) {
      this.props.dispatch(routerRedux.replace(routerLinks("Login")));
    }
    checkAuth(user, this.props.history, false)
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !isEqual(this.props.location.pathname, prevProps.location.pathname) ||
      !isEqual(this.props.global.flatMenu, prevProps.global.flatMenu)
    ) {
      this.setState({
        currentMenu: this.getCurrentMenu(this.props) || {}
      });
    }
  }

  componentWillUnmount() {
    // Clean up monitoring
    this.unregisterEnquire();
  }

  getCurrentMenu(props) {
    const {
      location: { pathname },
      global
    } = props || this.props;
    const menu = this.getMeunMatchKeys(global.flatMenu, pathname)[0];
    return menu;
  }

  getMeunMatchKeys = (flatMenu, path) => {
    return flatMenu.filter(item => {
      return item.path.indexOf(path) > -1
    });
  };

  /**
   * Top left menu icon shrink control
   */
  onCollapseLeftSide = _ => {
    const collapsedLeftSide =
      this.state.leftCollapsedWidth === 0
        ? true
        : !this.state.collapsedLeftSide;

    this.setState({
      collapsedLeftSide,
      leftCollapsedWidth: 60
    });
  };

  /**
   * Close the left sidebar completely, that is, the width is 0
   */
  onCollapseLeftSideAll = _ => {
    this.setState({
      collapsedLeftSide: true,
      leftCollapsedWidth: 0
    });
  };

  /**
   * Expand the multifunction area in the bar where the breadcrumbs are located
   */
  onExpandTopBar = _ => {
    this.setState({
      expandTopBar: true
    });
  };

  /**
   * Contrary to above
   */
  onCollapseTopBar = _ => {
    this.setState({
      expandTopBar: false
    });
  };

  onChangeTheme = theme => {
    $$.setStore(config.store.theme, theme);
    this.setState({
      theme
    });
  };

  render() {
    const {
      collapsedLeftSide,
      leftCollapsedWidth,
      theme,
      user,
      currentMenu,
      isMobile
    } = this.state;
    const { routerData, location, global } = this.props;
    const { menu, flatMenu } = global;
    const { childRoutes } = routerData;
    const classnames = cx('basic-layout', 'full-layout', {
      fixed: theme.layout && theme.layout.indexOf('fixedSidebar') !== -1,
      'fixed-header':
        theme.layout && theme.layout.indexOf('fixedHeader') !== -1,
      'fixed-breadcrumbs':
        theme.layout && theme.layout.indexOf('fixedBreadcrumbs') !== -1,
      'hided-breadcrumbs':
        theme.layout && theme.layout.indexOf('hidedBreadcrumbs') !== -1
    });

    return (
      <Layout className={classnames}>
        <Header>
          <NavBar
            collapsed={collapsedLeftSide}
            onCollapseLeftSide={this.onCollapseLeftSide}
            theme={theme.navbar}
            user={user}
            isMobile={isMobile}
          />
        </Header>
        <Layout>
          <LeftSideBar
            collapsed={collapsedLeftSide}
            leftCollapsedWidth={leftCollapsedWidth}
            onCollapse={this.onCollapseLeftSide}
            onCollapseAll={this.onCollapseLeftSideAll}
            location={location}
            theme={theme.leftSide}
            flatMenu={flatMenu}
            currentMenu={currentMenu}
            menu={menu}
            user={user}
            isMobile={isMobile}
          />
          <Content>
            {theme.layout.indexOf('tabLayout') >= 0 ? (
              <TabsLayout childRoutes={childRoutes} location={location} />
            ) : (
              <Layout className="full-layout">
                <Header>
                  <TopBar
                    onCollapse={this.onCollapseTopBar}
                    currentMenu={currentMenu}
                    location={location}
                    theme={theme}
                    menu={menu}
                  />
                </Header>
                <Content style={{ overflow: 'hidden' }}>
                  <SwitchTransition>
                    <CSSTransition
                      key={location.pathname}
                      classNames="fade"
                      timeout={500}
                    >
                      <Layout className="full-layout">
                        <Content className="router-page">
                          <Switch location={location}>{childRoutes}</Switch>
                        </Content>
                      </Layout>
                    </CSSTransition>
                  </SwitchTransition>
                </Content>
              </Layout>
            )}
          </Content>
        </Layout>
        <SkinToolbox onChangeTheme={this.onChangeTheme} theme={theme} />
      </Layout>
    );
  }
}

BasicLayout.propTypes = {
  dispatch: PropTypes.func,
  routerData: PropTypes.object,
  location: PropTypes.object,
  global: PropTypes.object,
};

export default BasicLayout
