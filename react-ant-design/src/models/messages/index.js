export const scope = 'models.Global';

export default {
  user: {
    id: `${scope}.user`,
    defaultMessage: 'User',
  },
  settings: {
    id: `${scope}.settings`,
    defaultMessage: 'Settings',
  },
  profile: {
    id: `${scope}.profile`,
    defaultMessage: 'Profile',
  },
  website: {
    id: `${scope}.website`,
    defaultMessage: 'Website',
  },
  find: {
    id: "models.Global.find",
    defaultMessage: 'Find',
  },
  news: {
    id: "models.Global.news",
    defaultMessage: 'News',
  },
}// 💬 generate link to here
