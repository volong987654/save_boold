import axios from 'axios';
import routerLinks from "@/utils/routerLinks";
import config from '@/config';

export const update_PROFILE = async (payload) => {
  const { data } = await axios.put(routerLinks("Profile", "api") +'/' + payload.id, payload);
  if (data.message) config.notice.success(data.message);
  return data.data;
}
