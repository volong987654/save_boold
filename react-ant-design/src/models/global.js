import intl from 'react-intl-universal';
import $$ from 'cmn-utils';
import axios from "axios";

import config from '@/config';
import modelEnhance from '@/utils/modelEnhance';
import routerLinks from "@/utils/routerLinks";

import messages from './messages';
import { update_PROFILE } from "./service"

export default modelEnhance({
  namespace: 'global',

  state: {
    menu: [],
    flatMenu: [],
    locale: $$.getStore(config.store.language, "en"),
    user: $$.getStore(config.store.user),
    links: false,
  },

  effects: {
    *getMenu({payload}, { select, put }) {
      const { user } = yield select(state => state.global);
      let links = true;

      let data = [
        { name: intl.formatMessage(messages.find), icon: 'las la-money-check', path: routerLinks("Find", "curd"),},
        { name: intl.formatMessage(messages.news), icon: 'las la-newspaper', path: routerLinks("News", "curd"),},
        { name: intl.formatMessage(messages.user), icon: 'las la-user-circle', path: routerLinks("User", "curd", "{\"last_time\":\"descend\"}")},
        {
          name: intl.formatMessage(messages.settings),
          icon: 'las la-cogs',
          path: '',
          children: [
            { name: intl.formatMessage(messages.profile), path: routerLinks("Profile") },
            { name: intl.formatMessage(messages.website), path: routerLinks("Website") }
          ],
        }
        // 💬 generate link to here
      ];

      if (user && user.role_id !== 1 && !!user.permissions) {
        links = {[routerLinks("Profile")]: ["GET"]};
        user.permissions.map((item) => links[item.permission_route] = item.can);

        data = data.filter((item) => {
          let check = false;
          for (const key of Object.keys(links)){
            if (!item.children && !check && item.path.indexOf(key) === 0) {
              check = true
            } else if (item.children && item.children.length > 0) {
              item.children = item.children.filter((item) => item.path.indexOf(key) === 0 )
              check = item.children.length > 0;
            }
          }
          return check
        })
      }

      yield put({ type: 'success', payload: { links } });

      const loopMenu = (menu, pitem = {}) => {
        menu.forEach(item => {
          if (pitem.path) {
            item.parentPath = pitem.parentPath ? pitem.parentPath.concat(pitem.path) : [pitem.path];
          }
          if (item.children && item.children.length) {
            loopMenu(item.children, item);
          }
        });
      }
      loopMenu(data);

      yield put({
        type: 'getMenuSuccess',
        payload: data,
      });
    },
    *setLocale({payload}, {put}) {
      axios.defaults.headers.common['X-localization'] = payload;
      yield put({
        type: 'setLocaleSuccess',
        payload,
      });
    },
    *updateProfile({payload}, {call, put}) {
      const user = yield call(update_PROFILE, payload);
      $$.setStore(config.store.user, user);
      yield put({
        type: 'success',
        payload: { user },
      });
    }
  },

  reducers: {
    getMenuSuccess(state, { payload }) {
      return {
        ...state,
        menu: payload,
        flatMenu: getFlatMenu(payload),
      };
    },
    setLocaleSuccess(state, { payload }) {
      $$.setStore(config.store.language, payload);
      return {
        ...state,
        locale: payload,
      };
    },
    success(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
});

export function getFlatMenu(menus) {
  let menu = [];
  menus.forEach(item => {
    if (item.children) {
      menu = menu.concat(getFlatMenu(item.children));
    }
    menu.push(item);
  });
  return menu;
}
