import { createRoutes } from '@/utils/core';
import BasicLayout from '@/layouts/BasicLayout';
import UserLayout from '@/layouts/UserLayout';
import routerLinks from "@/utils/routerLinks";

import Login from './Auth/Login';
import Register from './Auth/Register';

import Page403 from './Pages/403';
import NotFound from './Pages/404';
import Page500 from './Pages/500';

import User from './Admin/User';
import Profile from './Admin/Profile';
import Website from './Admin/Website';
import Find from './Admin/Find';
import News from './Admin/News';

const routesConfig = app => [
  {
    path: '/auth',
    title: 'login',
    indexRoute: routerLinks("Login"),
    component: UserLayout,
    childRoutes: [
      Login(app),
      Register(app),
      NotFound()
    ]
  },
  {
    path: '/',
    title: 'admin',
    component: BasicLayout,
    indexRoute: routerLinks("Profile"),
    childRoutes: [
      // 💬 generate admin to here
      News(app),
      Find(app),
      Website(app),
      Profile(app),
      User(app),
      Page403(),
      Page500(),
      NotFound()
    ]
  },
  // {
  //   path: '/',
  //   title: 'frontend',
  //   indexRoute: routerLinks('Home'),
  //   component: FrontendLayout,
  //   childRoutes: [
  //     // 💬 generate frontend to here
  //     Home(app),
  //     NotFound(),
  //   ]
  // },
];

export default app => createRoutes(app, routesConfig);
