import React, { Component } from 'react';
import { connect, router } from 'dva';
import { Row, Layout, Typography } from 'antd';
import intl from 'react-intl-universal';

import Form from 'components/Form';
import routerLinks from "@/utils/routerLinks";
import { formatEmail } from "@/utils/formatRegex";

import Success from './Success';
import messages from '../messages';


import logoImg from 'assets/images/logo1.png';
import './index.less';
import '../../Login/components/index.less';
import {check_slug} from "../../../Admin/User/service";


const { Link } = router;
const { Content } = Layout;
const { Title } = Typography;

const columns = [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.email),
    name: 'email',
    formItem: {
      rules: [
        ({ getFieldValue }) => ({
          validator: async (rule, value) => {
            if (!!value && value.trim() !== "" && formatEmail.test(value)) {
              const data = await check_slug({
                id: getFieldValue("id"),
                email: value,
                model: "users"
              });
              if (data) return Promise.resolve()
              else return Promise.reject(intl.formatMessage(messages.thisEmailAddressIsAlreadyTaken));
            } else return Promise.resolve()
          },
        }),
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterEmail)
        },
        {
          type: "email",
          message: intl.formatMessage(messages.pleaseEnterTheCorrectEmailFormat)
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.password),
    name: 'password',
    formItem: {
      type: "password",
      repeat: true,
    },
  },
]

@connect(({ register, loading }) => ({
  register,
  loading: loading.effects['register/submit']
}))
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
      registerSuccess: false
    };
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.register.status) {
      return {
        registerSuccess: true
      };
    }
    return null;
  }
  handleSubmit = values => {
    const { dispatch } = this.props;
    dispatch({
      type: 'register/submit',
      payload: {
        ...values,
      }
    });
  };

  render() {
    const { loading } = this.props;
    const { registerSuccess } = this.state;

    const formProps = {
      loading,
      columns,
      onSubmit: this.handleSubmit,
      prefixCls: "login-form",
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
    };

    if (registerSuccess) {
      return <Success />;
    }
    return (
      <Layout className="full-layout register-page login-page">
        <Content>
          <div className="user-img">
            <img src={logoImg} alt="logo" />
          </div>
          <Title level={4} className="text-center">{intl.formatMessage(messages.titleSignUp)}</Title>
          <Form ref="form" {...formProps} />
          <Row align="center">
            <Link to={routerLinks("Login")}>
              {intl.formatMessage(messages.LoginNow)}
            </Link>
          </Row>
        </Content>
      </Layout>
    );
  }
}

export default Register;
