import axios from 'axios';

export async function register_User(payload) {
  const { data } = await axios.post('/auth', payload);
  return data;
}
