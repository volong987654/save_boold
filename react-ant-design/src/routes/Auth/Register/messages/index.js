export const scopeUser = 'routes.User';
export const scope = 'routes.Register';

export default ({
  name: {
    id: `${scopeUser}.name`,
    defaultMessage: 'Name',
  },
  pleaseEnterName: {
    id: `${scopeUser}.pleaseEnterName`,
    defaultMessage: 'Please enter name',
  },
  email: {
    id: `${scopeUser}.email`,
    defaultMessage: 'Email',
  },
  thisEmailAddressIsAlreadyTaken: {
    id: `${scopeUser}.thisEmailAddressIsAlreadyTaken`,
    defaultMessage: 'This email address is already taken',
  },
  pleaseEnterEmail: {
    id: `${scopeUser}.pleaseEnterEmail`,
    defaultMessage: 'Please enter email',
  },
  pleaseEnterTheCorrectEmailFormat: {
    id: `${scopeUser}.pleaseEnterTheCorrectEmailFormat`,
    defaultMessage: 'Please enter the correct email format',
  },
  password: {
    id: `${scopeUser}.password`,
    defaultMessage: 'Password',
  },

  titleSignUp: {
    id: `${scope}.titleSignUp`,
    defaultMessage: 'Sign Up To Admin Panel',
  },
  LoginNow: {
    id: `${scope}.LoginNow`,
    defaultMessage: 'Sign in with an existing account',
  },
  registrationSuccess: {
    id: `${scope}.registrationSuccess`,
    defaultMessage: 'Registration Success',
  },
  textRegister: {
    id: `${scope}.textRegister`,
    defaultMessage: 'The activation email has been sent to your email address and is valid for 24 hours. Please log in to the email in time and click on the link in the email to activate the account.',
  },

  viewMailbox: {
    id: `${scope}.viewMailbox`,
    defaultMessage: 'View mailbox',
  },
  backToLogin: {
    id: `${scope}.backToLogin`,
    defaultMessage: 'Back to login',
  },
});
