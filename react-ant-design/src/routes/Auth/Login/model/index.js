import { routerRedux } from 'dva';
import $$ from 'cmn-utils';
import axios from 'axios';

import config from '@/config';
import routerLinks from "@/utils/routerLinks";
import checkAuth from "@/utils/checkAuth";
import { login_User } from '../service';

export default {
  namespace: 'login',

  state: {
    loggedIn: false,
    message: '',
    user: {}
  },

  subscriptions: {
    setup({ history, dispatch }) {
      return history.listen(({ pathname }) => {
        if (pathname.indexOf(routerLinks("Login")) !== -1) {
          $$.removeStore(config.store.user);
          checkAuth(null, null, true)
        }
      });
    }
  },

  effects: {
    *login({ payload }, { call, put }) {
      try {
        const { status, message, data } = yield call(login_User, payload);
        if (status === "success") {
          axios.defaults.headers.common['Authorization'] = data.token
          data.remember = payload.remember;
          $$.setStore(config.store.user, data);
          yield put({
            type: 'global/@change',
            payload: { user: data }
          });
          yield put(routerRedux.replace('/'));
        } else {
          yield put({
            type: 'loginError',
            payload: { message }
          });
        }
      } catch (e) {
        console.log(e)
        yield put({
          type: 'loginError'
        });
      }
    },
    *logout(_, { put }) {}
  },

  reducers: {
    loginSuccess(state, { payload }) {
      return {
        ...state,
        loggedIn: true,
        message: '',
        user: payload
      };
    },
    loginError(state, { payload }) {
      return {
        ...state,
        loggedIn: false,
        message: payload.message
      };
    }
  }
};
