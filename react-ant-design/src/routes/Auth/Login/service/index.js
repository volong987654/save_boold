import axios from 'axios';

export async function login_User(payload) {
  const { data } = await axios.get('/auth', { params: payload });
  return data;
}

export async function refresh_AUTH() {
  const { data } = await axios.patch('/auth', {});
  return data;
}
