import React from 'react';
import { connect } from 'dva';
import { Layout } from 'antd';

import BaseComponent from 'components/BaseComponent';
import Panel from 'components/Panel';
import Form from 'components/Form';

import './index.less';
import intl from "react-intl-universal";
import messages from "../messages";

const { Content } = Layout;
const columns = [
  {
    name: 'id',
    formItem: {
      type: 'hidden'
    }
  },
  {
    title: intl.formatMessage(messages.avatar),
    name: 'avatar',
    formItem: {
      type: "media",
    },
  },
  {
    title: intl.formatMessage(messages.bio),
    name: 'bio',
    formItem: {
      type: "editor",
    },
  },
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.email),
    name: 'email',
    formItem: {
      type: "input",
      disabled: true,
    },
  },
  {
    title: intl.formatMessage(messages.password),
    name: 'password',
    formItem: {
      type: "password",
      repeat: true,
    },
  },
]

@connect(({ global, loading }) => ({
  global,
  loading: loading.effects['global/updateProfile']
}))
class Profile extends BaseComponent {
  handleSubmit = values => {
    const { dispatch } = this.props;
    dispatch({
      type: 'global/updateProfile',
      payload: {
        ...values,
      }
    });
  };

  render() {
    const { global, loading } = this.props;
    const formProps = {
      loading,
      record: global.user,
      columns,
      onSubmit: this.handleSubmit,
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      cols: {
        xs: 24,
        md: 12,
        xl: 12
      },
    };

    return (
      <Layout className="full-layout page profile-page">
        <Content>
          <Panel title={intl.formatMessage(messages.subTitle)}>
            <Form ref="form" {...formProps} />
          </Panel>
        </Content>
      </Layout>
    );
  }
}

export default Profile
