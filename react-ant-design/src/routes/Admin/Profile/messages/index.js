export const scopeUser = 'routes.User';
export const scope = 'routes.Profile';

export default {
  avatar: {
    id: `${scopeUser}.avatar`,
    defaultMessage: 'Avatar',
  },
  name: {
    id: `${scopeUser}.name`,
    defaultMessage: 'Name',
  },
  pleaseEnterName: {
    id: `${scopeUser}.pleaseEnterName`,
    defaultMessage: 'Please enter name',
  },
  email: {
    id: `${scopeUser}.email`,
    defaultMessage: 'Email',
  },
  thisEmailAddressIsAlreadyTaken: {
    id: `${scopeUser}.thisEmailAddressIsAlreadyTaken`,
    defaultMessage: 'This email address is already taken',
  },
  pleaseEnterEmail: {
    id: `${scopeUser}.pleaseEnterEmail`,
    defaultMessage: 'Please enter email',
  },
  pleaseEnterTheCorrectEmailFormat: {
    id: `${scopeUser}.pleaseEnterTheCorrectEmailFormat`,
    defaultMessage: 'Please enter the correct email format',
  },
  password: {
    id: `${scopeUser}.password`,
    defaultMessage: 'Password',
  },
  bio: {
    id: `${scopeUser}.bio`,
    defaultMessage: 'Bio',
  },
  subTitle: {
    id: `${scope}.subTitle`,
    defaultMessage: 'Change Profile',
  },
};
