import modelEnhance from '@/utils/modelEnhance';
import PageHelper from '@/utils/pageHelper';
import routerLinks from "@/utils/routerLinks";

import {
  getList_NEWS,
  save_NEWS,
  update_NEWS,
  patch_NEWS,
  delete_NEWS,
  get_CATEGORY,
  change_CATEGORY,
  delete_CATEGORY,
  get_SLIDE,
  change_SLIDE,
  delete_SLIDE,
} from "../service"
import {change_ORGANIZATION, delete_ORGANIZATION, get_ORGANIZATION} from "../../User/service";

/**
* true when the page is first loaded
* Can use this value to prevent switching pages when
* Initialize data multiple times
*/
let url = true;
export default modelEnhance({
  namespace: 'news',

  state: {
    pageData: PageHelper.create(),
    categories: [],
    slides: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname, search }) => {
        if (pathname === routerLinks("News") && !!search && url) {
          url = false
          setTimeout(() => url = true, 500);
          const payload = PageHelper.URLToArray(search);
          dispatch({ type: 'init', payload });
        }
      });
    }
  },

  effects: {
    // Enter page load
    *init({ payload }, { put, select }) {
      const { pageData } = yield select(state => state.news);
      yield put({
        type: 'getPageInfo',
        payload: {
          pageData: pageData.setPage(payload)
        }
      });
      yield put({type: 'getAllAttributes'});
    },
    *getAllAttributes({ payload }, { put, select }) {
      const { links } = yield select(state => state.global);
      if (typeof links === "boolean" || typeof links[routerLinks("Category", "api")] !== "undefined") yield put({ type: 'getCategory' });
      if (typeof links === "boolean" || typeof links[routerLinks("Slide", "api")] !== "undefined") yield put({ type: 'getSlide' });
    },
    // Get paging data
    *getPageInfo({ payload }, { call }) {
      const { pageData } = payload;
      const data = yield call (getList_NEWS, pageData)
      pageData.setPage(data);
    },
    // Save after querying paging
    *save({ payload }, { put, select, call }) {
      const { values, success } = payload;
      const { pageData } = yield select(state => state.news);

      if (values.id) yield call(update_NEWS, values);
      else yield call(save_NEWS, values);

      yield put({ type: 'getPageInfo', payload: { pageData } });
      success();
    },
    // Delete after querying paging
    *remove({ payload }, { put, select, call }) {
      const { records, success } = payload;
      const { pageData } = yield select(state => state.news);
      const data = records.map(item => item.id)
      yield call(delete_NEWS, data);
      yield put({ type: 'getPageInfo', payload: { pageData } });
      success();
    },
    *patch({ payload }, { select, call, put }) {
      const { records } = payload;
      const { pageData } = yield select(state => state.news);
      yield call(patch_NEWS, records);
      yield put({ type: 'getPageInfo', payload: { pageData } });
    },
    *getCategory({ payload }, { call, put }) {
      const data = yield call(get_CATEGORY)
      yield put({ type: 'success', payload: data });
    },
    *changeCategory({ payload }, { call, put }) {
      const categories = yield call(change_CATEGORY, payload)
      yield put({ type: 'success', payload: { categories } });
    },
    *removeCategory({ payload }, { call, put }) {
      yield call(delete_CATEGORY, payload);
      yield put({ type: 'getCategory' });
    },

    *getSlide({ payload }, { call, put }) {
      const data = yield call(get_SLIDE)
      yield put({ type: 'success', payload: data });
    },
    *changeSlide({ payload }, { call, put }) {
      const slides = yield call(change_SLIDE, payload)
      yield put({ type: 'success', payload: { slides } });
    },
    *removeSlide({ payload }, { call, put }) {
      yield call(delete_SLIDE, payload);
      yield put({ type: 'getSlide' });
    },
  },

  reducers: {
    success(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  }
});
