import axios from 'axios';
import PageHelper from '@/utils/pageHelper';

import config from '@/config';
import routerLinks from "@/utils/routerLinks";
import { handGetParent, handPostParent } from '@/utils/methodRequest';

export const getList_NEWS = async (payload) => {
  const { data } = await axios.get(routerLinks("News", "api"), {params: PageHelper.requestFormat(payload)});
  return PageHelper.responseFormat(data);
}
export const save_NEWS = async (payload) => {
  const { data } = await axios.post(routerLinks("News", "api"), payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const update_NEWS = async (payload) => {
  const { data } = await axios.put(routerLinks("News", "api") + '/' + payload.id, payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const delete_NEWS = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const id = payload[index];
    const {data} = await axios.delete(routerLinks("News", "api") + '/' + id);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}
export const patch_NEWS = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const {data} = await axios.patch(routerLinks("News", "api") + '/' + payload[index].id, payload[index]);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}

export const get_CATEGORY = async () => handGetParent(routerLinks("Category", "api") + "/", "categories")
export const change_CATEGORY = async (payload) => handPostParent(routerLinks("Category", "api") + "/", payload);
export const delete_CATEGORY = async (id) => {
  const { data } = await axios.delete(routerLinks("Category", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}

export const get_SLIDE = async () => handGetParent(routerLinks("Slide", "api") + "/", "slides")
export const change_SLIDE = async (payload) => handPostParent(routerLinks("Slide", "api") + "/", payload);
export const delete_SLIDE = async (id) => {
  const { data } = await axios.delete(routerLinks("Slide", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}
