import React from 'react';
import { Space, Popconfirm } from 'antd';
import moment from 'moment';
import intl from "react-intl-universal";

import DataTable from 'components/DataTable';
import Button from 'components/Button';

import routerLinks from "@/utils/routerLinks";
import { slugify } from "@/utils/core";

import messages from '../messages';
import {check_slug} from "../../User/service";


export const createColumns = (self, categories) => {
	return [
    {
      name: 'id',
      formItem: {
        type: 'hidden'
      }
    },
    {
      title: "Image",
      name: 'image',
      tableItem: {
        render: (url) => url ? <img src={url} alt="" width="50"/> : intl.formatMessage(messages.noImage)
      },
      formItem: {
        type: "media",
      },
    },
    {
      title: intl.formatMessage(messages.name),
      name: 'name',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        onBlur: (e, form) => {
          const { getFieldValue, setFieldsValue, validateFields } = form.refs.form
          if (!getFieldValue("slug")) {
            setFieldsValue({slug: slugify(e.currentTarget.value)});
            validateFields(["slug"])
          }
        },
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterName),
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.slug),
      name: 'slug',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        rules: [
          ({ getFieldValue }) => ({
            validator: async (rule, value) => {
              if (!!value && value.trim() !== "") {
                const data = await check_slug({
                  id: getFieldValue("id"),
                  slug: value,
                  model: "news"
                });
                if (data) return Promise.resolve()
                else return Promise.reject(intl.formatMessage(messages.theSlugHasAlreadyBeenTaken));
              } else return Promise.resolve()
            },
          }),
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterSlug)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.category),
      name: 'categories.id',
      tableItem: {
        filters: categories,
        filterType: "checkbox",
        render: (text, record) => record.categories.map(item => item.name).join(", ")
      },
    },
    {
      title: intl.formatMessage(messages.category),
      name: 'category_id',
      dict: categories,
      formItem: {
        type: "select",
        mode: "multiple",
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterCategory)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.description),
      name: 'description',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        type: "textarea"
      },
    },
    {
      title: intl.formatMessage(messages.body),
      name: 'body',
      formItem: {
        type: "editor"
      },
    },
    {
      title: intl.formatMessage(messages.createdAt),
      name: 'created_at',
      tableItem: {
        filterType: "datetime",
        sorter: true,
        render(text) {
          return moment(text).format("DD/MM/YYYY");
        }
      },
    },
    {
      title: intl.formatMessage(messages.updatedAt),
      name: 'updated_at',
      tableItem: {
        filterType: "datetime",
        sorter: true,
        render(text) {
          return moment(text).format("DD/MM/YYYY");
        }
      },
    },
    {
      title: intl.formatMessage(messages.operating),
      tableItem: {
        width: 180,
        render: (text, record) => {
          record.category_id = record.categories.map(item => item.id);
          return (
            <DataTable.Oper>
              <Space>
                {self.checkPermission(routerLinks("News", "api"), "PUT") && (
                  <Button tooltip={intl.formatMessage(messages.modify)} onClick={() => self.onUpdate(record)}>
                    <i className="las la-edit" />
                  </Button>
                )}
                {self.checkPermission(routerLinks("News", "api"), "PUT") && (
                  <Button
                    type={record.status === 0 ? "primary" : ""}
                    danger={record.status === 0}
                    tooltip={intl.formatMessage(messages.status)}
                    onClick={() => self.handlePatch([{ id: record.id, status: !record.status }])}
                  >
                    <i className={`las ${ record.status === 0 ? "la-low-vision" : "la-eye"}`} />
                  </Button>
                )}
                {self.checkPermission(routerLinks("News", "api"), "DELETE") && (
                  <Popconfirm
                    title={intl.formatMessage(messages.areYouSure)}
                    icon={<i className="las la-question-circle" />}
                    onConfirm={() => self.handleDelete([record])}
                  >
                    <Button tooltip={intl.formatMessage(messages.delete)}>
                      <i className="las la-trash-alt" />
                    </Button>
                  </Popconfirm>
                )}
              </Space>
            </DataTable.Oper>
          )
        }
      }
    }
  ]
};
export const createFormCategoryDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      onBlur: (e, form) => {
        const { getFieldValue, setFieldsValue, validateFields } = form.refs.form
        if (!getFieldValue("slug")) {
          setFieldsValue({slug: slugify(e.currentTarget.value)});
          validateFields(["slug"])
        }
      },
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName),
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.slug),
    name: 'slug',
    tableItem: {
      filterType: "input",
      sorter: true,
    },
    formItem: {
      rules: [
        ({ getFieldValue }) => ({
          validator: async (rule, value) => {
            if (!!value && value.trim() !== "") {
              const data = await check_slug({
                id: getFieldValue("id"),
                slug: value,
                model: "categories"
              });
              if (data) return Promise.resolve()
              else return Promise.reject(intl.formatMessage(messages.theSlugHasAlreadyBeenTaken));
            } else return Promise.resolve()
          },
        }),
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterSlug)
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.description),
    name: 'description',
    formItem: {
      type: 'textarea',
    },
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]

export const createFormSlideDrag = () => [
  {
    title: intl.formatMessage(messages.image),
    name: 'image',
    formItem: {
      type: "media",
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.selectYourImage),
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.title),
    name: 'title',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterTitle),
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.text),
    name: 'text',
    formItem: {
      type: 'textarea',
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterText),
        }
      ]
    },
  },
  {
    title: "Page",
    name: 'page',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName),
        }
      ]
    },
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]
