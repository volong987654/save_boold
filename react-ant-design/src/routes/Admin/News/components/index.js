import React from 'react';
import { connect } from 'dva';
import { Layout } from 'antd';
import intl from "react-intl-universal";

import BaseComponent from 'components/BaseComponent';
import Toolbar from 'components/Toolbar';
import DataTable from 'components/DataTable';
import Button from 'components/Button';
import { ModalForm, ModalDragList } from 'components/Modal';
import routerLinks from "@/utils/routerLinks";

import messages from '../messages';
import { createColumns, createFormCategoryDrag, createFormSlideDrag } from './columns';
import './index.less';

const { Content, Header, Footer } = Layout;
const { Pagination } = DataTable;

@connect(({ news, global, loading }) => ({
  news,
  global,
  loading: loading.models.news
}))
class News extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      record: null,
      visible: false,
      visibleDragCategory: false,
      visibleDragSlide: false,
      rows: []
    }
  }

  handleDelete = records => {
    const { rows } = this.state;
    const { dispatch } = this.props;

    dispatch({
      type: 'news/remove',
      payload: {
        records,
        success: () => {
          // If the operation is successful, exclude the deleted row in the selected row
          this.setState({
            rows: rows.filter(
              item => !records.some(jItem => jItem.rowKey === item.rowKey)
            )
          });
        }
      }
    });
  };

  handlePatch = (records) => {
    const { dispatch } = this.props;
    dispatch({ type: 'news/patch', payload: { records }});
  }

  render() {
    const { news, loading, dispatch, history, location } = this.props;
    const { pageData, categories, slides } = news;
    const columns = this.setSortFilter(pageData, createColumns(this, categories.map(item => ({ code: item.id, codeName: item.name }))));
    const { rows, record, visible, visibleDragCategory, visibleDragSlide } = this.state;

    const dataTableProps = {
      loading,
      columns,
      rowKey: 'id',
      dataItems: pageData,
      selectType: 'checkbox',
      showNum: false,
      isScroll: true,
      selectedRowKeys: rows.map(item => item.id),
      onChange: (page) => history.replace(location.pathname +pageData.toParams(page)),
      onSelect: (keys, rows) => this.setState({ rows })
    };
    const modalFormProps = {
      loading,
      record,
      visible,
      columns,
      modalOpts: {
        width: 700
      },
      onCancel: () => {
        this.setState({
          record: null,
          visible: false
        });
      },
      // Add and modify will enter this method,
      // can use the primary key or whether there is a record to distinguish the status
      onSubmit: values => {
        values = !record ? values : {...values, id: record.id}
        dispatch({
          type: 'news/save',
          payload: {
            values,
            success: () => {
              this.setState({
                record: null,
                visible: false
              });
            }
          }
        });
      }
    };
    const dragProps = (columns, data, key, visible, visibleKey, title, maxDepth, name, url, readOnly = false, callArray, checkCallArray) => {
      return {
        loading, columns, data, visible, title, maxDepth,
        callArray, checkCallArray, readOnly,
        onSubmit: (data, value) => {
          if (value) dispatch({
            type: `news/change${name}`,
            payload: {
              value, data,
              permissions: {
                "post": this.checkPermission(url, "POST"),
                "put" : this.checkPermission(url, "PUT")
              }
            }
          });
          else dispatch({ type: "news/@change", payload: { [key]: data }})
        },
        onDrag: (data) => dispatch({ type: "news/@change", payload: { [key]: data }}),
        onCancel: () => this.setState({ [visibleKey]: false }),
        onSave: () => {
          dispatch({
            type: `news/change${name}`,
            payload: {
              value: null, data,
              permissions: {
                "post": this.checkPermission(url, "POST"),
                "put" : this.checkPermission(url, "PUT")
              }
            }
          })
          this.setState({ [visibleKey]: false });
        },
        onDelete: (id) => {
          if (id > 0) dispatch({ type: `news/remove${name}`, payload: id })
          else dispatch({ type: "news/@change", payload: this.deleteItem(data, id) })
        },
        checkPermission: (method) => this.checkPermission(url, method),
      };
    }
    const modalDrapCategoryProps = dragProps(
      createFormCategoryDrag(), categories, "categories", visibleDragCategory, "visibleDragCategory",
      intl.formatMessage(messages.category), 1,"Category", routerLinks("Category", "api"));

    const modalDragSlideProps = dragProps(
      createFormSlideDrag(), slides, "slides", visibleDragSlide, "visibleDragSlide",
      intl.formatMessage(messages.slide), 1,"Slide", routerLinks("Slide", "api"));

    return (
      <Layout className="full-layout news-page">
        <Header>
          <Toolbar
            appendLeft={
              <Button.Group>
                {this.checkPermission(routerLinks("News", "api"), "POST") && (
                  <Button type="primary" icon={<i className="las la-plus" />} onClick={this.onAdd}>
                    {intl.formatMessage(messages.add)}
                  </Button>
                )}
                {this.checkPermission(routerLinks("News", "api"), "DELETE") && (
                  <Button
                    type="primary"
                    disabled={!rows.length}
                    onClick={e => this.onDelete(rows)}
                    icon={<i className="las la-trash-alt" />}
                  >
                    {intl.formatMessage(messages.delete)}
                  </Button>
                )}
              </Button.Group>
            }
          >
            <Button.Group>
              {this.checkPermission(routerLinks("Slide", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-images" />} onClick={() => this.setState({ visibleDragSlide: true })}
                >
                  {intl.formatMessage(messages.slide)}
                </Button>
              )}
              {this.checkPermission(routerLinks("Category", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-sitemap" />} onClick={() => this.setState({ visibleDragCategory: true })}
                >
                  {intl.formatMessage(messages.category)}
                </Button>
              )}
            </Button.Group>
          </Toolbar>
        </Header>
        <Content className="has-footer">
          <DataTable {...dataTableProps} />
        </Content>
        <Footer>
          <Pagination {...dataTableProps} />
        </Footer>
        <ModalForm {...modalFormProps} />
        {this.checkPermission(routerLinks("Category", "api")) && <ModalDragList {...modalDrapCategoryProps} />}
        {this.checkPermission(routerLinks("Slide", "api")) && <ModalDragList {...modalDragSlideProps} />}
      </Layout>
    );
  }
}

export default News;
