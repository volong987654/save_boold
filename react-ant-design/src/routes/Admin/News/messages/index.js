export const scopeUser = 'routes.User';
export const scope = 'routes.News';

export default {
  createdAt: {
    id: `${scopeUser}.createdAt`,
    defaultMessage: 'Created at',
  },
  updatedAt: {
    id: `${scopeUser}.updatedAt`,
    defaultMessage: 'Updated at',
  },
  operating: {
    id: `${scopeUser}.operating`,
    defaultMessage: 'Operating',
  },
  modify: {
    id: `${scopeUser}.modify`,
    defaultMessage: 'Modify',
  },
  status: {
    id: `${scopeUser}.status`,
    defaultMessage: 'Status',
  },
  areYouSure: {
    id: `${scopeUser}.areYouSure`,
    defaultMessage: 'Are you sure?',
  },
  delete: {
    id: `${scopeUser}.delete`,
    defaultMessage: 'Delete',
  },
  add: {
    id: `${scopeUser}.add`,
    defaultMessage: 'Add',
  },
  noImage: {
    id: `${scopeUser}.noImage`,
    defaultMessage: 'No Image',
  },

  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  pleaseEnterName: {
    id: `${scope}.pleaseEnterName`,
    defaultMessage: 'Please enter name',
  },
  slug: {
    id: `${scope}.slug`,
    defaultMessage: 'Slug',
  },
  theSlugHasAlreadyBeenTaken: {
    id: `${scope}.theSlugHasAlreadyBeenTaken`,
    defaultMessage: 'The slug has already been taken',
  },
  pleaseEnterSlug: {
    id: `${scope}.pleaseEnterSlug`,
    defaultMessage: 'Please enter slug',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Description',
  },
  body: {
    id: `${scope}.body`,
    defaultMessage: 'Body',
  },
  category: {
    id: `${scope}.category`,
    defaultMessage: 'Category',
  },
  pleaseEnterCategory: {
    id: `${scope}.pleaseEnterCategory`,
    defaultMessage: 'Please enter category',
  },
  slide: {
    id: `${scope}.slide`,
    defaultMessage: 'Slide',
  },
  image: {
    id: `${scope}.image`,
    defaultMessage: 'Image',
  },
  selectYourImage: {
    id: `${scope}.selectYourImage`,
    defaultMessage: 'Select your image',
  },
  title: {
    id: `${scope}.titleInput`,
    defaultMessage: 'Title',
  },
  pleaseEnterTitle: {
    id: `${scope}.pleaseEnterTitle`,
    defaultMessage: 'Please enter title',
  },
  text: {
    id: `${scope}.text`,
    defaultMessage: 'Text',
  },
  pleaseEnterText: {
    id: `${scope}.pleaseEnterText`,
    defaultMessage: 'Please enter text',
  },
  page: {
    id: `${scope}.page`,
    defaultMessage: 'Page',
  },
  pleaseEnterPage: {
    id: `${scope}.pleaseEnterPage`,
    defaultMessage: 'Please enter page',
  },
};
