import {routerRedux} from "dva";

import modelEnhance from '@/utils/modelEnhance';
import routerLinks from "@/utils/routerLinks";
import {getList_SETTING,  update_SETTING} from "../service";

export default modelEnhance({
  namespace: 'setting',

  state: {
    settings: []
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname, search }) => {
        if (pathname === routerLinks("Website")) {
          dispatch({ type: 'init' });
        }
      });
    }
  },

  effects: {
    *init({ payload }, { put }) {
      yield put({type: 'getSetting'});
    },
    *getSetting({ payload }, { call, put }) {
      const settings = yield call(getList_SETTING)
      yield put({ type: 'success', payload: { settings } });
    },
    *update({payload}, {call, put}) {
      yield call(update_SETTING, payload);
      yield put(routerRedux.replace(routerLinks("Login")));
    }
  },

  reducers: {
    success(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  }
});
