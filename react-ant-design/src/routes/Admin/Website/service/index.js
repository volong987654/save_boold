import axios from 'axios';
import routerLinks from "@/utils/routerLinks";
import config from '@/config';

export const getList_SETTING = async () => {
  const { data } = await axios.get(routerLinks("Website", "api"));
  return data;
}

export const update_SETTING = async (payload) => {
  const { data } = await axios.post(routerLinks("Website", "api"), payload);
  if (data.message) config.notice.success(data.message);
  return data.data;
}
