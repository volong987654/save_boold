import React from 'react';
import { connect } from 'dva';
import { Layout } from 'antd';

import BaseComponent from 'components/BaseComponent';
import Panel from 'components/Panel';
import Form from 'components/Form';
import './index.less';
import intl from "react-intl-universal";
import messages from "../messages";

const { Content } = Layout;

@connect(({ setting, loading }) => ({
  setting,
  loading: loading.effects['setting/update']
}))
class Setting extends BaseComponent {
  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: "setting/@change", payload: { settings: [] }})
  }
  
  handleSubmit = values => {
    const { dispatch } = this.props;
    dispatch({
      type: 'setting/update',
      payload: {
        ...values,
      }
    });
  };

  render() {
    const { setting, loading } = this.props;
    const { settings } = setting;
    const columns = [];
    const record = {}
    settings.map(item => {
      columns.push({ title: item.title, name: item.name, formItem: { type: item.type } })
      record[item.name] = item.value;
      return null;
    })
    const formProps = {
      loading,
      record,
      columns,
      onSubmit: this.handleSubmit,
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      cols: {
        xs: 24,
        md: 12,
        xl: 12
      },
    };

    return (
      <Layout className="full-layout page setting-page">
        <Content>
          <Panel title={intl.formatMessage(messages.subTitle)}>
            { columns.length > 0 && <Form ref="form" {...formProps} />}
          </Panel>
        </Content>
      </Layout>
    );
  }
}

export default Setting
