export const scope = 'routes.Website';

export default {
  subTitle: {
    id: `${scope}.subTitle`,
    defaultMessage: 'Change Setting Website',
  },
};
