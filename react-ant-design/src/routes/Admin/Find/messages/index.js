export const scopeUser = 'routes.User';
export const scope = 'routes.Find';

export default {
  noImage: {
    id: `${scopeUser}.noImage`,
    defaultMessage: 'No Image',
  },
  avatar: {
    id: `${scopeUser}.avatar`,
    defaultMessage: 'Avatar',
  },
  name: {
    id: `${scopeUser}.name`,
    defaultMessage: 'Name',
  },
  email: {
    id: `${scopeUser}.email`,
    defaultMessage: 'Email',
  },
  address: {
    id: `${scopeUser}.address`,
    defaultMessage: "Address",
  },
  phone: {
    id: `${scopeUser}.phone`,
    defaultMessage: "Phone",
  },
  gender: {
    id: `${scopeUser}.gender`,
    defaultMessage: "Gender",
  },
  male: {
    id: `${scopeUser}.male`,
    defaultMessage: "Male",
  },
  female: {
    id: `${scopeUser}.female`,
    defaultMessage: "Female",
  },
  identityCard: {
    id: `${scopeUser}.identityCard`,
    defaultMessage: "Identity Card",
  },
  birthday: {
    id: `${scopeUser}.birthday`,
    defaultMessage: "Birthday",
  },
  createdAt: {
    id: `${scopeUser}.createdAt`,
    defaultMessage: 'Created at',
  },
  updatedAt: {
    id: `${scopeUser}.updatedAt`,
    defaultMessage: 'Updated at',
  },
  operating: {
    id: `${scopeUser}.operating`,
    defaultMessage: 'Operating',
  },
  modify: {
    id: `${scopeUser}.modify`,
    defaultMessage: 'Modify',
  },
  status: {
    id: `${scopeUser}.status`,
    defaultMessage: 'Status',
  },
  areYouSure: {
    id: `${scopeUser}.areYouSure`,
    defaultMessage: 'Are you sure?',
  },
  delete: {
    id: `${scopeUser}.delete`,
    defaultMessage: 'Delete',
  },
  add: {
    id: `${scopeUser}.add`,
    defaultMessage: 'Add',
  },
  hospital: {
    id: `${scopeUser}.hospital`,
    defaultMessage: 'Hospital',
  },
  lastTime: {
    id: `${scopeUser}.lastTime`,
    defaultMessage: 'Last Time',
  },

  instruction: {
    id: `${scope}.instruction`,
    defaultMessage: 'Instruction',
  },
  pleaseSelectInstruction: {
    id: `${scope}.pleaseSelectInstruction`,
    defaultMessage: 'Please select instruction',
  },
  user: {
    id: `${scope}.user`,
    defaultMessage: 'User',
  },
  donor: {
    id: `${scope}.donor`,
    defaultMessage: 'Donor',
  },
  pleaseSelectUser: {
    id: `${scope}.pleaseSelectUser`,
    defaultMessage: 'Please select user',
  },
  pleaseSelectHospital: {
    id: `${scope}.pleaseSelectHospital`,
    defaultMessage: 'Please select hospital',
  },
  blood: {
    id: `${scope}.blood`,
    defaultMessage: 'Blood',
  },
  pleaseSelectBlood: {
    id: `${scope}.pleaseSelectBlood`,
    defaultMessage: 'Please select blood',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Description',
  },
  pleaseEnterDescription: {
    id: `${scope}.pleaseEnterDescription`,
    defaultMessage: 'Please enter description',
  },
  relativesName: {
    id: `${scope}.relativesName`,
    defaultMessage: 'Relatives Name',
  },
  pleaseEnterRelativesName: {
    id: `${scope}.pleaseEnterRelativesName`,
    defaultMessage: 'Please enter relatives name',
  },
  relativesPhone: {
    id: `${scope}.relativesPhone`,
    defaultMessage: 'Relatives Phone',
  },
  pleaseEnterRelativesPhone: {
    id: `${scope}.pleaseEnterRelativesPhone`,
    defaultMessage: 'Please enter relatives phone',
  },
  donorSelection: {
    id: `${scope}.donorSelection`,
    defaultMessage: 'Donor selection',
  },
};
