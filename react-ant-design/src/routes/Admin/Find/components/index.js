import React from 'react';
import { connect } from 'dva';
import { Layout } from 'antd';
import intl from "react-intl-universal";

import BaseComponent from 'components/BaseComponent';
import Toolbar from 'components/Toolbar';
import DataTable from 'components/DataTable';
import Button from 'components/Button';
import { ModalForm, ModalTable, } from 'components/Modal';
import routerLinks from "@/utils/routerLinks";
import PageHelper from '@/utils/pageHelper';

import messages from '../messages';
import { createColumns, createColumnsModalTable } from './columns';
import { getListDonor_USER } from "../../User/service";

import './index.less';

const { Content, Header, Footer } = Layout;
const { Pagination } = DataTable;

@connect(({ find, global, loading }) => ({
  find,
  global,
  loading: loading.models.find
}))
class Find extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      record: null,
      visible: false,
      visibleTable: false,
      idFind: null,
      idDonor: null,
      tempId: null,
      dataItems: PageHelper.create(),
      rows: []
    }
  }

  handleDelete = records => {
    const { rows } = this.state;
    const { dispatch } = this.props;

    dispatch({
      type: 'find/remove',
      payload: {
        records,
        success: () => {
          // If the operation is successful, exclude the deleted row in the selected row
          this.setState({
            rows: rows.filter(
              item => !records.some(jItem => jItem.rowKey === item.rowKey)
            )
          });
        }
      }
    });
  };

  handlePatch = (records) => {
    const { dispatch } = this.props;
    dispatch({ type: 'find/patch', payload: { records }});
  }

  showUser = async (record) => {
    const data = await getListDonor_USER({blood_id: record.blood_id})
    this.state.dataItems.setPage({list: data})
    this.setState({visibleTable: true, idFind: record.id, idDonor: null, tempId: record.donor_id})
  }

  render() {
    const { find, loading, dispatch, history, location } = this.props;
    const { pageData, hospitals, bloods, users} = find;
    const columns = this.setSortFilter(pageData, createColumns(this, hospitals, bloods, users));
    const { rows, record, visible, visibleTable, dataItems, idDonor, idFind, tempId} = this.state;

    const dataTableProps = {
      loading,
      columns,
      dataItems: pageData,
      selectedRowKeys: rows.map(item => item.id),
      onChange: (page) => history.replace(location.pathname +pageData.toParams(page)),
      onSelect: (keys, rows) => this.setState({ rows })
    };
    const modalFormProps = {
      loading,
      record,
      visible,
      columns,
      onCancel: () => this.setState({ record: null, visible: false }),
      onSubmit: values => {
        dispatch({
          type: 'find/save',
          payload: {
            values: !record ? values : {...values, id: record.id},
            success: () => this.setState({ record: null, visible: false })
          }
        });
      }
    };
    const modalTableProps = {
      loading,
      visible: visibleTable,
      tableProps: {
        showNum: false,
        pagination: false,
        value: [tempId],
        dataItems,
        selectType: "radio",
        onChange: (keys) => this.setState({idDonor: keys[0]})
      },
      width: 1300,
      columns: createColumnsModalTable(this),
      showOk: idDonor !== null,
      onCancel: () => this.setState({ visibleTable: false }),
      onSubmit: () => this.handlePatch([{id: idFind, "donor_id": idDonor}])
    }

    return (
      <Layout className="full-layout find-page">
        <Header>
          <Toolbar
            appendLeft={
              <Button.Group>
                {this.checkPermission(routerLinks("Find", "api"), "POST") && (
                  <Button type="primary" icon={<i className="las la-plus" />} onClick={this.onAdd}>
                    {intl.formatMessage(messages.add)}
                  </Button>
                )}
                {this.checkPermission(routerLinks("Find", "api"), "DELETE") && (
                  <Button
                    type="primary"
                    disabled={!rows.length}
                    onClick={e => this.onDelete(rows)}
                    icon={<i className="las la-trash-alt" />}
                  >
                    {intl.formatMessage(messages.delete)}
                  </Button>
                )}
              </Button.Group>
            }
          >
          </Toolbar>
        </Header>
        <Content className="has-footer">
          <DataTable {...dataTableProps} />
        </Content>
        <Footer>
          <Pagination {...dataTableProps} />
        </Footer>
        <ModalForm {...modalFormProps} />
        <ModalTable {...modalTableProps} />
      </Layout>
    );
  }
}

export default Find;
