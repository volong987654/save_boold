import React from 'react';
import { Space, Popconfirm } from 'antd';
import intl from "react-intl-universal";

import DataTable from 'components/DataTable';
import Button from 'components/Button';
import routerLinks from "@/utils/routerLinks";
import { formatTreeselect } from "@/utils/core";

import messages from '../messages';
import {check_slug} from "../../User/service";
import moment from "moment";

export const createColumns = (self, hospitals, bloods, users) => {
	return [
    {
      name: 'id',
      formItem: {
        type: 'hidden'
      }
    },
    {
      title: intl.formatMessage(messages.instruction),
      name: 'instruction',
      tableItem: {
        render: (url) => url ? <img src={url} alt="" width="50"/> : intl.formatMessage(messages.noImage)
      },
      formItem: {
        type: "media",
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectInstruction)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.user),
      name: 'users.name|finds.user_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.user.name;
        }
      },
    },
    {
      title: intl.formatMessage(messages.user),
      name: 'user_id',
      dict: users.map(item => ({ code: item.id, codeName: item.name })),
      formItem: {
        type: "select",
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectUser)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.hospital),
      name: 'hospitals.name|finds.hospital_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.hospital.name;
        }
      },
    },
    {
      title: intl.formatMessage(messages.hospital),
      name: 'hospital_id',
      formItem: {
        type: "treeselect",
        treeData: formatTreeselect(hospitals),
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectHospital)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.blood),
      name: 'bloods.name|finds.blood_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.blood.name;
        }
      },
    },
    {
      title: intl.formatMessage(messages.blood),
      name: 'blood_id',
      formItem: {
        type: "treeselect",
        treeData: formatTreeselect(bloods),
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.blood)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.description),
      name: 'description',
      formItem: {
        type: "textarea",
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterDescription)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.relativesName),
      name: 'relatives_name',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterRelativesName)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.relativesPhone),
      name: 'relatives_phone',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterRelativesPhone)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.donor),
      name: 'users.name|finds.donor_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.donor ? record.donor.name : null;
        }
      },
    },
    {
      title: intl.formatMessage(messages.operating),
      tableItem: {
        width: 180,
        render: (text, record) => {
          return (
            <DataTable.Oper>
              <Space>
                {self.checkPermission(routerLinks("Find", "api"), "PUT") && (
                  <Button tooltip={intl.formatMessage(messages.modify)} onClick={() => self.onUpdate(record)}>
                    <i className="las la-edit" />
                  </Button>
                )}
                {self.checkPermission(routerLinks("Find", "api"), "PUT") && (
                  <Button tooltip={intl.formatMessage(messages.donorSelection)} onClick={() => self.showUser(record)}>
                    <i className="las la-user-check" />
                  </Button>
                )}
                {self.checkPermission(routerLinks("Find", "api"), "PUT") && (
                  <Button
                    type={record.status === 0 ? "primary" : ""}
                    danger={record.status === 0}
                    tooltip={intl.formatMessage(messages.status)}
                    onClick={() => self.handlePatch([{ id: record.id, status: !record.status }])}
                  >
                    <i className={`las ${ record.status === 0 ? "la-low-vision" : "la-eye"}`} />
                  </Button>
                )}
                {self.checkPermission(routerLinks("Find", "api"), "DELETE") && (
                  <Popconfirm
                    title={intl.formatMessage(messages.areYouSure)}
                    icon={<i className="las la-question-circle" />}
                    onConfirm={() => self.handleDelete([record])}
                  >
                    <Button tooltip={intl.formatMessage(messages.delete)}>
                      <i className="las la-trash-alt" />
                    </Button>
                  </Popconfirm>
                )}
              </Space>
            </DataTable.Oper>
          )
        }
      }
    }
  ]
};

export const createColumnsModalTable = (self) => {
  return [
    {
      name: 'id',
      formItem: {
        type: 'hidden'
      }
    },
    {
      title: intl.formatMessage(messages.avatar),
      name: 'avatar',
      tableItem: {
        render: (url) => url ? <img src={url} alt="" width="50"/> : intl.formatMessage(messages.noImage)
      },
    },
    {
      title: intl.formatMessage(messages.name),
      name: 'name',
      tableItem: {},
    },
    {
      title: intl.formatMessage(messages.email),
      name: 'email',
      tableItem: {},
    },
    {
      title: intl.formatMessage(messages.address),
      name: 'address',
      tableItem: {},
    },
    {
      title: intl.formatMessage(messages.phone),
      name: 'phone',
      tableItem: {},
    },
    {
      title: intl.formatMessage(messages.gender),
      name: 'gender',
      tableItem: {
        render: (gender) => gender === 1 ? intl.formatMessage(messages.male) : intl.formatMessage(messages.female)
      },
    },
    {
      title: intl.formatMessage(messages.birthday),
      name: 'birthday',
      tableItem: {
        render(text) {
          return moment(text).format("DD/MM/YYYY");
        }
      },
    },
    {
      title: intl.formatMessage(messages.lastTime),
      name: 'last_time',
      tableItem: {
        render(text) {
          return moment(text).format("DD/MM/YYYY");
        }
      },
    },

  ]
}
