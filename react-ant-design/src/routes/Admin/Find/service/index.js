import axios from 'axios';
import PageHelper from '@/utils/pageHelper';
import { handGetParent } from '@/utils/methodRequest';
import config from '@/config';
import routerLinks from "@/utils/routerLinks";

export const getList_FIND = async (payload) => {
  const { data } = await axios.get(routerLinks("Find", "api"), {params: PageHelper.requestFormat(payload)});
  return PageHelper.responseFormat(data);
}
export const save_FIND = async (payload) => {
  const { data } = await axios.post(routerLinks("Find", "api"), payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const update_FIND = async (payload) => {
  const { data } = await axios.put(routerLinks("Find", "api") + '/' + payload.id, payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const delete_FIND = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const id = payload[index];
    const {data} = await axios.delete(routerLinks("Find", "api") + '/' + id);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}
export const patch_FIND = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const {data} = await axios.patch(routerLinks("Find", "api") + '/' + payload[index].id, payload[index]);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}

export const select_HOSPITAL = async () => handGetParent(routerLinks("Hospital", "api") + "/select", "hospitals")
export const select_BLOOD = async () => handGetParent(routerLinks("Blood", "api") + "/select", "bloods")
export const select_USER = async () => handGetParent(routerLinks("User", "api") + "/select", "users")
