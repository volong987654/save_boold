import modelEnhance from '@/utils/modelEnhance';
import PageHelper from '@/utils/pageHelper';
import routerLinks from "@/utils/routerLinks";

import {
  getList_FIND,
  save_FIND,
  update_FIND,
  patch_FIND,
  delete_FIND,
  select_BLOOD,
  select_USER,
  select_HOSPITAL,
} from "../service"

/**
* true when the page is first loaded
* Can use this value to prevent switching pages when
* Initialize data multiple times
*/
let url = true;
export default modelEnhance({
  namespace: 'find',

  state: {
    pageData: PageHelper.create(),
    hospitals: [],
    bloods: [],
    users: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname, search }) => {
        if (pathname === routerLinks("Find") && !!search && url) {
          url = false
          setTimeout(() => url = true, 500);
          const payload = PageHelper.URLToArray(search);
          dispatch({ type: 'init', payload });
        }
      });
    }
  },

  effects: {
    // Enter page load
    *init({ payload }, { put, select }) {
      const { pageData } = yield select(state => state.find);
      yield put({
        type: 'getPageInfo',
        payload: {
          pageData: pageData.setPage(payload)
        }
      });
      yield put({type: 'getAllAttributes'});
    },
    *getAllAttributes({ payload }, { put }) {
      yield put({type: 'getSelectBlood'});
      yield put({type: 'getSelectUser'});
      yield put({type: 'getSelectHospital'});
    },
    // Get paging data
    *getPageInfo({ payload }, { call }) {
      const { pageData } = payload;
      const data = yield call (getList_FIND, pageData)
      pageData.setPage(data);
    },
    // Save after querying paging
    *save({ payload }, { put, select, call }) {
      const { values, success } = payload;
      const { pageData } = yield select(state => state.find);

      if (values.id) yield call(update_FIND, values);
      else yield call(save_FIND, values);

      yield put({ type: 'getPageInfo', payload: { pageData } });
      success();
    },
    // Delete after querying paging
    *remove({ payload }, { put, select, call }) {
      const { records, success } = payload;
      const { pageData } = yield select(state => state.find);
      const data = records.map(item => item.id)
      yield call(delete_FIND, data);
      yield put({ type: 'getPageInfo', payload: { pageData } });
      yield put({type: 'getAllAttributes'});
      success();
    },
    *patch({ payload }, { select, call, put }) {
      const { records } = payload;
      const { pageData } = yield select(state => state.find);
      yield call(patch_FIND, records);
      yield put({ type: 'getPageInfo', payload: { pageData } });
    },

    *getSelectBlood({ payload }, { call, put }) {
      const data = yield call(select_BLOOD)
      yield put({ type: 'success', payload: data });
    },
    *getSelectHospital({ payload }, { call, put }) {
      const data = yield call(select_HOSPITAL)
      yield put({ type: 'success', payload: data });
    },
    *getSelectUser({ payload }, { call, put }) {
      const data = yield call(select_USER)
      yield put({ type: 'success', payload: data });
    },
  },

  reducers: {
    success(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  }
});
