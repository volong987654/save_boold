import axios from 'axios';
import PageHelper from '@/utils/pageHelper';
import { handGetParent, handPostParent } from '@/utils/methodRequest';
import config from '@/config';
import routerLinks from "@/utils/routerLinks";

export const getList_USER = async (payload) => {
  const { data } = await axios.get(routerLinks("User", "api"), {params: PageHelper.requestFormat(payload)});
  return PageHelper.responseFormat(data);
}
export const save_USER = async (payload) => {
  const { data } = await axios.post(routerLinks("User", "api"), payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const update_USER = async (payload) => {
  const { data } = await axios.put(routerLinks("User", "api") +'/' + payload.id, payload);
  if (data.message) config.notice.success(data.message);
  return data;
}
export const delete_USER = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const id = payload[index];
    const {data} = await axios.delete(routerLinks("User", "api") + '/' + id);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}
export const patch_USER = async (payload) => {
  for (let index = 0; index < payload.length; index++) {
    const {data} = await axios.patch(routerLinks("User", "api") + '/' + payload[index].id, payload[index]);
    if (data.message) config.notice.success(data.message);
  }
  return null;
}
export const getListDonor_USER = async (payload) => {
  const { data } = await axios.get(routerLinks("User", "api") + '/donor', {params: payload});
  return data
}

export const get_ROLE = async () => handGetParent(routerLinks("Role", "api") + "/", "roles")
export const change_ROLE = async (payload) => handPostParent(routerLinks("Role", "api")+'/', payload);
export const delete_ROLE = async (id) => {
  const { data } = await axios.delete(routerLinks("Role", "api")+ '/' + id);
  if (data.message) config.notice.success(data.message);
}

export const get_PERMISSION = async () => handGetParent(routerLinks("Permission", "api") + "/", "permissions")
export const change_PERMISSION = async (payload) => handPostParent(routerLinks("Permission", "api") + "/", payload);
export const delete_PERMISSION = async (id) => {
  const { data } = await axios.delete(routerLinks("Permission", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}

export const get_BLOOD = async () => handGetParent(routerLinks("Blood", "api") + "/", "bloods")
export const change_BLOOD = async (payload) => handPostParent(routerLinks("Blood", "api") + "/", payload);
export const delete_BLOOD = async (id) => {
  const { data } = await axios.delete(routerLinks("Blood", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}

export const get_ORGANIZATION = async () => handGetParent(routerLinks("Organization", "api") + "/", "organizations")
export const change_ORGANIZATION = async (payload) => handPostParent(routerLinks("Organization", "api") + "/", payload);
export const delete_ORGANIZATION = async (id) => {
  const { data } = await axios.delete(routerLinks("Organization", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}

export const change_ROLE_PERMISSION = async (payload) => {
  const { data: oldData, permissions } = payload;
  for (let index = 0; index < oldData.length; index++) {
    const method = oldData[index].id > 0 ? 'put' : 'post';
    if (permissions[method]) {
      const {data} = await axios[method](routerLinks("RolePermission", "api") + '/' + (method === 'put' ? oldData[index].id : ''), oldData[index]);
      if (data.message) config.notice.success(data.message);
    }
  }
  return null;
}
export const delete_ROLE_PERMISSION = async (id) => {
  const { data } = await axios.delete(routerLinks("RolePermission", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
  return null;
}

export const check_slug = async (obj) => {
  const { data } = await axios.post(routerLinks("CheckSlug", "api"), obj)
  return data.data;
}

export const get_HOSPITAL = async () => handGetParent(routerLinks("Hospital", "api") + "/", "hospitals")
export const change_HOSPITAL = async (payload) => handPostParent(routerLinks("Hospital", "api") + '/', payload);
export const delete_HOSPITAL = async (id) => {
  const { data } = await axios.delete(routerLinks("Hospital", "api") + '/' + id);
  if (data.message) config.notice.success(data.message);
}
