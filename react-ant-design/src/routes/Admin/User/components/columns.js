import React from 'react';
import { Space, Popconfirm, Badge, Popover, Input } from 'antd';
import moment from 'moment';
import intl from "react-intl-universal";

import DataTable from 'components/DataTable';
import Button from 'components/Button';
import routerLinks from "@/utils/routerLinks";
import { formatEmail } from "@/utils/formatRegex";
import { formatTreeselect } from "@/utils/core";

import messages from '../messages';
import { check_slug } from "../service";

const { TextArea } = Input;

export const createColumns = (self, roles, bloods, hospitals, organizations) => {
	return [
    {
      name: 'id',
      formItem: {
        type: 'hidden'
      }
    },
    {
      title: intl.formatMessage(messages.avatar),
      name: 'avatar',
      tableItem: {
        render: (url) => url ? <img src={url} alt="" width="50"/> : intl.formatMessage(messages.noImage)
      },
      formItem: {
        type: "media",
      },
    },
    {
      title: intl.formatMessage(messages.bio),
      name: 'bio',
      formItem: {
        type: "editor",
      },
    },
    {
      title: intl.formatMessage(messages.name),
      name: 'name',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterName)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.email),
      name: 'email',
      tableItem: {
        filterType: "input",
        sorter: true,
      },
      formItem: {
        rules: [
          ({ getFieldValue }) => ({
            validator: async (rule, value) => {
              if (!!value && value.trim() !== "" && formatEmail.test(value)) {
                const data = await check_slug({
                  id: getFieldValue("id"),
                  email: value,
                  model: "users"
                });
                if (data) return Promise.resolve()
                else return Promise.reject(intl.formatMessage(messages.thisEmailAddressIsAlreadyTaken));
              } else return Promise.resolve()
            },
          }),
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterEmail)
          },
          {
            type: "email",
            message: intl.formatMessage(messages.pleaseEnterTheCorrectEmailFormat)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.password),
      name: 'password',
      formItem: {
        type: "password",
        repeat: true,
      },
    },
    {
      title: intl.formatMessage(messages.role),
      name: 'roles.name|users.role_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.role.name;
        }
      },
    },
    {
      title: intl.formatMessage(messages.role),
      name: 'role_id',
      dict: roles,
      formItem: {
        type: 'select',
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectRole)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.bloodType),
      name: 'bloods.name|users.blood_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.blood ? record.blood.name : "";
        }
      },
    },
    {
      title: intl.formatMessage(messages.bloodType),
      name: 'blood_id',
      formItem: {
        type: "treeselect",
        treeData: formatTreeselect(bloods),
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectBloodType)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.organization),
      name: 'organizations.name|users.organization_id',
      tableItem: {
        sorter: true,
        render(text, record) {
          return record.organization ? record.organization.name : "";
        }
      },
    },
    {
      title: intl.formatMessage(messages.organization),
      name: 'organization_id',
      dict: organizations,
      formItem: {
        type: "select",
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectOrganization)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.hospital),
      name: "hospitals.id",
      tableItem: {
        filters: hospitals.map((item) => ({ code: item.id, codeName: item.name })),
        filterType: "checkbox",
        render: (text, record) => record.hospitals.map(item => item.name).join(", ")
      }
    },
    {
      title: intl.formatMessage(messages.hospital),
      name: "hospital_id",
      formItem: {
        type: "treeselect",
        multiple: true,
        treeData: formatTreeselect(hospitals),
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.hospital)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.job),
      name: 'job',
      formItem: {},
    },
    {
      title: intl.formatMessage(messages.address),
      name: 'address',
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterAddress)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.phone),
      name: 'phone',
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterAddress)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.gender),
      name: 'gender',
      dict: [{code: 1, codeName: intl.formatMessage(messages.male)}, {code: 2, codeName: intl.formatMessage(messages.female)}],
      formItem: {
        type: 'select',
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseSelectGender)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.identityCard),
      name: 'identity_card',
      formItem: {
        rules: [
          {
            required: true,
            message: intl.formatMessage(messages.pleaseEnterIdentityCard)
          }
        ]
      },
    },
    {
      title: intl.formatMessage(messages.birthday),
      name: 'birthday',
      formItem: {
        type: "date",
      },
    },
    {
      title: intl.formatMessage(messages.lastTime),
      name: 'last_time',
      tableItem: {
        filterType: "datetime",
        sorter: true,
        render(text) {
          return moment(text).format("DD/MM/YYYY");
        }
      },
    },
    {
      title: intl.formatMessage(messages.operating),
      tableItem: {
        width: 180,
        render: (text, record) => {
          record.birthday = moment.isMoment(record.birthday) ? record.birthday : moment(record.birthday);
          record.hospital_id = record.hospitals.map(item => item.id);

          return (
            <DataTable.Oper>
              <Space>
                {self.checkPermission(routerLinks("User", "api"), "PUT") && (
                  <Button tooltip={intl.formatMessage(messages.modify)} onClick={() => self.onUpdate(record)}>
                    <i className="las la-edit" />
                  </Button>
                )}
                {self.checkPermission(routerLinks("User", "api"), "PUT") && (
                  <Popover content={<><TextArea
                    value={self.state.note}
                    onChange={({ target: { value } }) => self.setState({ note: value })}
                    autoSize={{ minRows: 3, maxRows: 5 }}
                  /><Button size="small" type="primary" onClick={() => self.handlePatch([{ id: record.id, note: self.state.note }])} block>Save</Button></>}
                   title={intl.formatMessage(messages.note)}
                           onVisibleChange={(visible) => self.setState({ note: visible ? record.note : "" })}
                  >
                    <Button>
                      <i className="las la-sticky-note" />
                    </Button>
                  </Popover>
                )}
                {self.checkPermission(routerLinks("User", "api"), "PUT") && (
                  <Button
                    type={record.status === 0 ? "primary" : ""}
                    danger={record.status === 0}
                    tooltip={intl.formatMessage(messages.status)}
                    onClick={() => self.handlePatch([{ id: record.id, status: !record.status }])}
                  >
                    <i className={`las ${ record.status === 0 ? "la-low-vision" : "la-eye"}`} />
                  </Button>
                )}
                <Badge count={record.history.length}>
                  <Button
                    tooltip={intl.formatMessage(messages.history)}
                    onClick={() => {
                      self.state.dataItems.setPage({list: record.history})
                      self.setState({ visibleTable: true  })
                    }}
                    type={record.history.length > 0 ? "primary" : ""}
                  >
                    <i className="las la-clock" />
                  </Button>
                </Badge>
                {self.checkPermission(routerLinks("User", "api"), "DELETE") && (
                  <Popconfirm
                    title={intl.formatMessage(messages.areYouSure)}
                    icon={<i className="las la-question-circle" />}
                    onConfirm={() => self.handleDelete([record])}
                  >
                    <Button tooltip={intl.formatMessage(messages.delete)}>
                      <i className="las la-trash-alt" />
                    </Button>
                  </Popconfirm>
                )}
              </Space>
            </DataTable.Oper>
          )
        }
      }
    }
  ]
};
export const createFormBloodDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.description),
    name: 'description',
    formItem: {
      type: "textarea"
    }
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]
export const createColumnsModalTable = () => [
  {
    title: "Time",
    name: 'time',
    tableItem: {
      render(text) {
        return moment(text).format("DD/MM/YYYY");
      }
    },
  },
  {
    title: "Health",
    name: 'health',
    tableItem: {}
  },
  {
    title: "Unit",
    name: 'unit',
    tableItem: {}
  },
  {
    title: "Erythrocyte",
    name: 'erythrocyte',
    tableItem: {}
  },
  {
    title: "Platelet",
    name: 'platelet',
    tableItem: {}
  },
  {
    title: "Hemoglobin",
    name: 'hemoglobin',
    tableItem: {}
  },
  {
    title: "Address",
    name: 'address',
    tableItem: {}
  },
  {
    title: "Status",
    name: 'status',
    tableItem: {
      render(text) {
        return <Button type="primary" size="small" danger={text !== 1}>
          {text === 1 ? <i className="las la-check-circle" /> : <i className="las la-times-circle" />}
        </Button>
      }
    }
  },
]

export const createFormRoleDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        ({ getFieldValue }) => ({
          validator: async (rule, value) => {
            if (!!value && value.trim() !== "") {
              const data = await check_slug({
                id: getFieldValue("id"),
                name: value,
                model: "roles"
              });
              if (data) return Promise.resolve()
              else return Promise.reject(intl.formatMessage(messages.thisNameIsAlreadyInUse));
            } else return Promise.resolve()
          },
        }),
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.description),
    name: 'description',
    formItem: {
      type: "textarea"
    }
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]
export const createFormPermissionDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.description),
    name: 'description',
    formItem: {
      type: "textarea"
    }
  },
  {
    title: intl.formatMessage(messages.route),
    name: 'route',
    formItem: {
      rules: [
        ({ getFieldValue }) => ({
          validator: async (rule, value) => {
            if (!!value && value.trim() !== "") {
              const data = await check_slug({
                id: getFieldValue("id"),
                route: value,
                model: "permissions"
              });
              if (data) return Promise.resolve()
              else return Promise.reject(intl.formatMessage(messages.thisRouteIsAlreadyInUse));
            } else return Promise.resolve()
          },
        }),
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterRoute)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]
export const createFormSetPermissionDrag = (permissions) => [
  {
    name: 'id',
    formItem: {
      type: 'hidden'
    }
  },
  {
    name: 'role_id',
    formItem: {
      type: 'hidden'
    }
  },
  {
    title: intl.formatMessage(messages.permission),
    name: "permission_route",
    dict: permissions.map(item => ({code: item.route, codeName: item.name})),
    formItem: {
      type: "select",
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseSelectPermission)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.can),
    name: "can",
    dict: [
      {code: "POST", codeName: intl.formatMessage(messages.add)},
      {code: "GET", codeName: intl.formatMessage(messages.view)},
      {code: "PUT", codeName: intl.formatMessage(messages.modify)},
      {code: "DELETE", codeName: intl.formatMessage(messages.delete)},
    ],
    formItem: {
      type: "select",
      mode: "multiple",
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseSelectCan)
        }
      ]
    }
  },
]
export const createFormHospitalDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.address),
    name: 'address',
    formItem: {
      type: 'textarea',
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterAddress)
        }
      ]
    },
  },
]
export const createFormOrganizationDrag = () => [
  {
    title: intl.formatMessage(messages.name),
    name: 'name',
    formItem: {
      rules: [
        {
          required: true,
          message: intl.formatMessage(messages.pleaseEnterName)
        }
      ]
    }
  },
  {
    title: intl.formatMessage(messages.description),
    name: 'description',
    formItem: {
      type: 'textarea',
    },
  },
  {
    title: intl.formatMessage(messages.logo),
    name: 'logo',
    formItem: {
      type: 'media',
    },
  },
  {
    title: intl.formatMessage(messages.url),
    name: 'url',
    formItem: {
    },
  },
  {
    title: intl.formatMessage(messages.status),
    name: 'status',
    formItem: {
      type: "switch",
      initialValue: true,
    }
  },
]
