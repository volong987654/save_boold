import React from 'react';
import { connect } from 'dva';
import { Layout } from 'antd';
import intl from "react-intl-universal";

import BaseComponent from 'components/BaseComponent';
import Toolbar from 'components/Toolbar';
import DataTable from 'components/DataTable';
import Button from 'components/Button';
import { ModalForm, ModalDragList, ModalArrayForm, ModalTable } from 'components/Modal';
import routerLinks from "@/utils/routerLinks";
import PageHelper from '@/utils/pageHelper';

import messages from '../messages';
import { createColumns, createFormRoleDrag, createFormSetPermissionDrag, createFormBloodDrag, createColumnsModalTable, createFormHospitalDrag, createFormOrganizationDrag } from './columns';
import './index.less';

const { Content, Header, Footer } = Layout;
const { Pagination } = DataTable;

@connect(({ user, global, loading }) => ({
  global,
  user,
  loading: loading.models.user
}))
class User extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      record: null,
      visible: false,
      visibleDragRole: false,
      visibleArray: false,
      visibleDragBlood: false,
      visibleDragHospital: false,
      visibleDragOrganization: false,
      dataItems: PageHelper.create(),
      visibleTable: false,
      id: null,
      note: "",
      array: [],
      rows: []
    }
  }

  handleDelete = records => {
    const { rows } = this.state;
    const { dispatch } = this.props;

    dispatch({
      type: 'user/remove',
      payload: {
        records,
        success: () => {
          this.setState({ rows: rows.filter(item => !records.some(jItem => jItem.rowKey === item.rowKey)) });
        }
      }
    });
  };

  handlePatch = (records) => {
    const { dispatch } = this.props;
    dispatch({ type: 'user/patch', payload: { records }});
  }

  render() {
    const { user, loading, dispatch, history, location } = this.props;
    const { pageData, roles, permissions, bloods, hospitals, organizations, } = user;
    const columns = this.setSortFilter(pageData, createColumns(this, roles.map(item => ({ code: item.id, codeName: item.name })), bloods, hospitals, organizations.map(item => ({ code: item.id, codeName: item.name }))));
    const { rows, record, visible, visibleDragRole, visibleArray, array, id, visibleDragBlood, dataItems, visibleTable, visibleDragHospital, visibleDragOrganization } = this.state;

    const dataTableProps = {
      loading,
      columns,
      dataItems: pageData,
      selectedRowKeys: rows.map(item => item.id),
      onChange: (page) => history.replace(location.pathname +pageData.toParams(page)),
      onSelect: (keys, rows) => this.setState({ rows })
    };
    const modalFormProps = {
      loading,
      record,
      visible,
      columns,
      modalOpts: {
        width: 1200
      },
      formOpts: {
        cols: { xs: 24, md: 12, xl: 12 }
      },
      onCancel: () => this.setState({ record: null, visible: false }),
      onSubmit: values => {
        dispatch({
          type: 'user/save',
          payload: {
            values: !record ? values : {...values, id: record.id},
            success: () => this.setState({ record: null, visible: false})
          }
        });
      }
    };
    const modalArrayFormProps = {
      loading,
      array: array,
      visible: visibleArray,
      title: intl.formatMessage(messages.setPermission),
      columns: createFormSetPermissionDrag(permissions),
      modalOpts: {
        width: 1200
      },
      formOpts: {
        cols: {
          xs: 24,
          md: 12,
          xl: 12
        },
      },
      onAdd: () => {
        array.push({ id: -1, role_id: id, permission_id: undefined, can: undefined })
        this.setState({ array })
      },
      onRemove: (index) => {
        if (array[index].id > 0) dispatch({ type: 'user/removeRolePermission', payload: array[index].id });
        array.splice(index, 1);
        this.setState({ array })
      },
      onCancel: () => this.setState({ visibleArray: false, id: null, array: []}),
      onSubmit: async data => {
        dispatch({ type: 'user/changeRolePermission', payload: {
          data,
          permissions: {
            "post": this.checkPermission(routerLinks("RolePermission", "api"), "POST"),
            "put" : this.checkPermission(routerLinks("RolePermission", "api"), "PUT")
          }
        }});
        this.setState({ visibleArray: false, id: null, array: []});
      },
      checkPermission: (method) => this.checkPermission(routerLinks("RolePermission", "api"), method),
    };
    const modalTableProps = {
      loading,
      visible: visibleTable,
      tableProps: {
        showNum: false,
        pagination: false,
        dataItems,
        selectType: null,
      },
      width: 1300,
      columns: createColumnsModalTable(this),
      onCancel: () => {
        this.setState({ visibleTable: false });
      },
    }

    const dragProps = (columns, data, key, visible, visibleKey, title, maxDepth, name, url, readOnly = false, callArray, checkCallArray) => {
      return {
        loading, columns, data, visible, title, maxDepth,
        callArray, checkCallArray, readOnly,
        onSubmit: (data, value) => {
          if (value) dispatch({
            type: `user/change${name}`,
            payload: {
              value, data,
              permissions: {
                "post": this.checkPermission(url, "POST"),
                "put" : this.checkPermission(url, "PUT")
              }
            }
          });
          else dispatch({ type: "user/@change", payload: { [key]: data }})
        },
        onDrag: (data) => dispatch({ type: "user/@change", payload: { [key]: data }}),
        onCancel: () => this.setState({ [visibleKey]: false }),
        onSave: () => {
          dispatch({
            type: `user/change${name}`,
            payload: {
              value: null, data,
              permissions: {
                "post": this.checkPermission(url, "POST"),
                "put" : this.checkPermission(url, "PUT")
              }
            }
          })
          this.setState({ [visibleKey]: false });
        },
        onDelete: (id) => {
          if (id > 0) dispatch({ type: `user/remove${name}`, payload: id })
          else dispatch({ type: "user/@change", payload: this.deleteItem(data, id) })
        },
        checkPermission: (method) => this.checkPermission(url, method),
      };
    }
    const modalDragRoleProps = dragProps(
      createFormRoleDrag(), roles, "roles", visibleDragRole, "visibleDragRole",
      intl.formatMessage(messages.role), 1,"Role", routerLinks("Role", "api"), true,
      this.checkPermission(routerLinks("RolePermission", "api")) ? (id, array) => this.setState({id, array: array ? array : [], visibleArray: true}) : undefined,
      (item) => item["is_admin"] !== 1,
    );

    const modalDragBloodProps = dragProps(
      createFormBloodDrag(), bloods, "bloods", visibleDragBlood, "visibleDragBlood",
      intl.formatMessage(messages.bloodType), 2,"Blood", routerLinks("Blood", "api"),
    );
    const modalDrapHospitalProps = dragProps(
      createFormHospitalDrag(), hospitals, "hospitals", visibleDragHospital, "visibleDragHospital",
      intl.formatMessage(messages.hospital), 3,"Hospital", routerLinks("Hospital", "api"));
    const modalDrapOrganizationProps = dragProps(
      createFormOrganizationDrag(), organizations, "organizations", visibleDragOrganization, "visibleDragOrganization",
      intl.formatMessage(messages.organization), 1,"Organization", routerLinks("Organization", "api"));

    return (
      <Layout className="full-layout user-page">
        <Header>
          <Toolbar
            appendLeft={
              <Button.Group>
                {this.checkPermission(routerLinks("User", "api"), "POST") && (
                  <Button type="primary" icon={<i className="las la-plus" />} onClick={this.onAdd}>
                    {intl.formatMessage(messages.add)}
                  </Button>
                )}
                {this.checkPermission(routerLinks("User", "api"), "DELETE") && (
                  <Button
                    type="primary"
                    disabled={!rows.length}
                    onClick={e => this.onDelete(rows)}
                    icon={<i className="las la-trash-alt" />}
                  >
                    {intl.formatMessage(messages.delete)}
                  </Button>
                )}
              </Button.Group>
            }
          >
            <Button.Group>
              {this.checkPermission(routerLinks("Role", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-briefcase" />} onClick={() => this.setState({ visibleDragRole: true })}
                >
                  {intl.formatMessage(messages.role)}
                </Button>
              )}
              {this.checkPermission(routerLinks("Blood", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-tint" />} onClick={() => this.setState({ visibleDragBlood: true })}
                >
                  {intl.formatMessage(messages.bloodType)}
                </Button>
              )}
              {this.checkPermission(routerLinks("Hospital", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-hospital" />} onClick={() => this.setState({ visibleDragHospital: true })}
                >
                  {intl.formatMessage(messages.hospital)}
                </Button>
              )}
              {this.checkPermission(routerLinks("Organization", "api")) && (
                <Button
                  type="primary"
                  icon={<i className="las la-briefcase" />} onClick={() => this.setState({ visibleDragOrganization: true })}
                >
                  {intl.formatMessage(messages.organization)}
                </Button>
              )}
            </Button.Group>
          </Toolbar>
        </Header>
        <Content className="has-footer">
          <DataTable {...dataTableProps} />
        </Content>
        <Footer>
          <Pagination {...dataTableProps} />
        </Footer>
        <ModalForm {...modalFormProps} />
        {this.checkPermission(routerLinks("RolePermission", "api")) && <ModalArrayForm {...modalArrayFormProps} /> }
        {this.checkPermission(routerLinks("Role", "api")) && <ModalDragList {...modalDragRoleProps} /> }
        {this.checkPermission(routerLinks("Hospital", "api")) && <ModalDragList {...modalDrapHospitalProps} />}
        {this.checkPermission(routerLinks("Organization", "api")) && <ModalDragList {...modalDrapOrganizationProps} />}
        <ModalTable {...modalTableProps} />
        <ModalDragList {...modalDragBloodProps} />
      </Layout>
    );
  }
}

export default User;
