import modelEnhance from '@/utils/modelEnhance';
import PageHelper from '@/utils/pageHelper';
import routerLinks from "@/utils/routerLinks";

import {
  getList_USER,
  save_USER,
  update_USER,
  patch_USER,
  delete_USER,
  get_ROLE,
  change_ROLE,
  delete_ROLE,
  get_PERMISSION,
  change_PERMISSION,
  delete_PERMISSION,
  change_ROLE_PERMISSION,
  delete_ROLE_PERMISSION,
  get_BLOOD,
  change_BLOOD,
  delete_BLOOD,
  get_ORGANIZATION,
  change_ORGANIZATION,
  delete_ORGANIZATION,
  get_HOSPITAL,
  change_HOSPITAL,
  delete_HOSPITAL,
} from "../service"

/**
* true when the page is first loaded
* Can use this value to prevent switching pages when
* Initialize data multiple times
*/
let url = true;
export default modelEnhance({
  namespace: 'user',

  state: {
    pageData: PageHelper.create(),
    roles: [],
    permissions: [],
    bloods: [],
    hospitals: [],
    organizations: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname, search }) => {
        if (pathname === routerLinks("User") && !!search && url) {
          url = false
          setTimeout(() => url = true, 500);
          const payload = PageHelper.URLToArray(search);
          dispatch({ type: 'init', payload });
        }
      });
    }
  },

  effects: {
    // Enter page load
    *init({ payload }, { put, select }) {
      const { pageData } = yield select(state => state.user);
      yield put({
        type: 'getPageInfo',
        payload: {
          pageData: pageData.setPage(payload)
        }
      });
      yield put({type: 'getAllAttributes'});
    },
    *getAllAttributes({ payload }, { put, select }) {
      const { links } = yield select(state => state.global);
      if (typeof links === "boolean" || typeof links[routerLinks("Role", "api")] !== "undefined") yield put({ type: 'getRole' });
      if (typeof links === "boolean" || typeof links[routerLinks("Permission", "api")] !== "undefined") yield put({ type: 'getPermission' });
      if (typeof links === "boolean" || typeof links[routerLinks("Blood", "api")] !== "undefined") yield put({ type: 'getBlood' });
      if (typeof links === "boolean" || typeof links[routerLinks("Hospital", "api")] !== "undefined") yield put({ type: 'getHospital' });
      if (typeof links === "boolean" || typeof links[routerLinks("Organization", "api")] !== "undefined") yield put({ type: 'getOrganization' });
    },
    // Get paging data
    *getPageInfo({ payload }, { call }) {
      const { pageData } = payload;
      const data = yield call (getList_USER, pageData)
      pageData.setPage(data);
    },
    // Save after querying paging
    *save({ payload }, { put, select, call }) {
      const { values, success } = payload;
      const { pageData } = yield select(state => state.user);

      if (values.id) yield call(update_USER, values);
      else yield call(save_USER, values);

      yield put({ type: 'getPageInfo', payload: { pageData } });
      success();
    },
    // Delete after querying paging
    *remove({ payload }, { put, select, call }) {
      const { records, success } = payload;
      const { pageData } = yield select(state => state.user);

      const data = records.map(item => item.id)
      yield call(delete_USER, data);

      yield put({ type: 'getPageInfo', payload: { pageData } });
      yield put({type: 'getAllAttributes'});
      success();
    },
    *patch({ payload }, { select, call, put }) {
      const { records } = payload;
      const { pageData } = yield select(state => state.user);
      yield call(patch_USER, records);
      yield put({ type: 'getPageInfo', payload: { pageData } });
    },

    *getRole({ payload }, { call, put }) {
      const data = yield call(get_ROLE)
      yield put({ type: 'success', payload: data });
    },
    *changeRole({ payload }, { call, put }) {
      const roles = yield call(change_ROLE, payload)
      yield put({ type: 'success', payload: { roles } });
    },
    *removeRole({ payload }, { call, put }) {
      yield call(delete_ROLE, payload);
      yield put({ type: 'getRole' });
    },

    *getPermission({ payload }, { call, put }) {
      const data = yield call(get_PERMISSION)
      yield put({ type: 'success', payload: data });
    },
    *changePermission({ payload }, { call, put }) {
      const permissions = yield call(change_PERMISSION, payload)
      yield put({ type: 'success', payload: { permissions } });
    },
    *removePermission({ payload }, { call, put }) {
      yield call(delete_PERMISSION, payload);
      yield put({ type: 'getPermission' });
    },

    *changeRolePermission({ payload }, { call }) {
      yield call(change_ROLE_PERMISSION, payload)
    },
    *removeRolePermission({ payload }, { select, call, put }) {
      yield call(delete_ROLE_PERMISSION, payload);
      yield put({type: 'getAllAttributes'});
    },

    *getBlood({ payload }, { call, put }) {
      const data = yield call(get_BLOOD)
      yield put({ type: 'success', payload: data });
    },
    *changeBlood({ payload }, { call, put }) {
      const permissions = yield call(change_BLOOD, payload)
      yield put({ type: 'success', payload: { permissions } });
    },
    *removeBlood({ payload }, { call, put }) {
      yield call(delete_BLOOD, payload);
      yield put({ type: 'getBlood' });
    },
    *getOrganization({ payload }, { call, put }) {
      const data = yield call(get_ORGANIZATION)
      yield put({ type: 'success', payload: data });
    },
    *changeOrganization({ payload }, { call, put }) {
      const organizations = yield call(change_ORGANIZATION, payload)
      yield put({ type: 'success', payload: { organizations } });
    },
    *removeOrganization({ payload }, { call, put }) {
      yield call(delete_ORGANIZATION, payload);
      yield put({ type: 'getOrganization' });
    },

    *getHospital({ payload }, { call, put }) {
      const data = yield call(get_HOSPITAL)
      yield put({ type: 'success', payload: data });
    },
    *changeHospital({ payload }, { call, put }) {
      const hospitals = yield call(change_HOSPITAL, payload)
      yield put({ type: 'success', payload: { hospitals } });
    },
    *removeHospital({ payload }, { call, put }) {
      yield call(delete_HOSPITAL, payload);
      yield put({ type: 'getHospital' });
    },
  },

  reducers: {
    success(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  }
});
