export const scope = 'routes.User';

export default {
  avatar: {
    id: `${scope}.avatar`,
    defaultMessage: 'Avatar',
  },
  noImage: {
    id: `${scope}.noImage`,
    defaultMessage: 'No Image',
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  pleaseEnterName: {
    id: `${scope}.pleaseEnterName`,
    defaultMessage: 'Please enter name',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'Email',
  },
  thisEmailAddressIsAlreadyTaken: {
    id: `${scope}.thisEmailAddressIsAlreadyTaken`,
    defaultMessage: 'This email address is already taken',
  },
  pleaseEnterEmail: {
    id: `${scope}.pleaseEnterEmail`,
    defaultMessage: 'Please enter email',
  },
  pleaseEnterTheCorrectEmailFormat: {
    id: `${scope}.pleaseEnterTheCorrectEmailFormat`,
    defaultMessage: 'Please enter the correct email format',
  },
  password: {
    id: `${scope}.password`,
    defaultMessage: 'Password',
  },
  role: {
    id: `${scope}.role`,
    defaultMessage: 'Role',
  },
  pleaseSelectRole: {
    id: `${scope}.pleaseSelectRole`,
    defaultMessage: 'Please select role',
  },
  bio: {
    id: `${scope}.bio`,
    defaultMessage: 'Bio',
  },
  lastTime: {
    id: `${scope}.lastTime`,
    defaultMessage: 'Last Time',
  },
  createdAt: {
    id: `${scope}.createdAt`,
    defaultMessage: 'Created at',
  },
  updatedAt: {
    id: `${scope}.updatedAt`,
    defaultMessage: 'Updated at',
  },
  operating: {
    id: `${scope}.operating`,
    defaultMessage: 'Operating',
  },
  modify: {
    id: `${scope}.modify`,
    defaultMessage: 'Modify',
  },
  status: {
    id: `${scope}.status`,
    defaultMessage: 'Status',
  },
  areYouSure: {
    id: `${scope}.areYouSure`,
    defaultMessage: 'Are you sure?',
  },
  delete: {
    id: `${scope}.delete`,
    defaultMessage: 'Delete',
  },
  thisNameIsAlreadyInUse: {
    id: `${scope}.thisNameIsAlreadyInUse`,
    defaultMessage: 'This name is already in use',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Description',
  },
  isAdmin: {
    id: `${scope}.isAdmin`,
    defaultMessage: 'Is Admin',
  },
  route: {
    id: `${scope}.route`,
    defaultMessage: 'Route',
  },
  thisRouteIsAlreadyInUse: {
    id: `${scope}.thisRouteIsAlreadyInUse`,
    defaultMessage: 'This route is already in use',
  },
  pleaseEnterRoute: {
    id: `${scope}.pleaseEnterRoute`,
    defaultMessage: 'Please enter route',
  },
  permission: {
    id: `${scope}.permission`,
    defaultMessage: 'Permission',
  },
  pleaseSelectPermission: {
    id: `${scope}.pleaseSelectPermission`,
    defaultMessage: 'Please select permission',
  },
  can: {
    id: `${scope}.can`,
    defaultMessage: 'Can',
  },
  add: {
    id: `${scope}.add`,
    defaultMessage: 'Add',
  },
  view: {
    id: `${scope}.view`,
    defaultMessage: 'View',
  },
  pleaseSelectCan: {
    id: `${scope}.pleaseSelectCan`,
    defaultMessage: 'Please select can',
  },
  setPermission: {
    id: `${scope}.setPermission`,
    defaultMessage: "Set Permission",
  },
  bloodType: {
    id: `${scope}.bloodType`,
    defaultMessage: "Blood Type",
  },
  pleaseSelectBloodType: {
    id: `${scope}.pleaseSelectBloodType`,
    defaultMessage: "Please select blood type",
  },

  organization: {
    id: `${scope}.organization`,
    defaultMessage: "Organization",
  },
  pleaseSelectOrganization: {
    id: `${scope}.pleaseSelectOrganization`,
    defaultMessage: "Please select organization",
  },
  logo: {
    id: `${scope}.logo`,
    defaultMessage: "Logo",
  },
  url: {
    id: `${scope}.url`,
    defaultMessage: "Url",
  },
  job: {
    id: `${scope}.job`,
    defaultMessage: "Job",
  },
  address: {
    id: `${scope}.address`,
    defaultMessage: "Address",
  },
  pleaseEnterAddress: {
    id: `${scope}.pleaseEnterAddress`,
    defaultMessage: "Please enter address",
  },
  phone: {
    id: `${scope}.phone`,
    defaultMessage: "Phone",
  },
  pleaseEnterPhone: {
    id: `${scope}.pleaseEnterPhone`,
    defaultMessage: "Please enter phone",
  },
  gender: {
    id: `${scope}.gender`,
    defaultMessage: "Gender",
  },
  male: {
    id: `${scope}.male`,
    defaultMessage: "Male",
  },
  female: {
    id: `${scope}.female`,
    defaultMessage: "Female",
  },
  pleaseSelectGender: {
    id: `${scope}.pleaseSelectGender`,
    defaultMessage: "Please select gender",
  },
  identityCard: {
    id: `${scope}.identityCard`,
    defaultMessage: "Identity Card",
  },
  pleaseEnterIdentityCard: {
    id: `${scope}.pleaseEnterIdentityCard`,
    defaultMessage: "Please enter identity card",
  },
  birthday: {
    id: `${scope}.birthday`,
    defaultMessage: "Birthday",
  },
  history: {
    id: `${scope}.history`,
    defaultMessage: "History",
  },
  hospital: {
    id: `${scope}.hospital`,
    defaultMessage: 'Hospital',
  },
  note: {
    id: `${scope}.note`,
    defaultMessage: 'Note',
  },
};
