
export default {
  admin: {
    id: "models.Router.admin",
    defaultMessage: 'Administrator page',
  },
  "403": {
    id: "models.Router.403",
    defaultMessage: 'Forbidden',
  },
  "404": {
    id: "models.Router.404",
    defaultMessage: 'Not Found',
  },
  "500": {
    id: "models.Router.500",
    defaultMessage: 'Internal Server Error',
  },
  login: {
    id: "routes.Login.title",
    defaultMessage: 'Login',
  },
  register: {
    id: "routes.Register.title",
    defaultMessage: 'Register',
  },
  user: {
    id: "routes.User.title",
    defaultMessage: 'User Management',
  },
  profile: {
    id: "routes.Profile.title",
    defaultMessage: 'Profile Page',
  },
  website: {
    id: "routes.Website.title",
    defaultMessage: 'Website Page',
  },
  find: {
    id: "routes.Find.title",
    defaultMessage: 'Find Management',
  },
  news: {
    id: "routes.News.title",
    defaultMessage: 'News Management',
  },
}// 💬 generate link to here
