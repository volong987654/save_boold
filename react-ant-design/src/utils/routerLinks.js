export default (name, type, sorts = "{\"updated_at\":\"descend\"}") => {
  const types = {
    curd: `?pageNum=1&pageSize=10&filters={}&sorts=${sorts}`
  }
  const array = {
    Register: '/auth/register',
    Login: '/auth/login',
    Pages403: '/403',
    Pages404: '/404',
    Pages500: '/500',
    BusinessCRUD: '/crud',
    Home: '/home',
    User: '/user',
    Profile: '/profile',
    Website: '/website',
    Find: '/find',
    News: '/news'
  }// 💬 generate link to here

  const apis = {
    CheckSlug: '/check-slug',
    Permission: '/permission',
    Blood: '/blood',
    RolePermission: '/role-permission',
    User: '/user',
    Role: '/role',
    Profile: '/auth',
    Website: '/setting',
    Find: '/find',
    Hospital: '/hospital',
    News: '/news',
    Organization: '/organization',
    Category: '/category',
    Slide: '/slide',
  }// 💬 generate api to here

  switch(type) {
    case "curd":
      return array[name] + types[type];
    case "api":
      return apis[name];
    default:
      return array[name];
  }
};
