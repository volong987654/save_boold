import React, {Component} from 'react';
import { Row, Col, Badge } from 'antd';
import PropTypes from "prop-types";

import MediaManagement from 'components/MediaManagement';

class MediaControlled extends Component {
  constructor(props) {
    super(props);
    let values = []
    if (props.value) {
      values = props.limit === 1 ? [props.value] : JSON.parse(props.value);
    }
    this.state = {
      visible: false,
      url: values,
      index: -1,
    };
  }
  onSelect = (newUrl, type) => {
    const { onChange, limit } = this.props;
    const { url, index } = this.state;
    if (index > -1) {
      if (newUrl) {
        url[index] = newUrl;
      } else {
        url.splice(index, 1);
      }
    } else if (newUrl) {
      url.push(newUrl);
    }
    if (limit === 1) {
      onChange(newUrl);
    } else {
      onChange(JSON.stringify(url))
    }
    this.setState({ visible: false, url })
  };

  renderIcon = (item) => {
    const array = item.split(".");
    switch (array[array.length - 1]) {
      case "pdf":
        return <><i className="las la-file-pdf la-3x" /> <br/> <span className="word-break">{item}</span></>
      default:
        return <img src={item} alt="avatar" style={{ width: '100%' }} />
    }
  }

  render() {
    const { limit, onChange } = this.props;
    const { visible, url } = this.state;
    const propsMedia = {
      visible,
      onHide: () => this.setState({ visible: false }),
      onSelect: this.onSelect,
      limit: limit - url.length,
    }
    return (
      <>
        <Row gutter={[10, 10]}>
        {url.map((item, index) => (
          <Col key={index}>
            <Badge count={<i className="las la-times-circle" onClick={() => {
              url.splice(index, 1);
              onChange(JSON.stringify(url))
            }} />}>
              <div className="media-item" onClick={() => this.setState({ visible: true, index })}>
                {this.renderIcon(item)}
              </div>
            </Badge>
          </Col>
        ))}
        { url.length < limit && (
          <Col>
            <div className="media-item" onClick={() => this.setState({ visible: true, index: -1 })}>
              <i className="las la-plus la-3x" />
            </div>
          </Col>
        )}
        </Row>
        <MediaManagement { ...propsMedia }/>
      </>
    )
  }
}
MediaControlled.propTypes = {
  value: PropTypes.string,
  limit: PropTypes.number,
  onChange: PropTypes.func,
};

MediaControlled.defaultProps = {
  limit: 1,
};

export default ({
  ...otherProps
}) => <MediaControlled {...otherProps} />;
