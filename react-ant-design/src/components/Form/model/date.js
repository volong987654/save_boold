import React from 'react';
import { DatePicker, TimePicker } from 'antd';

const { MonthPicker, RangePicker } = DatePicker;
const { RangePicker: RangePickerTime } = TimePicker;

/**
 * Date, time component
 */
export default ({
  name,
  form,
  type,
  record,
  defaultValue,
  formFieldOptions = {},
  format,
  onChange,
  normalize,
  preview,
  getPopupContainer,
  ...otherProps
}) => {

  const props = {
    ...otherProps
  };

  let Component = DatePicker;

  switch (type) {
    case 'date':
      break;
    case 'datetime':
      break;
    case 'date~':
      Component = RangePicker;
      break;
    case 'month':
      Component = MonthPicker;
      break;
    case 'time':
      Component = TimePicker;
      break;
    case 'time~':
      Component = RangePickerTime
      break;
    default:
      break;
  }

  if (format) props.format = format;
  else if (type === 'month') props.format = 'YYYY-MM';
  else if (type === 'datetime' || type === 'date~')
    props.format = 'YYYY-MM-DD HH:mm:ss';
  else if (type === 'time' || type === 'time~') props.format = 'HH:mm:ss';
  else props.format = 'YYYY-MM-DD';


  return <Component {...props} />;
};
