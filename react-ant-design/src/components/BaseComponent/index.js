import React from 'react';
import { Modal } from 'antd';
import $$ from 'cmn-utils';
import config from '@/config';

class BaseComponent extends React.Component {
  notice = config.notice; // notification

  /**
   * History api route jump
   */
  get history() {
    return this.props.history;
  }

  /**
   * New
   */
  onAdd = () => {
    this.setState({
      record: null,
      visible: true
    });
  };

  /**
   * Modify
   * @param {object} Form record
   */
  onUpdate = record => {
    this.setState({
      record,
      visible: true
    });
  };

  /**
   * Delete
   * @param {object | array} Form records, an array when deleting in bulk
   */
  onDelete = record => {
    if (!record) return;
    if ($$.isArray(record) && !record.length) return;

    const content = `Do you want to delete this ${
      $$.isArray(record) ? record.length : ''
    } item?`;

    Modal.confirm({
      title: 'Note',
      content,
      onOk: () => {
        this.handleDelete($$.isArray(record) ? record : [record]);
      },
      onCancel() {}
    });
  };

  handleAdd() {
    /* Subclass rewriting */
  }
  handleUpdate() {
    /* Subclass rewriting */
  }
  handleDelete(records) {
    /* Subclass rewriting */
  }

  checkPermission = (url, method = "GET") => {
    const { global } = this.props;
    const { links } = global;
    if (typeof links === "object") {
      if (method === "ALL") return typeof links[url] !== "undefined" && links[url].length > 3;
      return typeof links[url] !== "undefined" && links[url].indexOf(method) > -1;
    }
    return true
  }

  deleteItem = (array, id) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) {
        array.splice(i, 1);
        i--;
      } else if (array[i].children){
        array[i].children = this.deleteItem(array[i].children, id);
      }
    }
    return array;
  }

  setSortFilter = (pageData, columns) => {
    if (Object.keys(pageData.sorts).length || Object.keys(pageData.filters).length) {
      columns.map(item => {
        if (pageData.sorts[item.name] || pageData.filters[item.name]) {
          item.tableItem = {
            ...item.tableItem,
            sortOrder: pageData.sorts[item.name],
            filteredValue: pageData.filters[item.name]
          }
        }
        return item;
      })
    }
    return columns;
  }

}

export default BaseComponent;
