import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Modal, Row, Col, Popconfirm} from 'antd';
import cx from 'classnames';
import intl from 'react-intl-universal';
import omit from 'object.omit';

import Form from 'components/Form';
import Button from 'components/Button';

import messages from './messages';
import './style/index.less';

class ModalArrayForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.visible !== prevState.visible) {
      return {
        visible: nextProps.visible
      };
    }
    return null;
  }

  closeModal = () => {
    if (this.props.onCancel) {
      this.props.onCancel();
      return;
    }
    this.setState({
      visible: false
    });
  };

  onSubmit = async () => {
    const { onSubmit } = this.props;
    const array = [];
    let check = true;
    for (let key in this.refs) {
      try {
        const value = await this.refs[key].refs.form.validateFields()
        array.push(value)
      } catch (errorInfo) {
        check = false;
        console.log('Failed:', errorInfo);
      }
    }
    if (check) onSubmit && onSubmit(array);
  };

  render() {
    const {
      title,
      array,
      className,
      columns,
      onCancel,
      onSubmit,
      modalOpts,
      formOpts,
      loading,
      full,
      onAdd,
      onRemove,
      checkPermission,
    } = this.props;

    const classname = cx(className, 'antui-modal-array-form', { 'full-modal': full });
    const modalProps = {
      className: classname,
      visible: this.state.visible,
      width: 700,
      style: { top: 20 },
      title: title,
      maskClosable: false,
      destroyOnClose: true,
      onCancel: this.closeModal,
      footer: [
        onCancel && (
          <Button key="back" onClick={this.closeModal}>
            {intl.formatMessage(messages.cancel)}
          </Button>
        ),
        (checkPermission("POST") || checkPermission("PUT")) && onSubmit ? (
          <Button key="submit" type="primary" onClick={this.onSubmit} loading={loading}>
            {intl.formatMessage(messages.save)}
          </Button>
        ) : null
      ],
      ...modalOpts
    };
    const _columns = columns.map(item => omit(item, [
      'tableItem',
      'searchItem',
    ]))
    const formProps = {
      columns: _columns,
      onSubmit,
      footer: false,
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      ...formOpts
    };
    return (
      <Modal {...modalProps}>
        {checkPermission("POST") && (
          <Button tooltip={intl.formatMessage(messages.new)}  type="primary" onClick={onAdd}>
            <i className="las la-plus" />
          </Button>
        )}
        {array.map((item, index) => (
          <Row align="middle" gutter={16} key={index}>
            <Col flex="1">
              <Form ref={"form" + index} {...formProps} record={item} />
            </Col>
            {checkPermission("DELETE") && (
              <Col>
                <Popconfirm
                  title={intl.formatMessage(messages.areYouSure)}
                  icon={<i className="las la-question-circle" />}
                  onConfirm={() => onRemove(index)}
                >
                  <Button tooltip={intl.formatMessage(messages.delete)} type="primary" danger>
                    <i className="las la-minus" />
                  </Button>
                </Popconfirm>
              </Col>
            )}
          </Row>
        ))}
      </Modal>
    );
  }
}

ModalArrayForm.propTypes = {
  title: PropTypes.string,
  array: PropTypes.array,
  visible: PropTypes.bool,
  loading: PropTypes.bool,
  full: PropTypes.bool,
  preview: PropTypes.bool,
  columns: PropTypes.array,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  checkPermission: PropTypes.func,
  modalOpts: PropTypes.object,
  formOpts: PropTypes.object,
  className: PropTypes.string
};

export default ModalArrayForm;
