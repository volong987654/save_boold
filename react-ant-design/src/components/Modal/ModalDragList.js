import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Col, Drawer, Modal, Popconfirm, Row, Spin} from 'antd';
import cx from 'classnames';
import Nestable from 'react-nestable';
import intl from "react-intl-universal";

import Form from 'components/Form';
import Button from 'components/Button';

import messages from "./messages";

import './style/index.less';
class ModalDragList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleEditMenu: false,
      record: null,
    }
  }
  order = 0;

  onEdit = (record) => {
    this.setState({ visibleEditMenu: true, record })
  }

  changeFeatured = (item, id) => {
    const { data, onSubmit } = this.props;
    onSubmit(this.findItemById(item, id, data), null);
  }

  findItemById = (newItem, id, array) => {
    return array.map((item) => {
      if (item.id === id) {
        return newItem
      } else if (item.children){
        item.children = this.findItemById(newItem, id, item.children)
      }
      return item;
    })
  }

  onSubmit = (value) => {
    const { onSubmit, data } = this.props;
    const { record } = this.state;
    let temp;

    value = {...record, ...value};

    if (!!record) {
      temp = this.findItemById(value, record.id, data)
    } else {
      value.id = -data.length
      value.order = data.length + 1
      data.push(value)
      temp = [...data];
    }
    onSubmit(temp, value);
    this.setState({ visibleEditMenu: false, record: null })
  }

  onDragEnd = (items, item) => {
    let { onDrag } = this.props
    this.order = 0;
    items = this.loop(items, null, item);
    return onDrag(items)
  }

  loop (array, parentId, item) {
    for (let i = 0; i < array.length; i++) {
      this.order++;
      array[i]['order'] = this.order;
      array[i]['parent_id'] = parentId;
      if (array[i].children.length > 0) {
        array[i].children = this.loop(array[i].children, array[i].id, item);
      }
    }
    return array;
  }

  render() {
    const {
      className, title, onCancel, onSave, onDelete, checkPermission, loading, modalOpts, visible, data, formOpts,
      columns, maxDepth, callArray, checkCallArray, readOnly,
    } = this.props;
    const { visibleEditMenu, record } = this.state;
    const modalProps = {
      className: cx(className, 'antui-modaldrag', ),
      visible,
      title,
      width: 700,
      maskClosable: false,
      destroyOnClose: true,
      onCancel,
      footer: [
        onCancel && (
          <Button key="back" onClick={onCancel}>
            {intl.formatMessage(messages.cancel)}
          </Button>
        ),
        (checkPermission("POST") || checkPermission("PUT")) && onSave ? (
          <Button key="submit" type="primary" onClick={onSave} loading={loading}>
            {intl.formatMessage(messages.save)}
          </Button>
        ) : null
      ],
      ...modalOpts
    };
    const formProps = {
      columns,
      record,
      onSubmit: this.onSubmit,
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      ...formOpts
    };

    return (
      <>
        <Modal {...modalProps}>
          <Spin spinning={loading}>
            {checkPermission("POST") && (
              <Button tooltip={intl.formatMessage(messages.new)} size="small" onClick={() => this.onEdit(null)}>
                <i className="las la-plus" />
              </Button>
            )}
            <Nestable
              maxDepth={maxDepth}
              items={data}
              collapsed={true}
              onChange={this.onDragEnd}
              renderItem={({ item, collapseIcon }) => (
                <Row className="item-list" align="middle" justify="space-between">
                  <Col>{collapseIcon}{!!item.title ? item.title : item.name}</Col>
                  <Col>
                    <Button.Group>
                      {!readOnly && checkPermission("PUT") ? (
                        <Button
                          tooltip={intl.formatMessage(messages.status)}
                          size="small"
                          type={item.status === 0 ? "primary" : ""}
                          danger={item.status === 0}
                          onClick={() => this.changeFeatured({...item, status:  item.status === 1 ? 0 : 1}, item.id)}
                        >
                          <i className={`las ${ item.status === 0 ? "la-low-vision" : "la-eye"}`} />
                        </Button>
                      ) : null}
                      {!readOnly && typeof item.featured  !== 'undefined' && checkPermission("PUT") ? (
                        <Button
                          tooltip={intl.formatMessage(messages.featured)}
                          size="small"
                          type={item.featured === 1 ? "primary" : ""}
                          onClick={() => this.changeFeatured({...item, featured:  item.featured === 1 ? 0 : 1}, item.id)}
                        >
                          <i className="las la-award" />
                        </Button>
                      ) : null}
                      {typeof callArray !== "undefined" && checkCallArray(item) ? (
                        <Button
                          tooltip={intl.formatMessage(messages.setAttributes)}
                          size="small"
                          onClick={() => callArray(item.id, item.attributes)}
                        >
                          <i className="las la-sitemap" />
                        </Button>
                      ) : null}
                      {!readOnly && checkPermission("PUT") ? (
                        <Button tooltip={intl.formatMessage(messages.edit)} size="small" onClick={() => this.onEdit(item)}>
                          <i className="las la-edit" />
                        </Button>
                      ) : null}
                      {!readOnly && checkPermission("DELETE") ? (
                        <Popconfirm
                          title={intl.formatMessage(messages.areYouSure)}
                          icon={<i className="las la-question-circle" />}
                          onConfirm={() => onDelete(item.id)}
                        >
                          <Button tooltip={intl.formatMessage(messages.delete)} size="small" type="primary" danger>
                            <i className="las la-trash-alt" />
                          </Button>
                        </Popconfirm>
                      ) : null}
                    </Button.Group>
                  </Col>
                </Row>
              )}
            />
          </Spin>
        </Modal>
        <Drawer
          destroyOnClose={true}
          title={record ? intl.formatMessage(messages.edit) :  intl.formatMessage(messages.new)}
          placement="right"
          closable={false}
          onClose={() => this.setState({ visibleEditMenu: false })}
          visible={visibleEditMenu}
        >
          <Form ref="form" {...formProps} />
        </Drawer>
      </>
    );
  }
}

ModalDragList.propTypes = {
  visible: PropTypes.bool,
  loading: PropTypes.bool,
  className: PropTypes.string,
  title: PropTypes.string,
  data: PropTypes.array,
  columns: PropTypes.array,
  onDrag: PropTypes.func,
  onCancel: PropTypes.func,
  onSave: PropTypes.func,
  onSubmit: PropTypes.func,
  onDelete: PropTypes.func,
  checkPermission: PropTypes.func,
  callArray: PropTypes.func,
  checkCallArray: PropTypes.func,
  modalOpts: PropTypes.object,
  formOpts: PropTypes.object,
  maxDepth: PropTypes.number,
}

ModalDragList.defaultProps = {
  visible: false,
  data: [],
  maxDepth: 1,
}

export default ModalDragList;
