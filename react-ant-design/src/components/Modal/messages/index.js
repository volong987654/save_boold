export const scope = 'components.Modal';

export default {
  edit: {
    id: `${scope}.edit`,
    defaultMessage: 'Edit',
  },
  new: {
    id: `${scope}.new`,
    defaultMessage: 'New',
  },
  cancel: {
    id: `${scope}.cancel`,
    defaultMessage: 'Cancel',
  },
  save: {
    id: `${scope}.save`,
    defaultMessage: 'Save',
  },
  status: {
    id: `${scope}.status`,
    defaultMessage: 'Status',
  },
  featured: {
    id: `${scope}.featured`,
    defaultMessage: 'Featured',
  },
  setAttributes: {
    id: `${scope}.setAttributes`,
    defaultMessage: 'Set Attributes',
  },
  areYouSure: {
    id: `${scope}.areYouSure`,
    defaultMessage: 'Are you sure?',
  },
  delete: {
    id: `${scope}.delete`,
    defaultMessage: 'Delete',
  },
};
