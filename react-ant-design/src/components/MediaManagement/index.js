import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb, Card, Col, Modal, Row, Drawer, Upload, Popover, Popconfirm, Spin, Tabs, Badge, Checkbox } from 'antd';
import Button from 'components/Button';

import intl from 'react-intl-universal';
import $$ from "cmn-utils";

import Form from 'components/Form';
import config from '@/config';

import { getList_MEDIA, save_MEDIA, delete_MEDIA } from "./service";
import messages from './messages';

import './style/index.less';

const { TabPane } = Tabs;

class MediaManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: $$.getStore(config.store.user),
      visibleDraw: false,
      loading: true,
      url: "/",
      data: [],
      status: 1,
      multi: false,
      images: [],
    }
    this.getList("/");
  }

  getList = async (url) => {
    const data = await getList_MEDIA(url);
    this.setState({ data, url, loading: false })
  }

  url = (index = 1) => {
    const { url } = this.state;
    this.setState({ loading: true })
    const array = url.split("/");
    if (array.length > 2) {
      return this.getList(array.slice(0, index).join("/") + '/');
    }
    return this.getList("/");
  }

  onSubmit = async (value) => {
    const { url } = this.state;
    this.setState({ loading: true })
    value.type = "folder";
    value.path = url;
    await save_MEDIA(value)
    await this.getList(url);
  }

  detail = item => (
    <>
      <div>{intl.formatMessage(messages.name)}: {item.name}</div>
      {item.type !== "folder" && (
        <>
          <div>{intl.formatMessage(messages.size)}: {item.size}</div>
          <div>{intl.formatMessage(messages.lastModified)}: {item.lastModified}</div>
        </>
      )}
    </>
  )

  handleDelete = async file => {
    const { url } = this.state;
    this.setState({ loading: true })
    try {
      await delete_MEDIA({
        path: file.origin,
        type: file.type,
      });
      await this.getList(url);
    } catch {
      this.setState({ loading: false })
    }
  }

  button = (type) => {
    switch (type) {
      case "zip":
        return <i className="las la-file-archive" />
      case "pdf":
        return <i className="las la-file-pdf" />
      case "file":
        return <i className="las la-file-alt" />
      case "video":
        return <i className="las la-film" />
      case "jpeg":
      case "png":
        return <i className="las la-image" />
      default:
        break;
    }
  }

  title = (file) => {
    const { user } = this.state;
    const { onSelect } = this.props;
    return (
      <Row justify="space-between" align="middle">
        <div>{intl.formatMessage(messages.detail)}</div>
        <Button.Group>
          <Button size="small" type="link" onClick={() => onSelect(file.url, file.type)}>
            { this.button(file.type) }
          </Button>
          {user["role_id"] === 1 && (
            <Popconfirm
              title={intl.formatMessage(messages.areYouSure)}
              icon={<i className="las la-question-circle" />}
              onConfirm={() => this.handleDelete(file)}
            >
              <Button size="small" type="link" danger>
                <i className="las la-trash-alt" />
              </Button>
            </Popconfirm>
          )}
        </Button.Group>
      </Row>
    )
  }

  render() {
    const { visible, onHide, onSelect, limit } = this.props;
    const { visibleDraw, status, data, url, loading, multi, images } = this.state;
    if (multi && limit < 2) this.setState({ multi: false })
    const props = {
      action: '/api/v1/media',
      headers: { Authorization: $$.getStore(config.store.user).token },
      listType: 'picture',
      className: "upload-block",
      multiple: true,
      data: (file) => {
        return {
          file,
          path: url
        }
      },
      onChange: (data) => {
        if (data.file.percent === 100 && data.event) {
          setTimeout(() => this.getList(url), 500)
        }
      }
    };
    const formProps = {
      loading,
      columns: [
        {
          title: intl.formatMessage(messages.nameFolder),
          name: "name",
          formItem: {
            rules: [
              { required: true, message: intl.formatMessage(messages.pleaseEnterNameFolder) },
              { validator:(_, value) => /^[a-zA-Z\d]+$/.test(value) ? Promise.resolve() : Promise.reject(intl.formatMessage(messages.onlyLettersAndDigitsAllowed)) }
            ]
          }
        }
      ],
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      onSubmit: this.onSubmit
    };
    const formOutsideProps = {
      loading,
      columns: [
        {
          title: intl.formatMessage(messages.link),
          name: "link",
          formItem: {
            rules: [
              { required: true, message: intl.formatMessage(messages.pleaseEnterNameLink) },
              { type: "url", message: intl.formatMessage(messages.incorrectPathFormat) },
            ]
          }
        }
      ],
      formItemLayout: {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 }
      },
      onSubmit: (value) => {
        onSelect(value.link, 'image')
      }
    };

    return (
      <>
        <Modal
          className="media-management"
          title={intl.formatMessage(messages.title)}
          width={700}
          visible={visible}
          footer={null}
          onCancel={onHide}
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab={intl.formatMessage(messages.myFile)} key="1">
              <Row justify="space-between" style={{paddingBottom: "10px"}}>
                <Col>
                  <Breadcrumb>
                    {url.split("/").map((item, index) => {
                      return (
                        <Breadcrumb.Item key={index} onClick={() => this.url(index + 1)}>
                          {index === 0 ? <i className="las la-home" /> : item}
                        </Breadcrumb.Item>
                      )
                    })}
                  </Breadcrumb>
                </Col>
                <Col>
                  <Button.Group>
                    {(multi && images.length && limit > 1) ? (
                      <Button tooltip={intl.formatMessage(messages.addImages)} type="primary" size="small" onClick={() => {
                        images.map((url, index) => { if (index < limit) onSelect(url, 'image'); return null;});
                        this.setState({images: []})
                      }}>
                        <i className="las la-plus-square" />
                      </Button>
                    ) : null}
                    <Button tooltip={intl.formatMessage(messages.multiImages)} danger={multi} type="primary" size="small" onClick={() => this.setState({ multi: !multi })}>
                      <i className="las la-images" />
                    </Button>
                    <Button tooltip={intl.formatMessage(messages.createFolder)} type="primary" size="small" onClick={() => this.setState({ visibleDraw: true, status: 1 })}>
                      <i className="las la-folder-plus" />
                    </Button>
                    <Button tooltip={intl.formatMessage(messages.uploadFile)} type="primary" size="small" onClick={() => this.setState({ visibleDraw: true, status: 2 })}>
                      <i className="las la-file-upload" />
                    </Button>
                  </Button.Group>
                </Col>
              </Row>
              <Spin spinning={loading}>
                <Row gutter={[16, 16]} align="middle">
                  { url.slice(1).split("/").length > 1 && (
                    <Col span={6}>
                      <Card
                        onClick={() => !multi && this.url(url.split("/").length - 2)}
                        bodyStyle={{padding: 10, textAlign: "center"}}
                        hoverable
                      >
                        <i className="las la-level-up-alt la-3x" />
                        <div>{intl.formatMessage(messages.upToFolder)}</div>
                      </Card>
                    </Col>
                  )}
                  {data.map((item, index) => (
                    <Col span={6} key={index}>
                      <Popover content={this.detail(item)} title={this.title(item)}>
                        <Badge count={(item.type !== "folder" && multi) ? <Checkbox checked={images.indexOf(item.url) > -1} /> : null }>
                          <Card
                            onClick={() => {
                              if (!multi) (item.type === "folder" ? this.getList(url + item.name + "/") : onSelect(item.url, item.type))
                              else if (item.type !== "folder"){
                                if (images.indexOf(item.url) === -1 ) images.push(item.url);
                                else images.splice(images.indexOf(item.url), 1);
                                this.setState({images: images})
                              }
                            }}
                            bodyStyle={{padding: item.type !== "folder" ? 0 : 10, textAlign: "center"}}
                            hoverable
                            cover={(item.type === "jpeg" || item.type === "png") && <img alt="example" src={item.url} />}
                          >
                            {(item.type !== "jpeg" && item.type !== "png") && (
                              <>
                                {item.type === "folder" && <i className="las la-folder-open la-3x" />}
                                {item.type === "pdf" && <i className="las la-file-pdf la-3x" />}
                                <div className="word-break">{item.name}</div>
                              </>
                            )}
                          </Card>
                        </Badge>
                      </Popover>
                    </Col>
                  ))}
                </Row>
              </Spin>
            </TabPane>
            <TabPane tab={intl.formatMessage(messages.outsideLink)} key="2">
              <Form ref="form" {...formOutsideProps} />
            </TabPane>
          </Tabs>
        </Modal>
        <Drawer
          title={status === 1 ? intl.formatMessage(messages.createFolder) : intl.formatMessage(messages.uploadFile)}
          placement="right"
          closable={false}
          onClose={() => this.setState({ visibleDraw: false })}
          visible={visibleDraw}
        >
          { status === 1 ? (
            <Form ref="form" {...formProps} />
          ) : (
            <Upload {...props}>
              <Button block>
                <i className="las la-file-upload" /> {intl.formatMessage(messages.upload)}
              </Button>
            </Upload>
          )}
        </Drawer>
      </>
    );
  }
}
MediaManagement.propTypes = {
  visible: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
}

MediaManagement.defaultProps = {
}
export default MediaManagement;
