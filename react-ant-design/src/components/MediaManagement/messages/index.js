export const scope = 'components.Media';

export default {
  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  size: {
    id: `${scope}.size`,
    defaultMessage: 'Size',
  },
  lastModified: {
    id: `${scope}.lastModified`,
    defaultMessage: 'Last Modified',
  },
  detail: {
    id: `${scope}.detail`,
    defaultMessage: 'Detail',
  },
  areYouSure: {
    id: `${scope}.areYouSure`,
    defaultMessage: 'Are you sure?',
  },
  nameFolder: {
    id: `${scope}.nameFolder`,
    defaultMessage: 'Name Folder',
  },
  pleaseEnterNameFolder: {
    id: `${scope}.pleaseEnterNameFolder`,
    defaultMessage: 'Please enter name folder',
  },
  onlyLettersAndDigitsAllowed: {
    id: `${scope}.onlyLettersAndDigitsAllowed`,
    defaultMessage: 'Only letters and digits allowed',
  },
  link: {
    id: `${scope}.link`,
    defaultMessage: 'Link',
  },
  pleaseEnterNameLink: {
    id: `${scope}.pleaseEnterNameLink`,
    defaultMessage: 'Please enter name Link',
  },
  incorrectPathFormat: {
    id: `${scope}.incorrectPathFormat`,
    defaultMessage: 'Incorrect path format',
  },
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Media Management',
  },
  myFile: {
    id: `${scope}.myFile`,
    defaultMessage: 'My file',
  },
  outsideLink: {
    id: `${scope}.outsideLink`,
    defaultMessage: 'Outside Link',
  },
  createFolder: {
    id: `${scope}.createFolder`,
    defaultMessage: 'Create Folder',
  },
  uploadFile: {
    id: `${scope}.uploadFile`,
    defaultMessage: 'Upload File',
  },
  upload: {
    id: `${scope}.upload`,
    defaultMessage: 'Upload',
  },
  upToFolder: {
    id: `${scope}.upToFolder`,
    defaultMessage: 'Up to Folder',
  },
  multiImages: {
    id: `${scope}.multiImages`,
    defaultMessage: 'Multi Images',
  },
  addImages: {
    id: `${scope}.addImages`,
    defaultMessage: 'Add Images',
  },
};
