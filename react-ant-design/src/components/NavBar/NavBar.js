import React, { PureComponent } from 'react';
import { Popover, Badge, Avatar } from 'antd';
import { router } from 'dva';
import cx from 'classnames';
import intl from 'react-intl-universal';
import PropTypes from 'prop-types';

import routerLinks from "@/utils/routerLinks";
import config from '@/config';
import SearchBox from './SearchBox';
import messages from './messages';

import logoImg from 'assets/images/logo1.png';
import './style/index.less';

const { Link } = router;

/**
 * Head office area
 */
class NavBar extends PureComponent {
  constructor (props) {
    super(props);
    this.state = {
      openSearchBox: false
    };
  }

  toggleFullScreen() {
    if (
      !document.fullscreenElement &&
      !document.mozFullScreenElement &&
      !document.webkitFullscreenElement &&
      !document.msFullscreenElement
    ) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  }

  onCloseSearchBox = () => {
    this.setState({
      openSearchBox: false
    });
  };

  onOpenSearchBox = () => {
    this.setState({
      openSearchBox: true
    });
  };

  render() {
    const { openSearchBox } = this.state;
    const {
      fixed,
      theme,
      onCollapseLeftSide,
      collapsed,
      user,
      isMobile
    } = this.props;

    const classnames = cx('navbar', {
      'navbar-fixed-top': !!fixed,
      'navbar-sm': isMobile ? true : collapsed,
      ['bg-' + theme]: !!theme
    });

    return (
      <header className={classnames}>
        <div className="navbar-branding">
          <Link className="navbar-brand" to="/">
            <img src={(user.setting && user.setting.image_logo) ? user.setting.image_logo : logoImg} alt="logo" />
            <strong>{(user.setting && user.setting.name_website) ?  user.setting.name_website : config.title}</strong>
          </Link>
          <span className="toggle_sidemenu_l" onClick={onCollapseLeftSide}>
            <i className="las la-bars" />
          </span>
        </div>
        <ul className="nav navbar-nav navbar-left clearfix">
          {isMobile ? null : (
            <li onClick={this.toggleFullScreen}>
              <button className="request-fullscreen">
                <i className="las la-expand" />
              </button>
            </li>
          )}
        </ul>
        <ul className="nav navbar-nav navbar-right clearfix">
          <li className="dropdown">
            <Popover
              placement="bottomRight"
              title={user.name}
              overlayClassName={cx('navbar-popup', { [theme]: !!theme })}
              content={<UserDropDown />}
              trigger="click"
            >
              <button className="dropdown-toggle">
                <Badge dot>
                  <Avatar src={require('assets/images/avatar.jpg')}>
                    {user.userName}
                  </Avatar>
                </Badge>
              </button>
            </Popover>
          </li>
        </ul>
        <SearchBox visible={openSearchBox} onClose={this.onCloseSearchBox} />
      </header>
    );
  }
}

const UserDropDown = props => (
  <ul className="dropdown-menu list-group dropdown-persist">
    <li className="list-group-item">
      <Link to={routerLinks("Profile")} className="animated animated-short fadeInUp">
        <i className="las la-cog" /> {intl.formatMessage(messages.acountSetting)}
      </Link>
    </li>
    <li className="list-group-item dropdown-footer">
      <Link to={routerLinks("Login")}>
        <i className="las la-power-off" /> {intl.formatMessage(messages.logOut)}
      </Link>
    </li>
  </ul>
);

NavBar.propTypes = {
  theme: PropTypes.string,
  language: PropTypes.string,
  fixed: PropTypes.bool,
  collapsed: PropTypes.bool,
  isMobile: PropTypes.bool,
  onCollapseLeftSide: PropTypes.func,
  onChangeLanguage: PropTypes.func,
  user: PropTypes.any,
};

NavBar.defaultProps = {
  fixed: true,
  theme: '' // 'bg-dark',
};

export default NavBar;
