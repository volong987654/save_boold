document.addEventListener("DOMContentLoaded", (event) => {
  setTimeout( () => {
    document.getElementById("loading").style.display = "none";
    document.getElementsByTagName("html")[0].style.overflow = "auto";
  }, 800);


  // Counter number
  const handCountScroll = () => {
    const classCounter = document.getElementsByClassName("counter")[0];
    if (classCounter) {
      let a = 0;
      window.addEventListener("scroll", () => {
        const oTop = classCounter.offsetTop - window.innerHeight;
        if (a === 0 && window.scrollY > oTop) {
          const counter = document.querySelectorAll("[data-count]");
          counter.forEach(item => {
            $({countNum: 0}).animate(
              { countNum: item.dataset.count },
              {
                duration: 2000,
                easing: "swing",
                step: function () {
                  item.textContent = parseInt(this.countNum, 0).toString();
                },
                complete: function () {
                  item.textContent = this.countNum;
                }
              }
            );
          });
          a = 1;
        }
      }, {passive: true});
    }
  };
  handCountScroll();

  // Form Validation
  $("form").map((index, item) => {
    $(item).parsley({
      trigger: "change",
      // successClass: "uk-form-success",
      errorClass: "uk-form-danger",
      errorsWrapper: "<div class=\"invalid-message\"></div>",
      errorTemplate: "<span class='uk-text-danger'></span>",
    })
      .on("form:submit",(e) => {
        const data = {};
        for (let i = 0; i < e.fields.length; i++) {
          data[e.fields[i].element.name] = e.fields[i].value;
        }
        console.log(data);
        return false;
      });
  });


  // Input Special
  $("[type=datetime-js]").datetimepicker({
    timepicker:false,
    mask:true,
    format:"Y/m/d H:i",
  });
  $("[type=phone]").inputmask();
  $("[type=tree-select]").treeSelect({
    json: {
      title: "t",
      value: "v",
      child: "c"
    },
    datatree: [{
      t: "*** text1",
      v: 1,
      c: []
    }, {
      t: "*** text2",
      v: 2,
      c: []
    },
    {
      t: "*** text3",
      v: 3,
      c: [
        {
          t: "*** lv2 text3",
          v: 3,
          c: []
        },
        {
          t: "*** lv2 text4",
          v: 4,
          c: [
            {
              t: "*** lv3 text3",
              v: 3,
              c: []
            },
            {
              t: "*** lv3 text4",
              v: 4,
            },
            {
              t: "*** lv3 text5",
              v: 5,
              c: []
            },
          ]
        },
        {
          t: "*** lv2 text5",
          v: 5,
          c: []
        }
      ]
    },
    {
      t: "*** text4",
      v: 4,
      c: []
    },
    {
      t: "*** text5",
      v: 5,
      c: []
    },
    ]
  });
  $(".handle-counter").handleCounter({
    minimum: 1,
    maximize: null,
  });

  $("#smartwizard").smartWizard({
    theme: "arrows",
    justified: true,
    autoAdjustHeight: false,
    enableURLhash: false,
    // toolbarSettings: {
    //   showNextButton: false,
    //   showPreviousButton: false,
    // },
  });
});
