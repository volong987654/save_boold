<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
      // Call the php artisan migrate:refresh
      $this->command->call('migrate:refresh');
      $this->command->warn("Data cleared, starting from blank database.");

      $admin = factory("App\Models\Role")->create(["name" => "Administrator","is_admin" => true, "description" => ""]);
      $guest = factory("App\Models\Role")->create(["name" => "Guest", "description" => ""]);
      $author = factory("App\Models\Role")->create(["name" => "Author", "description" => ""]);
      $this->command->info('Role Administrator and Author added.');

      $userRoute = factory("App\Models\Permission")->create(["route" => "/user", "name" => "User", "description" => ""]);
      $roleRoute = factory("App\Models\Permission")->create(["route" => "/role", "name" => "Role", "description" => ""]);
      $permissionRoute = factory("App\Models\Permission")->create(["route" => "/permission", "name" => "Permission", "description" => ""]);
      $rolePermissionRoute = factory("App\Models\Permission")->create(["route" => "/role-permission", "name" => "Set Permission in Role", "description" => ""]);
      $rolePermissionRoute = factory("App\Models\Permission")->create(["route" => "/setting", "name" => "Setting", "description" => ""]);

      factory("App\Models\Setting")->create(["name" => "image_logo", "title" => "Image Logo", "value" => "", "type" => "media", "order" => 1]);
      factory("App\Models\Setting")->create(["name" => "name_website", "title" => "Name Website", "value" => "", "type" => "input", "order" => 2]);


      factory("App\Models\RolePermission", 1)->create(["role_id" => $author->id, "permission_route" => $userRoute->route]);
      $this->command->info('Permission all route added and synced in role Author.');

      factory("App\User")->create(["name" => "Administrator","email" => "admin@admin.com", "role_id" => $admin->id, "status" => true, "blood_id" => null, "organization_id" => null]);
      $this->command->info('Default Email added.');
    }

    if ($this->command->confirm('Do you wish to auto generate data sample ?')) {
      $blood = mt_rand(10, 20);
      factory("App\Models\Blood", $blood)->create();
      $this->command->info('Blood ' . $blood . ' added.');

      $organization = mt_rand(10, 20);
      factory("App\Models\Organization", $organization)->create();
      $this->command->info('Organization ' . $organization . ' added.');

      $hospital = mt_rand(10, 20);
      factory("App\Models\Hospital", $hospital)->create();
      $this->command->info('Hospital ' . $hospital . ' added.');

      $user = mt_rand(15, 30);
      factory("App\User", $user)->create(["role_id" => 2])->each(function ($row) use ($hospital) {
        $row->hospitals()->attach(mt_rand(1,$hospital));
      });
      $this->command->info('User ' . $user . ' added.');

      $history = mt_rand(10, 20);
      factory("App\Models\History", $history)->create();
      $this->command->info('History ' . $history . ' added.');

      $find = mt_rand(15, 30);
      factory("App\Models\Find", $find)->create(["donor_id" => null]);
      $this->command->info('Find ' . $find . ' added.');

      $category = mt_rand(2, 5);
      factory("App\Models\Category", $category)->create();
      $this->command->info('Category ' . $category . ' added.');

      $news= mt_rand(10, 20);
      factory("App\Models\News", $news)->create()->each(function ($row) use ($category) {
        $row->categories()->attach(mt_rand(1,$category));

        $comment = mt_rand(10, 20);
        factory("App\Models\Comment", $comment)->create(["news_id" => $row->id]);
        $this->command->info('Comment ' . $comment . ' added.');
      });
      $this->command->info('News ' . $news . ' added.');

      $slide = mt_rand(10, 20);
      factory("App\Models\Slide", $slide)->create();
      $this->command->info('Slide ' . $slide . ' added.');



    }
  }
}
