<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Blood;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Organization;
use App\Models\Slide;
use App\Models\Find;

use App\Models\History;
use App\Models\Hospital;
use App\Models\News;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\Setting;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Role::class, function (Faker $faker) {
  static $order = 1;
  return [
    'name' => $faker->name,
    'description' => $faker->paragraph,
    "order" => $order++,
  ];
});

$factory->define(User::class, function (Faker $faker) {
  return [
    'blood_id' => function () {
      return Blood::all()->random();
    },
    'organization_id' => function () {
      return Organization::all()->random();
    },
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'email_verified_at' => now(),
    'password' => 'password',
    'remember_token' => Str::random(10),
    'address' => $faker->address,
    'phone' => $faker->phoneNumber,
    'gender' => mt_rand(1,2),
    "identity_card" => $faker->ein,
    "birthday" => $faker->dateTimeBetween("-4 weeks"),
    "job" => $faker->jobTitle,
    "note"=> $faker->text,
    "last_time"=> $faker->dateTimeBetween("-4 months"),
    "longitude"=> $faker->longitude,
    "latitude"=> $faker->latitude,
    "avatar"=>$faker->imageUrl($width = 640, $height = 480)
  ];
});

$factory->define(Permission::class, function (Faker $faker) {
  static $order = 1;
  return [
    'route' => $faker->url,
    'name' => $faker->name,
    'description' => $faker->paragraph,
    "order" => $order++,
  ];
});

$factory->define(RolePermission::class, function (Faker $faker) {
  return [
    'role_id' => function () {
      return Role::all()->random();
    },
    'permission_route' =>  $faker->url,
    "can" => ["GET"]
  ];
});

$factory->define(Setting::class, function (Faker $faker) {
//  $arrayType = [
//    "autocomplete", "cascade", "checkbox", "editor", "date",
//    "time~", "date~", "datetime", "month", "time",
//    "input", "media", "number", "password", "radio",
//    "select", "switch", "table", "transfer", "transfertree",
//    "treeselect", "textarea"];
  static $order = 1;
  $title = $faker->name;
  $name = Str::slug($title, "-");
//  $number = mt_rand(0,21);

  return [
    'name' => $name,
    'title' => $title,
    "value" => $faker->name,
    'details' => $faker->paragraph,
    'type' => "input",
    "ins"=>$faker->url,
    "facebook"=>$faker->url,
    "email" =>  $faker->unique()->safeEmail,
    "address" => $faker->address,
    "phone" => $faker->phoneNumber,
    "order" => $order++,
  ];
});

$factory->define(History::class, function (Faker $faker) {
  return [
    'user_id' => function () {
      return User::all()->random();
    },
    "time" => $faker->dateTimeBetween("-4 weeks"),
    'health' => mt_rand(1,2),
    'unit' => $faker->randomFloat(2, 0, 99),
    'erythrocyte' => $faker->randomFloat(2, 0, 99),
    'platelet' => $faker->randomFloat(2, 0, 99),
    'hemoglobin' => $faker->randomFloat(2, 0, 99),
    'address' => $faker->address,
    "status" => mt_rand(0,1),
  ];
});

$factory->define(Blood::class, function (Faker $faker) {
  $arrayParent = [null,1,1,1,1,1,1,1,1,null,10,10,10,null,14,14,14,null,18,18,null,21,21,21];
  static $order = 1;
  $parentId = $arrayParent[$order - 1];
  return [
    "parent_id" => $parentId,
    "order" => $order++,
    "name" => "Blood. ".$faker->sentence(1),
    "description" => $faker->text(200),
  ];
});

$factory->define(Hospital::class, function (Faker $faker) {
  $arrayParent = [null,1,2,2,2,1,6,6,6,null,10,11,11,11,10,14,14,null,18,19,19,18,23,23];
  static $order = 1;
  $parentId = $arrayParent[$order - 1];
  return [
    "parent_id" => $parentId,
    "order" => $order++,
    "name" => "Hospital. ".$faker->sentence(1),
    "address" => $faker->address,
  ];
});

$factory->define(Find::class, function (Faker $faker) {
  return [
    'user_id' => function () {
      return User::all()->random();
    },
    'hospital_id' => function () {
      return Hospital::all()->random();
    },
    'blood_id' => function () {
      return Blood::all()->random();
    },
    "description" => $faker->text(200),
    'relatives_name' => $faker->name,
    'relatives_phone' => $faker->phoneNumber,
    "instruction" => $faker->imageUrl($width = 640, $height = 480),
    "donor_id"=> function () {
      return User::all()->random();
    },
  ];
});

$factory->define(News::class, function (Faker $faker) {
  $name = $faker->sentence(2);
  $slug = Str::slug($name, "-");

  return [
    'name' => $name,
    "slug" => $slug,
    "description" => $faker->text(200),
    'body' => $faker->paragraph(50),
    "image" => $faker->imageUrl($width = 640, $height = 480),
  ];
});
$factory->define(Slide::class, function (Faker $faker) {
  $arrayParent = ['contact','news','news-blog','finds','vinhdanh','home'];
  static $order = 1;
  $parentId = $arrayParent[$number = mt_rand(0,5)];
  return [
    'title' => $faker->sentence(2),
    "text" => $faker->text(200),
    "image" => $faker->imageUrl($width = 640, $height = 480),
    "page" => $parentId,
    "order" => $order++,
  ];
});


$factory->define(Category::class, function (Faker $faker) {
  $name = $faker->sentence(2);
  $slug = Str::slug($name, "-");
  static $order = 1;

  return [
    'name' => $name,
    "slug" => $slug,
    "description" => $faker->text(200),
    "order" => $order++,
  ];
});


$factory->define(Organization::class, function (Faker $faker) {
  static $order = 1;

  return [
    'name' => $faker->sentence(2),
    "description" => $faker->text(200),
    "order" => $order++,
    "logo" => $faker->imageUrl($width = 640, $height = 480),
    "url" => $faker->url,
  ];
});
$factory->define(Comment::class, function (Faker $faker) {
  static $order = 1;
  return [
    'news_id' => function () {
      return News::all()->random();
    },
    "author_id" => function () {
      return User::all()->random();
    },
    "parent_id" => null,
    "order" => $order++,
    "content" => $faker->text(200),
  ];
});
