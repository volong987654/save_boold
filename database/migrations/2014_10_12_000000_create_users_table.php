<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('role_id')->unsigned()->nullable();
      $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('set null');

      $table->unsignedInteger('blood_id')->unsigned()->nullable();
      $table->foreign('blood_id')->references('id')->on('bloods')->onUpdate('cascade')->onDelete('set null');

      $table->unsignedBigInteger('organization_id')->unsigned()->nullable();
      $table->foreign('organization_id')->references('id')->on('organizations')->onUpdate('cascade')->onDelete('set null');

      $table->string('name')->comment('Họ và tên');
      $table->string('email')->unique();

      $table->string('address')->comment('Địa chỉ');
      $table->string('phone')->comment('Số điện thoại');
      $table->integer('gender')->comment('Giới tính');
      $table->string('identity_card')->comment('Số CMND');

      $table->timestamp('email_verified_at')->nullable();
      $table->string('password')->comment('Mật khẩu');
      $table->rememberToken();
      $table->string('avatar')->nullable()->comment('ảnh');
      $table->text('bio')->nullable();

      $table->timestamp('birthday')->nullable()->comment('Ngày sinh');
      $table->string('job')->nullable()->comment('Nghề nghiệp');
      $table->string('note')->nullable()->comment('Ghi chú');
      $table->timestamp('last_time')->nullable()->comment('Thời gian hiến máu gần nhất');
      $table->float('longitude')->nullable()->comment('Kinh tuyến');
      $table->float('latitude')->nullable()->comment('Vĩ tuyến');

      $table->boolean('status')->default(false)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}
