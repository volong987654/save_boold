<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsCategoriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('news_id')->unsigned();
      $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
      $table->unsignedBigInteger('category_id')->unsigned();
      $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('news_categories');
  }
}
