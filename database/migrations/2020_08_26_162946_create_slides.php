<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlides extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
          $table->id();
          $table->string('title')->comment('Tiêu đề ngắn');
          $table->string('text')->comment('Tiêu đề dài');
          $table->string('image')->comment('Ảnh');
          $table->string('page')->comment('Tên trang');
          $table->integer('order')->default(1);
          $table->tinyInteger('status')->default(true)->comment('Trạng thái');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides');
    }
}
