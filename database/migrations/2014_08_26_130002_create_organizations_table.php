<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('organizations', function (Blueprint $table) {
      $table->id();
      $table->integer('order')->default(1);
      $table->string('name');
      $table->text('description')->nullable();
      $table->string('logo')->nullable()->comment('ảnh');
      $table->string('url')->nullable()->comment('đường dẫn website');
      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->boolean('featured')->default(false)->comment('nổi bật');
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('organizations');
  }
}
