<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('roles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name')->unique()->comment('Tên vai trò');
      $table->text('description')->nullable()->comment('Mô tả');
      $table->boolean('is_admin')->default(false);
      $table->integer('order')->default(1)->comment('Thứ tự');
      $table->boolean('status')->default(true)->comment('Trạng thái');
       $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('roles');
  }
}
