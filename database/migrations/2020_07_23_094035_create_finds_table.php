<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFindsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('finds', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

      $table->unsignedInteger('hospital_id')->unsigned();
      $table->foreign('hospital_id')->references('id')->on('hospitals')->onUpdate('cascade')->onDelete('cascade');

      $table->unsignedInteger('blood_id')->unsigned();
      $table->foreign('blood_id')->references('id')->on('bloods')->onUpdate('cascade')->onDelete('cascade');

      $table->string('instruction')->comment('Y lệnh');
      $table->text('description')->comment('Lý do cần máu');
      $table->string('relatives_name')->comment('Họ và tên người thân');
      $table->string('relatives_phone')->comment('SĐT người thân');
      $table->unsignedInteger('donor_id')->unsigned()->nullable()->comment('Id người hiến');
      $table->foreign('donor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('finds');
  }
}
