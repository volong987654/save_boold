<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('histories', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

      $table->timestamp('time')->nullable()->comment('Thời gian hiến máu');
      $table->integer('health')->comment('Sức khỏe khi hiến');
      $table->decimal('unit', 8, 2)->comment('Đơn vị hiến');
      $table->decimal('erythrocyte', 8, 2)->comment('Hồng cầu');
      $table->decimal('platelet', 8, 2)->comment('Tiểu cầu');
      $table->decimal('hemoglobin', 8, 2)->comment('Huyết sắc tố');
      $table->string('address')->comment('Địa chỉ hiến');
      $table->boolean('status')->default(true)->comment('Trạng thái');

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('histories');
  }
}
