<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('permissions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('route')->unique()->comment('Tuyến đường');
      $table->string('name')->comment('Tên quyền');
      $table->text('description')->nullable()->comment('Mô tả');
      $table->integer('order')->default(1)->comment('Thứ tự');
      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('permissions');
  }
}
