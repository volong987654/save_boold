<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHospitalsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('hospitals', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('parent_id')->unsigned()->nullable();
      $table->foreign('parent_id')->references('id')->on('hospitals')->onUpdate('cascade')->onDelete('set null');

      $table->integer('order')->default(1)->comment('Thứ tự');
      $table->string('name')->comment('Tên bệnh viện');
      $table->string('address')->comment('Địa chỉ');
      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('hospitals');
  }
}
