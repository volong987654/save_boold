<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloodsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bloods', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('parent_id')->unsigned()->nullable()->default(null);
      $table->foreign('parent_id')->references('id')->on('bloods')->onUpdate('cascade')->onDelete('set null');

      $table->integer('order')->default(1)->comment('Thứ tự');
      $table->string('name')->comment('Nhóm máu');
      $table->text('description')->nullable()->comment('Mô tả');
      $table->boolean('status')->default(true)->comment('Trạng thái');
       $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bloods');
  }
}
