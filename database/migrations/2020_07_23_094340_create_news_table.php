<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name')->comment('Tiêu đề');
      $table->string('image')->comment('Ảnh');
      $table->string('slug')->unique()->comment('Đường dẫn');
      $table->text('description')->comment('Mô tả');
      $table->text('body')->comment('Nội dung');
      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('news');
  }
}
