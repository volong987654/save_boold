<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('comments', function (Blueprint $table) {
      $table->id();
      $table->unsignedBigInteger('parent_id')->unsigned()->nullable()->default(null);
      $table->foreign('parent_id')->references('id')->on('comments')->onUpdate('cascade')->onDelete('set null');
      $table->unsignedInteger('news_id')->unsigned();
      $table->foreign('news_id')->references('id')->on('news')->onUpdate('cascade')->onDelete('cascade');
      $table->unsignedInteger('author_id')->unsigned();
      $table->foreign('author_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
      $table->text('content');
      $table->integer('order')->default(1);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('comments');
  }
}
