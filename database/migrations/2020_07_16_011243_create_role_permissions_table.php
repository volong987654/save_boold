<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permissions', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('role_id')->unsigned();
          $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
          $table->string('permission_route');
          $table->foreign('permission_route')->references('route')->on('permissions')->onUpdate('cascade')->onDelete('cascade');
          $table->json('can');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
