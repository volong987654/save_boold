<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('settings', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name')->unique()->comment('Tên');
      $table->string('title')->comment('Tiêu đề');
      $table->text('value')->nullable()->comment('Giá trị');
      $table->text('details')->nullable()->default(null)->comment('Chi tiết');
      $table->string('type')->comment('Loại');
      $table->string('email')->comment('email');
      $table->string('address')->comment('địa chỉ trụ sở');
      $table->string('phone')->comment('sdt');
      $table->string('ins')->comment('url ins');
      $table->string('facebook')->comment('url face');
      $table->integer('order')->default('1')->comment('Thứ tự');
      $table->boolean('status')->default(true)->comment('Trạng thái');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('settings');
  }
}
