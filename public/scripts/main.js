"use strict";
document.addEventListener("DOMContentLoaded", (event) => {
  setTimeout( () => {
    document.getElementById("loading").classList.add("animate__slideOutRight"),
    document.getElementsByTagName("html")[0].style.overflow = "auto"
  }, 800);

  Parsley.addMessages("vi", {
    defaultMessage: "Trường này có vẻ không đúng.",
    type: {
      email:        "Trường này không phải là email.",
      url:          "Trường này cần là một đường dẫn.",
      number:       "Trường này cần là một số.",
      integer:      "Trường này cần là số nguyên.",
      digits:       "Trường này cần là ký tự số.",
      alphanum:     "Trường này cần là chữ và số."
    },
    notblank:       "Trường này không được để trống.",
    required:       "Trường này là bắt buộc.",
    pattern:        "Giá trị này có vẻ không đúng.",
    min:            "Giá trị cần lớn hơn hoặc bằng %s.",
    max:            "Giá trị cần bé hơn hoặc bằng %s.",
    range:          "Giá trị cần nằm trong khoảng %s và %s.",
    minlength:      "Giá trị quá ngắn. Cần là %s ký tự hoặc nhiều hơn.",
    maxlength:      "Giá trị quá dài. Cần là %s ký tự hoặc ít hơn.",
    length:         "Độ dài không đúng. Cần có độ dài ở giữa %s và %s ký tự.",
    mincheck:       "Bạn cần chọn tối thiểu %s tùy chọn.",
    maxcheck:       "Bạn cần chọn %s tùy chọn hoặc ít hơn.",
    check:          "Bạn cần chọn giữa %s và %s tùy chọn.",
    equalto:        "Giá trị này cần bằng.",
    euvatin:        "Đây không phải là số VAT.",
  });
  Parsley.addMessages("ja", {
    defaultMessage: "無効な値です。",
    type: {
      email:        "有効なメールアドレスを入力してください。",
      url:          "有効なURLを入力してください。",
      number:       "数値を入力してください。",
      integer:      "整数を入力してください。",
      digits:       "数字を入力してください。",
      alphanum:     "英数字を入力してください。"
    },
    notblank:       "この値を入力してください",
    required:       "この値は必須です。",
    pattern:        "この値は無効です。",
    min:            "%s 以上の値にしてください。",
    max:            "%s 以下の値にしてください。",
    range:          "%s から %s の値にしてください。",
    minlength:      "%s 文字以上で入力してください。",
    maxlength:      "%s 文字以下で入力してください。",
    length:         "%s から %s 文字の間で入力してください。",
    mincheck:       "%s 個以上選択してください。",
    maxcheck:       "%s 個以下選択してください。",
    check:          "%s から %s 個選択してください。",
    equalto:        "値が違います。"
  });
  // Counter number
  const handCountScroll = () => {
    const classCounter = document.getElementsByClassName("counter")[0];
    if (classCounter) {
      let a = 0;
      window.addEventListener("scroll", () => {
        const oTop = classCounter.offsetTop - window.innerHeight;
        if (a === 0 && window.scrollY > oTop) {
          const counter = document.querySelectorAll("[data-count]");
          counter.forEach(item => {
            $({countNum: 0}).animate(
              { countNum: item.dataset.count },
              {
                duration: 2000,
                easing: "swing",
                step: function () {
                  item.textContent = parseInt(this.countNum, 0).toString();
                },
                complete: function () {
                  item.textContent = this.countNum;
                }
              }
            );
          });
          a = 1;
        }
      }, {passive: true});
    }
  };
  handCountScroll();

  // Form Validation
  // $("form").map(function (t, e) {
  //     $(e).parsley({
  //       trigger: "change",
  //       errorClass: "uk-form-danger",
  //       errorsWrapper: '<div class="invalid-message"></div>',
  //       errorTemplate: "<span class='uk-text-danger'></span>"
  //     })
    //   .on("form:error", function (t) {
    //     for (let e = {}, n = 0; n < t.fields.length; n++) e[t.fields[n].element.name] = t.fields[n].value;
    //     return !1
    //   })
    // })

  // Input Special
  $("[type=datetime-js]").datetimepicker({
    mask:true,
    format:"Y/m/d",
  });
  $("[type=phone]").inputmask();
  $(".handle-counter").handleCounter({
    minimum: 1,
    maximize: null,
  });

  $("#smartwizard").smartWizard({
    theme: "arrows",
    justified: true,
    autoAdjustHeight: false,
    enableURLhash: false,
    // toolbarSettings: {
    //   showNextButton: false,
    //   showPreviousButton: false,
    // },
  });
});



